<?php

use Step\Api\BaseFunctions_api as Base_api;
use \page\Pageobj_unique;

$I = new ApiTester($scenario);
$Base_api = new Base_api($scenario);

$Site_unique = Pageobj_unique::$prod_url_fhc;
$Error_Block_unique = Pageobj_unique::$errorblck;

$I->wantTo('verify 404 pages are custom - FierceHealthCare');

$I->Custom404pg($Base_api,
    $Site_unique,
    $Error_Block_unique
);
