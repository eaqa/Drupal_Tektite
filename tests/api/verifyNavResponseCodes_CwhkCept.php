<?php
/*Must Run test with the eng flag of --env apiResponseCode_TAC
codecept run api /Users/erikanderson/Tektite_refacor/tests/api/verifyNavResponseCodes_tacCept.php --steps --debug --env apiResponseCode_TAC
*/

use Page\Pageobj_unique;
use \page\Pageobj_NavLinks;
use Page\Pageobj_common;
use Step\Api\httpResponseCheck as httpCheck;

$Site = Pageobj_unique::$prod_url_Cwhk;


$I = new ApiTester($scenario);
$TestSteps = new httpCheck($scenario);

$I->wantTo('verify the integrity of Menu Navigation Link\'s header response codes -- CWHK');

#Main Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_Cwhk ##REPLACE with prod
);

#SubNav Validation
$I->Validate_httpStatus_subNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_Cwhk ##REPLACE with prod
);

#Top Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_top_Cwhk ##REPLACE with prod
);

#Bottom Footer Nav Menu
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_footerNav_Cwhk ##REPLACE with prod
);