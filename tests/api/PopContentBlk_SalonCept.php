<?php

use Page\Pageobj_unique;
use Page\Pageobj_common;
use Step\Api\PopContentBlock as PopContentBlk;
use Step\Api\BaseFunctions_api as Basefunctions;

//Replace for each SUT
$Site_Unique = Pageobj_unique::$prod_url_salon;
$Parsely_CSV_Index = '14'; #14 is the array index for Salon

$blockFiller = 'tektite-amsalon-';

$I = new ApiTester($scenario);
$TestSteps = new PopContentBlk($scenario);
$BaseFunctions = new \Step\Api\BaseFunctions_api($scenario);

$I->wantTo('know that the popular content block is pulling in 5 featured articles -- American Salon');

$TestSteps->VerifyResponseContainsTitleText(
    $Site_Unique,
    $blockFiller,
    $BaseFunctions,
    $Parsely_CSV_Index
);





