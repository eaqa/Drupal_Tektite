<?php
use \Step\Api\Solr as SolrSearch;
use \Step\Api\DataFeeds as Feeds;
use Page\Pageobj_unique;

$solr_core_unique = Pageobj_unique::$prod_solr_response;
$Site_unique = Pageobj_unique::$prod_url_response;
$search_H1Blk_unique = Pageobj_unique::$H1blk_solr_response;



$I = new ApiTester($scenario);
$Feeds = new Feeds($scenario);
$TestSteps = new SolrSearch($scenario);
$I->wantTo('Verify that Article Keywords are producing search result hits - Response Mag');

$article_URL =
    $I->grabArticleURL($Feeds, $Site_unique);

$keywordTerm_encoded =
    $I->grabArticleKeyWords($TestSteps,
        $article_URL);

$articleURL_decode = urldecode($article_URL);
$articleKeyword_decode = urldecode($keywordTerm_encoded);

$numFound =
//Perform keyword search and assertions
    $TestSteps->AssertKeywordQuery($Site_unique, $keywordTerm_encoded, $articleKeyword_decode, $search_H1Blk_unique);


echo "\n -------------------------------------- \n";
echo "\n Article Selected: {$articleURL_decode} \n";
echo "\n Keyword Selected: {$articleKeyword_decode} \n";
echo "\n Number of Hits: {$numFound} \n";
echo "\n -------------------------------------- \n";
