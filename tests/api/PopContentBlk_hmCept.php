<?php

use Page\Pageobj_unique;
use Page\Pageobj_common;
use Step\Api\PopContentBlock as PopContentBlk;
use Step\Api\BaseFunctions_api as Basefunctions;

//Replace for each SUT
$Site_Unique = Pageobj_unique::$prod_url_hm;
$Parsely_CSV_Index = '8'; #8 is the array index for HM

$blockFiller = '';

$I = new ApiTester($scenario);
$TestSteps = new PopContentBlk($scenario);
$BaseFunctions = new \Step\Api\BaseFunctions_api($scenario);

$I->wantTo('know that the popular content block is pulling in 5 featured articles -- Hotel Management');

$TestSteps->VerifyResponseContainsTitleText(
    $Site_Unique,
    $blockFiller,
    $BaseFunctions,
    $Parsely_CSV_Index
);