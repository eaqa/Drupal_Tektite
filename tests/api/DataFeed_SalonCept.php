<?php
use Step\Api\DataFeeds as feed;
use \page\Pageobj_unique;

$I = new ApiTester($scenario);
$feed = new feed($scenario);
$Site_unique = Pageobj_unique::$prod_url_salon;
$url_pattern = Pageobj_unique::$prod_url_pattern_salon;
$I->wantTo('verify the integrity of data feeds - American Salon');

//Syndication feeds//

////---NewsCred
#grab feed and assert for valid json
$feed->GrabJsonFeed_newscred($Site_unique);
#assert there are no $dates  older 7 days.
$feed->assert_JsonFeed_timestamp();
#assert urls are of the sut's domain
$feed->assert_JsonFeed_domin_nextpage($Site_unique);
#assert urls are of the sut's domain
$feed->assert_JsonFeed_domain_url($Site_unique);

///---LexixNexus
##grab feed and assert for valid json
$feed->GrabJsonFeed_lexis($Site_unique);
#assert there are no $dates  older 7 days.
$feed->assert_JsonFeed_timestamp();
#assert urls are of the sut's domain
$feed->assert_JsonFeed_domin_nextpage($Site_unique);
#assert urls are of the sut's domain
$feed->assert_JsonFeed_domain_url($Site_unique);

//RSS feeds//

#grab & validate if XML feed is valid
$feed->Validate_XML_Feed($Site_unique);
#assert no date $dates are older than 11 days
$feed->assert_XML_Feed_pubdate($Site_unique);
#assert domain of sut in $links
$feed->assert_XMLFeed_links_domin($Site_unique);
#assert domain of sut in $guid
$feed->assert_XMLFeed_gUid_domin($Site_unique, $url_pattern);
