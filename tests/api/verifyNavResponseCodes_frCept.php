<?php

use Page\Pageobj_unique;
use \page\Pageobj_NavLinks;
use Page\Pageobj_common;
use Step\Api\httpResponseCheck as httpCheck;

$Site = Pageobj_unique::$prod_url_fr;


$I = new ApiTester($scenario);
$TestSteps = new httpCheck($scenario);

$I->wantTo('verify the integrity of Menu Navigation Link\'s header response codes -- FierceRetail');

#Main Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_fr
);

#Sub Nav validation
$I->Validate_httpStatus_subNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_fr
);

#Top Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_top_fr
);

#Bottom Footer Nav Menu
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_footerNav_fr
);