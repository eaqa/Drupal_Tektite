<?php

use Page\Pageobj_unique;
use \page\Pageobj_NavLinks;
use Page\Pageobj_common;
use Step\Api\httpResponseCheck as httpCheck;

$Site = Pageobj_unique::$prod_url_amspa;


$I = new ApiTester($scenario);
$TestSteps = new httpCheck($scenario);

$I->wantTo('verify the integrity of Menu Navigation Link\'s header response codes -- AmSpa');

#Main Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_Amspa
);

#Top Nav validation
#Skipping TOP navigation test as the CURRENT ISSUE Link causes an infiniant loop
/*$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_top_Amspa
);*/

#Bottom Footer Nav Menu
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_footerNav_Amspa
);