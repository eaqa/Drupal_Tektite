<?php

use Page\Pageobj_unique;
use Page\Pageobj_common;
use Step\Api\PopContentBlock as PopContentBlk;
use Step\Api\BaseFunctions_api as Basefunctions;
/**
 * @skip
 */
/*
 * Ask Jakie Stone for Parse.ly API credentials
 *Curerntly this feature on CEO is TBD, not implemented for the time being.
 * */

//Replace for each SUT
$Site_Unique = Pageobj_unique::$prod_url_fceo;
$Parsely_CSV_Index = 'xxxxxx'; #1 is the array index for FierceCEO todo get parse.ly API creds

$blockFiller = 'tektite-fierce-';

$I = new ApiTester($scenario);
$TestSteps = new PopContentBlk($scenario);
$BaseFunctions = new \Step\Api\BaseFunctions_api($scenario);

//skipping test
$scenario->skip();

$I->wantTo('know that the popular content block is pulling in 5 featured articles -- FierceCEO');

$TestSteps->VerifyResponseContainsTitleText(
    $Site_Unique,
    $blockFiller,
    $BaseFunctions,
    $Parsely_CSV_Index
);





