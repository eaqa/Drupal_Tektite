<?php

use Page\Pageobj_unique;
use \page\Pageobj_NavLinks;
use Page\Pageobj_common;
use Step\Api\httpResponseCheck as httpCheck;

$Site = Pageobj_unique::$prod_url_response;


$I = new ApiTester($scenario);
$TestSteps = new httpCheck($scenario);

$I->wantTo('verify the integrity of Menu Navigation Link\'s header response codes -- ResponseMag');

#Main Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_response
);

#Top Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_topSoc_response
);

#Bottom Footer Nav Menu
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_footerNav_response
);