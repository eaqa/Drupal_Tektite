<?php

/*
 * At the time of writing this test, the Popular Content block has no data. Test should fail.
 *
 * Test  tests/api/PopContentBlk_SensorsMagCept.php
 Step  See number of elements "//*[@id="block-tektite-amsalon-popularcontentfromparsely"]/ul/li",5
 Fail  Number of elements counted differs from expected number
Failed asserting that 0 matches expected 5.

 * */

use Page\Pageobj_unique;
use Page\Pageobj_common;
use Step\Api\PopContentBlock as PopContentBlk;
use Step\Api\BaseFunctions_api as Basefunctions;

//Replace for each SUT
$Site_Unique = Pageobj_unique::$prod_url_sensorsMag;
$Parsely_CSV_Index = '12'; #14 is the array index for SensorsMag

$blockFiller = 'tektite-sensorsmag-';

$I = new ApiTester($scenario);
$TestSteps = new PopContentBlk($scenario);
$BaseFunctions = new \Step\Api\BaseFunctions_api($scenario);

$I->wantTo('know that the popular content block is pulling in 5 featured articles -- SensorsMag');

$TestSteps->VerifyResponseContainsTitleText(
    $Site_Unique,
    $blockFiller,
    $BaseFunctions,
    $Parsely_CSV_Index
);