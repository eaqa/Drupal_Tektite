<?php
/*Must Run test with the eng flag of --env apiResponseCode_LTA
codecept run api /Users/erikanderson/Tektite_refacor/tests/api/verifyNavResponseCodes_ltaCept.php --steps --debug --env apiResponseCode_LTA
*/
/**
 *
 * @env apiResponseCode_LTA
 *
 *
 */

use Page\Pageobj_unique;
use \page\Pageobj_NavLinks;
use Page\Pageobj_common;
use Step\Api\httpResponseCheck as httpCheck;

$Site = Pageobj_unique::$prod_url_lta;


$I = new ApiTester($scenario);
$TestSteps = new httpCheck($scenario);

$I->wantTo('verify the integrity of Menu Navigation Link\'s header response codes -- LuxuryTravelAdvidor');

#Main Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_lta
);

#SubNav Validation
$I->Validate_httpStatus_subNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_lta
);

#Top Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_top_lta
);

#Bottom Footer Nav Menu
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_footerNav_lta
);