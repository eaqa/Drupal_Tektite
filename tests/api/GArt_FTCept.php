<?php
use Step\Api\GA_veri as GA_Realtime;
use Step\Api\BaseFunctions_api as BaseFunction;
$GA_CSV_Index = '6'; #is the array index for FierceTelecom


$I = new ApiTester($scenario);
$TestSteps = new GA_Realtime($scenario);
$Base = new BaseFunction($scenario);
$I->wantTo('verify that there are real-time GA pageviews -- FierceTelecom');

//Grabbing GA rta page view hit count
$TestSteps->GA_rta($Base,
    $GA_CSV_Index);

//Verifying GA rta page view hit count is greater than '1'
$TestSteps->VerifyGA_rtaHits($Base,
    $GA_CSV_Index);