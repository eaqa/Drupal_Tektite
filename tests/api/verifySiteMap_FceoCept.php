<?php
use Page\Pageobj_unique;
use Page\Pageobj_common;
use Step\Api\SiteMap as SiteMap;

$SiteDomain_Unique = 'fierceceo';
$nodeLOC_unique = 'loc';
$xmlURL_index = Pageobj_common::$siteMapIndex;
$xmlURL_subpg = Pageobj_common::$siteMap_subpg1;
$xmlURL_subpg2 = Pageobj_common::$siteMap_subpg2;
$xmlTree_subpg1 = Pageobj_common::$treeNode_subpg1;



$I = new ApiTester($scenario);
$TestSteps = new SiteMap($scenario);

$I->wantTo('verify the sitemaps for correctness - FierceCmo');

//-------Asserting domain for index sitemap -------
$TestSteps->AssertDomain_SiteMapxml(
    $SiteDomain_Unique,
    $nodeLOC_unique,
    $xmlURL_index,
    $xmlTree_subpg1
);

#Asserting on sitemap index URLs are fully qualified/valid
$TestSteps->Assert_URL_SiteMapIndex(
    $SiteDomain_Unique, #domain of site
    $nodeLOC_unique, #defining loc as xml node to test
    $xmlURL_index, #defines going to the sitemap index
    $xmlTree_subpg1 #xml node to enter into array - "url"
);

#Asserting no node urls in index sitemap -------

$TestSteps->AssertNodeURL_SiteMapxml(
    $SiteDomain_Unique, #domain of site
    $nodeLOC_unique, #xml node for test
    $xmlURL_index,
    $xmlTree_subpg1 #xml node to enter into array - url
);

//-------Asserting domain for subpage sitemap 1  -------

$TestSteps->AssertDomain_SiteMapxml(
    $SiteDomain_Unique,
    $nodeLOC_unique,
    $xmlURL_subpg,
    $xmlTree_subpg1 #xml node to enter into array - url
);

#Asserting no node urls in subpage sitemap 1 -------

$TestSteps->AssertNodeURL_SiteMapxml(
    $SiteDomain_Unique, #domain of site
    $nodeLOC_unique, #xml node for test
    $xmlURL_subpg,
    $xmlTree_subpg1 #xml node to enter into array - url
);

//-------Asserting domain for subpage sitemap 2  -------

$TestSteps->AssertDomain_SiteMapxml(
    $SiteDomain_Unique,
    $nodeLOC_unique,
    $xmlURL_subpg2,
    $xmlTree_subpg1 #xml node to enter into array - url
);

#Asserting no node urls in subpage sitemap 2 -------

$TestSteps->AssertNodeURL_SiteMapxml(
    $SiteDomain_Unique, #domain of site
    $nodeLOC_unique, #xml node for test
    $xmlURL_subpg2,
    $xmlTree_subpg1 #xml node to enter into array - url
);