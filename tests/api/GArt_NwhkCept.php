<?php

/**
*Not Yet implemented
 *
 *
@skip - skip test until prod site is live
Need to update content to prod address
 */
use Step\Api\GA_veri as GA_Realtime;
use Step\Api\BaseFunctions_api as BaseFunction;
$GA_CSV_Index = 'xxxxxxxxxxxx'; #is the array index for CWHK  #Update CSV when GA is implemented for CWHK


$I = new ApiTester($scenario);
$TestSteps = new GA_Realtime($scenario);
$Base = new BaseFunction($scenario);
$I->wantTo('verify that there are real-time GA pageviews -- CWHK');

//Grabbing GA rta page view hit count
$TestSteps->GA_rta($Base,
    $GA_CSV_Index);

//Verifying GA rta page view hit count is greater than '1'
$TestSteps->VerifyGA_rtaHits($Base,
    $GA_CSV_Index);