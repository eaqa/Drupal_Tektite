<?php

/**
@skip test
 */
use \Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Chrome\ChromeDriver;

$I = new ApiTester($scenario);
$I->wantTo('perform actions and see result');

#method ONE
#using httpauthenticated function - user is NOT authed
//$I->amHttpAuthenticated('eanderson', 'Questex1');
//$I->amOnUrl('https://www.americanspa.com/user/login');
//$I->pauseExecution();

#Method TWO:
#Poxy opens a new chrome session but no internet connection occurs

//$pluginForProxyLogin = '/tmp/a'.uniqid().'.zip';
//
//$zip = new ZipArchive();
//$res = $zip->open($pluginForProxyLogin, ZipArchive::CREATE | ZipArchive::OVERWRITE);
//$zip->addFile('/Users/erikanderson/Selenium-Chrome-HTTP-Private-Proxy-master/manifest.json', 'manifest.json');
//$background = file_get_contents('/Users/erikanderson/Selenium-Chrome-HTTP-Private-Proxy-master/background.js');
//$background = str_replace(['%proxy_host', '%proxy_port', '%username', '%password'], ['5.39.64.181', '54991', 'd1g1m00d', '13de02d0e0z9'], $background);
//$zip->addFromString('background.js', $background);
//$zip->close();
//
//putenv("webdriver.chrome.driver=/Users/erikanderson/bin/chromedriver");
//
//$options = new ChromeOptions();
//$options->addExtensions([$pluginForProxyLogin]);
//$caps = DesiredCapabilities::chrome();
//$caps->setCapability(ChromeOptions::CAPABILITY, $options);
//
//$driver = ChromeDriver::start($caps);
//$driver->get('https://www.americanspa.com/user/login');
//
///*header('Content-Type: image/png');
//echo $driver->takeScreenshot();*/
//
//
//unlink($pluginForProxyLogin);


#Method THREE
# seems to create a proxy session but do not know how to then interact with the page
/*
$wd_host = 'http://localhost:4444/wd/hub';
$capabilities = [
    WebDriverCapabilityType::BROWSER_NAME => 'chrome',
    WebDriverCapabilityType::PROXY => [
        'proxyType' => 'manual',
        'httpProxy' => '127.0.0.1:2043',
        'sslProxy' => '127.0.0.1:2043',
    ],
];
$driver = RemoteWebDriver::create($wd_host, $capabilities);*/