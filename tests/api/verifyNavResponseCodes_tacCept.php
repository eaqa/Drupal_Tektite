<?php
/*Must Run test with the eng flag of --env apiResponseCode_TAC
codecept run api /Users/erikanderson/Tektite_refacor/tests/api/verifyNavResponseCodes_tacCept.php --steps --debug --env apiResponseCode_TAC
*/

/**
*
* @env apiResponseCode_TAC
*
*
 */
//codecept run api /Users/erikanderson/Tektite_refacor/tests/api/verifyNavResponseCodes_tacCept.php --steps --debug --env apiResponseCode_TAC
use Page\Pageobj_unique;
use \page\Pageobj_NavLinks;
use Page\Pageobj_common;
use Step\Api\httpResponseCheck as httpCheck;

$Site = Pageobj_unique::$prod_url_tac;


$I = new ApiTester($scenario);
$TestSteps = new httpCheck($scenario);

$I->wantTo('verify the integrity of Menu Navigation Link\'s header response codes -- Travel Agent Central');

#Main Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_tac
);

#SubNav Validation
$I->Validate_httpStatus_subNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_tac
);

#Top Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_top_tac
);

#Bottom Footer Nav Menu
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_footerNav_tac
);