<?php
/*Must Run test with the eng flag of --env apiResponseCode_TAC
codecept run api verifyNavResponseCodes_SalonCept.php --steps --debug --env apiResponseCode_Salon
*/
/**
 *
 * @env apiResponseCode_Salon
 *
 *
 */

use Page\Pageobj_unique;
use \page\Pageobj_NavLinks;
use Page\Pageobj_common;
use Step\Api\httpResponseCheck as httpCheck;

$Site = Pageobj_unique::$prod_url_salon;


$I = new ApiTester($scenario);
$TestSteps = new httpCheck($scenario);

$I->wantTo('verify the integrity of Menu Navigation Link\'s header response codes -- American Salon');

#Main Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_salon
);

#SubNav Validation
$I->Validate_httpStatus_subNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_main_salon
);

#Top Nav validation
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_top_salon
);

#Bottom Footer Nav Menu
$I->Validate_httpStatus_mainNav($TestSteps,
    $Site,
    Pageobj_unique::$NavLocator_footerNav_salon
);