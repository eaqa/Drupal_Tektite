<?php

use Page\Pageobj_unique;
use Step\Api\NewsletterTool as NWSLT;

$Site_Unique = Pageobj_unique::$prod_url_fc;
$Newsletter_SiteNumber_Unique = Pageobj_unique::$nwsl_siteNumber_FC;


//--API Test -- Grab RSS Data --//

$I = new ApiTester($scenario);
$TestSteps_api = new NWSLT($scenario);

$I->wantTo('verify that the Newsletter tool is bringing in content -- FierceCable');

$I->GrabandClean_ArticleTitle_inRSS_feed($TestSteps_api,
    $Site_Unique
);


//--Acceptance Test -- Search / Verify --//

use Step\Acceptance\BaseFunctions as BaseFunction;
$Base_acceptance = new BaseFunction($scenario);

$I = new AcceptanceTester($scenario);
$Base_acceptance = new BaseFunction($scenario);
$TestSteps_acceptance = new \Step\Acceptance\NewsletterTool($scenario);

//To Log into OneLogin and navigate to edition
$I->NewsletterTool_Login_navigate($TestSteps_acceptance, $Base_acceptance, $Newsletter_SiteNumber_Unique);

//If already logged in - navigate directly to Newsletter edition
//$TestSteps_acceptance->NavigateToEdition($Newsletter_SiteNumber_Unique);

$I->Verify_nwslt_contains_grabbedArticle_data($TestSteps_acceptance,

//Article title 1
    $TestSteps_api->CleanRSSData_Nochars($Site_Unique),
    $TestSteps_api->CleanRSSData_NoTrailingSpace($Site_Unique),

//Article title 2
    $TestSteps_api->CleanRSSData_Nochars($Site_Unique),
    $TestSteps_api->CleanRSSData_NoTrailingSpace($Site_Unique),

//Article title 3
    $TestSteps_api->CleanRSSData_Nochars($Site_Unique),
    $TestSteps_api->CleanRSSData_NoTrailingSpace($Site_Unique),

//Article title 4
    $TestSteps_api->CleanRSSData_Nochars($Site_Unique),
    $TestSteps_api->CleanRSSData_NoTrailingSpace($Site_Unique),

//Article title 5
    $TestSteps_api->CleanRSSData_Nochars($Site_Unique),
    $TestSteps_api->CleanRSSData_NoTrailingSpace($Site_Unique),

//Article title 6
    $TestSteps_api->CleanRSSData_Nochars($Site_Unique),
    $TestSteps_api->CleanRSSData_NoTrailingSpace($Site_Unique),

//Article title 7
    $TestSteps_api->CleanRSSData_Nochars($Site_Unique),
    $TestSteps_api->CleanRSSData_NoTrailingSpace($Site_Unique)
);
