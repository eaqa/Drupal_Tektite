<?php
namespace Helper;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Api extends \Codeception\Module
{

    public function loadpage($link)
    {
       // $this->getModule('PhpBrowser')->_loadPage('POST', $link);
        $this->getModule('PhpBrowser')->_loadPage('POST', $link);

    }

    public function assertContains($haystack, $needle, $message = '')
    {
        \PHPUnit_Framework_Assert::assertContains($haystack, $needle);
    }

    public function assertNotContains($haystack, $needle, $message = '')
    {
        \PHPUnit_Framework_Assert::assertNotContains($haystack, $needle, $message = '');
    }

    public function assertGreaterThan($expected, $actual, $message = '')
    {
        \PHPUnit_Framework_Assert::assertGreaterThan($expected, $actual, $message);
    }

    public function assertGreaterThanOrEqual($expected, $actual, $message = '')
    {
        \PHPUnit_Framework_Assert::assertGreaterThanOrEqual($expected, $actual, $message);
    }

    public function assertLessThanorEqual($expected, $actual, $message = '')
    {
        \PHPUnit_Framework_Assert::assertLessThanOrEqual($expected, $actual, $message);
    }

    public function assertTrue($condition, $message = '')
    {
        \PHPUnit_Framework_Assert::assertTrue($condition);
    }

    public function Grab_XML_feed($BASE_URL)
    {
        $request_url = ($BASE_URL . '/rss/xml');
        $xml = simplexml_load_file($request_url);
        if (!$xml) {
            \PHPUnit_Framework_Assert::fail("Got error when grabbing RSS feed");
        }
        return $xml;
    }

    function getRandomArrayElement($array)
    {
        return $array[array_rand($array)];
    }

    function seeDomHasElement($Domelement)
    {
        try {
            $Domelement;
        } catch (\PHPUnit_Framework_AssertionFailedError $f) {
            return false;
        }
        return true;
    }

    function remove_http($url) {
        $disallowed = array('http://', 'https://');
        foreach($disallowed as $d) {
            if(strpos($url, $d) === 0) {
                return str_replace($d, '', $url);
            }
        }
        return $url;
    }
}
