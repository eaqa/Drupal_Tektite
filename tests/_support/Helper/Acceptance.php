<?php
namespace Helper;


use Page\Pageobj_common;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{
    public function dontSeeFieldIsEmpty($value)
    {
        $this->assertFalse(empty($value));
    }

    public function assertNotEquals($expected, $actual, $message = '', $delta = '')
    {
        \PHPUnit_Framework_Assert::assertNotEquals($expected, $actual, $message, $delta);
    }

    public function assertEquals($expected, $actual, $message = '', $delta = '')
    {
        \PHPUnit_Framework_Assert::assertEquals($expected, $actual, $message, $delta);
    }

    public function seeFileExists($filepath)
    {
        \PHPUnit_Framework_Assert::assertFileExists($filepath);
    }

    function seePageHasElement($element)
    {
        try {
            $this->getModule('WebDriver')->seeElement($element);
        } catch (\PHPUnit_Framework_AssertionFailedError $f) {
            return false;
        }
        return true;
    }

}
