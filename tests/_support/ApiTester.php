<?php
Use Codeception\Util\Locator;
use Sincco\Tools\Curl;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

    public function Validate_httpStatus_mainNav(\Step\Api\httpResponseCheck $TestSteps, $URL,
                                                $mainNavLocator)
    {
        //Used to test main navigation -- 404 tests
        $I = $this;
        $I->amOnUrl($URL);
        $TestSteps->Validate_httpStatus_mainNav($TestSteps, $URL, $mainNavLocator);
    }

    public function Validate_httpStatus_subNav(\Step\Api\httpResponseCheck $TestSteps, $site,
                                               $SubNavlocator, $uniqueSubLink = '-')
    {
        //Used to test Sub navigation -- 404 tests
        $I = $this;
        $TestSteps->Validate_httpStatus_subNav($TestSteps, $site, $SubNavlocator, $uniqueSubLink);
    }

    public function GrabandClean_ArticleTitle_inRSS_feed(\Step\Api\NewsletterTool $TestSteps, $BASE_URL)
    {
        $I = $this;
        $TestSteps->GrabRSSdata($BASE_URL);
        $TestSteps->CleanRSSData_Nochars($BASE_URL);
        $TestSteps->CleanRSSData_NoTrailingSpace($BASE_URL);

    }

    public function Custom404pg(Step\Api\BaseFunctions_api $Base_api, $BaseURL,
    $custom404Blck)
    {
        $I = $this;
        //Verifying response code - api method
        $Base_api->loadpage($BaseURL . 'fakePage');
        $I->canseeResponseCodeIs('404');

        //verifying on actual frontend of page - webdriver
        $I->amOnUrl($BaseURL . 'fakePage');
        $I->waitForElement($custom404Blck);
        $I->canSeeElement($custom404Blck);
        $I->canSeeInTitle('Page Not Found |');
    }

    public function grabArticleURL(Step\Api\DataFeeds $feeds, $BASE_URL)
    {

        $I = $this;
        $xml = $I->Grab_XML_feed($BASE_URL);
        $response_array = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($xml), true);
        $y = $response_array['channel']['item'];
        $cnt = count($y);

        $i = 0;
        $links = array();
        while ($i < $cnt) {
            $i++;
            $links[] = $response_array['channel']['item'][$i - 1]['link'];
        }
        //print_r($links);
        $link = $I->getRandomArrayElement($links);
        $encodedLink = urlencode($link);
        return $encodedLink;
    }

    public function grabArticleKeyWords(\Step\Api\Solr $TestSteps, $articleURL)
    {
        $I = $this;
        $array =
        $TestSteps->fetchArticleData($articleURL); //fetches raw data from given article url
        $keyword =
        $I->getRandomArrayElement($array); //returns singular keyword (from array list - of more then 10)
        //print $keyword;
        $ch = curl_init();
        $keyord_encoded = curl_escape($ch, $keyword);
        return $keyord_encoded;

    }
}
