<?php
namespace Page;

class Pageobj_NavLinks
{

    //Navigation Links Verification

    //Global Locators
    #Global Main navigation menu
    public static $NavLocator_main = "//*[@id=\"block-mainnavigation\"]/ul/li/a";
    #Global Top Navigation menu
    public static $NavLocator_top = '//*[@id="block-topmenu"]/ul/li/a';
    #Global Top Social Media Links
    public static $NavLocator_topSoc = '//*[@id="block-tektitesocialshareblock"]/a';
    #Global bottom/footer Social Media Links
    public static $NavLocator_bottomSoc = '//*[@id="questex"]/div/a';
    #Global footer nav menu
    public static $NavLocator_footerNav = '//*[@id="block-tektitefooterblock"]/ul/li/a';


    //------------------FierceBiotech------------------------//

    public static $Main_navLinks_fbt = array
    (
        'https://www.fiercebiotech.com/' => array(

            'https://www.fiercebiotech.com/biotech' => Array
            (
                'https://www.fiercebiotech.com/biotech/r-d',
                'https://www.fiercebiotech.com/biotech/deals',
                'https://www.fiercebiotech.com/biotech/regulatory',
                'https://www.fiercebiotech.com/biotech/venture-capital',
                'https://www.fiercebiotech.com/biotech/outsourcing',
                'https://www.fiercebiotech.com/biotech/partnering',
                'https://www.fiercebiotech.com/biotech/financials'
            ),
            'https://www.fiercebiotech.com/research' => Array
            (
                'https://www.fiercebiotech.com/research/r-d',
                'https://www.fiercebiotech.com/research/deals',
                'https://www.fiercebiotech.com/research/regulatory',
                'https://www.fiercebiotech.com/research/personalized-medicine'
            ),
            'https://www.fiercebiotech.com/it' => Array
            (
                'https://www.fiercebiotech.com/it/deals',
                'https://www.fiercebiotech.com/it/r-d',
                'https://www.fiercebiotech.com/it/data-management',
                'https://www.fiercebiotech.com/it/regulatory',
                'https://www.fiercebiotech.com/it/genomics',
                'https://www.fiercebiotech.com/it/digital',
                'https://www.fiercebiotech.com/it/social-media'
            ),
            'https://www.fiercebiotech.com/cro' => Array
            (
                'https://www.fiercebiotech.com/cro/r-d',
                'https://www.fiercebiotech.com/cro/deals',
                'https://www.fiercebiotech.com/cro/regulatory',
                'https://www.fiercebiotech.com/cro/partnering',
                'https://www.fiercebiotech.com/cro/financials',
                'https://www.fiercebiotech.com/cro/manufacturing'
            ),

            'https://www.fiercebiotech.com/medtech' => Array
            (
                'https://www.fiercebiotech.com/medtech/deals',
                'https://www.fiercebiotech.com/medtech/financials',
                'https://www.fiercebiotech.com/medtech/r-d',
                'https://www.fiercebiotech.com/medtech/regulatory'
            )
        )
    );

    public static $Top_navLinks_fbt = array
    (
            'http://www.fiercedrugdevforum.com/',
            'http://www.fiercepharma.com/',
            'https://jobs.fiercebiotech.com/',
            'https://www.fiercebiotech.com/resources',
            'https://www.fiercebiotech.com/events',
            'https://pages.questexweb.com/FierceBiotech-Newsletter-Signup.html'
    );

    public static $Top_SocLinks_fbt = array
    (
        'https://twitter.com/fiercebiotech',
        'https://www.linkedin.com/groups/3560990',
        'https://www.facebook.com/fiercebiotech/'
    );

    public static $Bottom_SocLinks_fbt = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_fbt = array
    (
        'https://www.fiercebiotech.com/',
        'https://pages.questexweb.com/FierceBiotech-Newsletter-Signup.html',
        'https://pages.questexweb.com/Manage-Subscriptions_Manage-Your-Subscriptions.html?',
        'https://www.fiercebiotech.com/advertise-us',
        'https://www.fiercebiotech.com/rss-feeds',
        'http://www.questex.com/privacy-policy',
        'https://www.fiercebiotech.com/about-us'
    );

    //------------------FierceCable------------------------//

    public static $Main_navLinks_fc = array
    (
        'https://www.fiercecable.com/' => array(

            'https://www.fiercecable.com/cable' => Array
            (
                'https://www.fiercecable.com/cable/programming',
                'https://www.fiercecable.com/cable/operators',
                'https://www.fiercecable.com/cable/regulatory',
                'https://www.fiercecable.com/cable/financial'
            ),
            'https://www.fiercecable.com/video' => Array
            (
                "https://www.fiercecable.com/video/broadcasting",
                "https://www.fiercecable.com/video/tech"
            ),

        )
    );

    public static $Top_navLinks_fc = array
    (
        'http://www.paytvshow.com/',
        'http://www.fiercetelecom.com/',
        'http://www.fiercewireless.com/',
        'https://www.fiercecable.com/resources',
        'https://www.fiercecable.com/events',
        'http://jobs.fiercewireless.com/',
        'https://pages.questexweb.com/FierceCable-Newsletter-Signup.html'
    );

    public static $Top_SocLinks_fc = array
    (
        'https://www.linkedin.com/groups/8158264/profile',
        'https://www.facebook.com/fiercecable/?fref=nf',
        'https://twitter.com/FierceCable?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor'
    );

    public static $Bottom_SocLinks_fc = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_fc = array
    (
        'https://www.fiercecable.com/',
        'https://pages.questexweb.com/FierceCable-Newsletter-Signup.html',
        'https://pages.questexweb.com/Manage-Subscriptions_Manage-Your-Subscriptions.html?&_ga=1.138251120.1944058679.1465391333',
        'https://www.fiercecable.com/advertise-us',
        'https://www.fiercecable.com/rss-feeds',
        'http://www.questex.com/privacy-policy',
        'https://www.fiercecable.com/about-us'

    );

    //------------------Response Mag------------------------//

    public static $Main_navLinks_response = array
    (
        'https://www.responsemagazine.com/' => array(

            'http://www.response-digital.com/response/november_2017' => Array
            (

            ),
            'https://www.responsemagazine.com/media' => Array
            (

            ),
            'https://www.responsemagazine.com/technology' => Array
            (

            ),

            'https://www.responsemagazine.com/commerce' => Array
            (

            ),
            'https://www.responsemagazine.com/research' => Array
            (

            ),
            'https://www.responsemagazine.com/drma-voice' => Array
            (

            ),
        )
    );

    public static $Top_navLinks_response = array
    (
        'http://www.mtcshow.com/',
        'http://www.thedrma.com/',
        'https://www.responsemagazine.com/topic/web-exclusive',
        'https://www.responsemagazine.com/events',
        'https://responsemagazine-jobs.careerwebsite.com/',
        'https://pages.questexweb.com/Response-Newsletter-Signup.html'
    );

    public static $Top_SocLinks_response = array
    (
        'https://www.linkedin.com/groups/3608863',
        'https://twitter.com/responsemag',
        'https://www.facebook.com/responsemagazine/',
    );

    public static $Bottom_SocLinks_response = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_response = array
    (
        'https://www.responsemagazine.com/',
        'http://www.mtcshow.com/',
        'https://pages.questexweb.com/Response-Newsletter-Signup.html',
        'https://www.responsemagazine.com/about-us',
        'https://www.responsemagazine.com/advertise-us'
    );

    //------------------FierceHealthCare------------------------//

    public static $Main_navLinks_fhc = array
    (
        'https://www.fiercehealthcare.com/' => array(

            'https://www.fiercehealthcare.com/healthcare' => Array
            (
                'https://www.fiercehealthcare.com/healthcare/ambulatory-care',
                'https://www.fiercehealthcare.com/healthcare/finance',
                'https://www.fiercehealthcare.com/healthcare/hospitals',
                'https://www.fiercehealthcare.com/healthcare/patient-engagement',
                'https://www.fiercehealthcare.com/healthcare/population-health',
                'https://www.fiercehealthcare.com/healthcare/practices',
                'https://www.fiercehealthcare.com/healthcare/regulatory'
            ),
            'https://www.fiercehealthcare.com/it' => Array
            (
                'https://www.fiercehealthcare.com/it/analytics',
                'https://www.fiercehealthcare.com/it/ehr',
                'https://www.fiercehealthcare.com/it/mobile',
                'https://www.fiercehealthcare.com/it/privacy-security',
                'https://www.fiercehealthcare.com/it/regulatory'
            ),
            'https://www.fiercehealthcare.com/payer' => Array
            (
                'https://www.fiercehealthcare.com/payer/aca',
                'https://www.fiercehealthcare.com/payer/antifraud',
                'https://www.fiercehealthcare.com/payer/cms-chip',
                'https://www.fiercehealthcare.com/payer/member-engagement',
                'https://www.fiercehealthcare.com/payer/population-health',
                'https://www.fiercehealthcare.com/payer/regulatory',
                'https://www.fiercehealthcare.com/payer/technology'
            ),
        )
    );

            public static $Top_navLinks_fhc = array
            (
                'http://jobs.fiercehealthcare.com/',
                'https://www.fiercehealthcare.com/resources',
                'https://www.fiercehealthcare.com/events',
                'https://pages.questexweb.com/FierceHealthcare-Newsletter-Signup.html'
        );

    public static $Top_SocLinks_fhc = array
    (
        'https://www.linkedin.com/groups/3349257/profile',
        'https://twitter.com/FierceHealth',
        'https://www.facebook.com/FierceHealthcare/'
    );
    public static $Bottom_SocLinks_fhc = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_fhc = array
    (
        'https://www.fiercehealthcare.com/',
        'https://pages.questexweb.com/FierceHealthcare-Newsletter-Signup.html',
        'https://pages.questexweb.com/Manage-Subscriptions_Manage-Your-Subscriptions.html?%22%20target=%22_blank',
        'https://www.fiercehealthcare.com/advertise-us',
        'https://www.fiercehealthcare.com/rss-feeds',
        'http://www.questex.com/privacy-policy',
        'https://www.fiercehealthcare.com/about-us',
    );

    //------------------FiercePharma------------------------//

    public static $Main_navLinks_fp = array
    (
        'https://www.fiercepharma.com/' => array(

            'https://www.fiercepharma.com/pharma' => Array
            (
                'https://www.fiercepharma.com/pharma/m-a',
                'https://www.fiercepharma.com/pharma/regulatory',
                'https://www.fiercepharma.com/pharma/financials',
                'https://www.fiercepharma.com/pharma/corporate',
                'https://www.fiercepharma.com/pharma/legal'
            ),
            'https://www.fiercepharma.com/manufacturing' => Array
            (
                'https://www.fiercepharma.com/manufacturing/m-a',
                'https://www.fiercepharma.com/manufacturing/outsourcing',
                'https://www.fiercepharma.com/manufacturing/regulatory',
                'https://www.fiercepharma.com/manufacturing/supply-chain',
                'https://www.fiercepharma.com/manufacturing/partnering',
                'https://www.fiercepharma.com/manufacturing/drug-safety'
            ),
            'https://www.fiercepharma.com/marketing' => Array
            (
                'https://www.fiercepharma.com/marketing/regulatory',
                'https://www.fiercepharma.com/marketing/dtc-advertising',
                'https://www.fiercepharma.com/marketing/digital-and-social-media',
                'https://www.fiercepharma.com/marketing/data-and-analytics',
                'https://www.fiercepharma.com/marketing/launches'
            ),
            'https://www.fiercepharma.com/pharma-asia' => Array
            (
                'https://www.fiercepharma.com/pharma-asia/m-a',
                'https://www.fiercepharma.com/pharma-asia/r-d',
                'https://www.fiercepharma.com/pharma-asia/regulatory',
                'https://www.fiercepharma.com/pharma-asia/sales-and-marketing',
                'https://www.fiercepharma.com/pharma-asia/financials',
                'https://www.fiercepharma.com/pharma-asia/manufacturing',
            ),
            'https://www.fiercepharma.com/animal-health' => Array
            (
                'https://www.fiercepharma.com/animal-health/r-d',
                'https://www.fiercepharma.com/animal-health/m-a',
                'https://www.fiercepharma.com/animal-health/regulatory',
                'https://www.fiercepharma.com/animal-health/veterinarian',
                'https://www.fiercepharma.com/animal-health/financials',
                'https://www.fiercepharma.com/animal-health/vaccines'
            ),
            'https://www.fiercepharma.com/drug-delivery' => Array
            (
                'https://www.fiercepharma.com/drug-delivery/r-d',
                'https://www.fiercepharma.com/drug-delivery/regulatory',
                'https://www.fiercepharma.com/drug-delivery/partnering'
            ),
            'https://www.fiercepharma.com/vaccines' => Array
            (
                'https://www.fiercepharma.com/vaccines/deals',
                'https://www.fiercepharma.com/vaccines/infectious-diseases',
                'https://www.fiercepharma.com/vaccines/r-d',
                'https://www.fiercepharma.com/vaccines/regulatory'
            ),
        )
    );

    public static $Top_navLinks_fp = array
    (
        'http://www.fiercedrugdevforum.com/',
        'http://www.fiercebiotech.com/',
        'http://jobs.fiercebiotech.com/',
        'https://www.fiercepharma.com/resources',
        'https://www.fiercepharma.com/events',
        'https://pages.questexweb.com/FiercePharma-Newsletter-Signup.html'
    );

    public static $Top_SocLinks_fp = array
    (
        'https://twitter.com/fiercepharma',
        'https://www.linkedin.com/groups/5042642'
    );

    public static $Bottom_SocLinks_fp = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_fp = array
    (
        'https://www.fiercepharma.com/',
        'https://pages.questexweb.com/FiercePharma-Newsletter-Signup.html',
        'https://pages.questexweb.com/Manage-Subscriptions_Manage-Your-Subscriptions.html?',
        'https://www.fiercepharma.com/advertise-us',
        'https://www.fiercepharma.com/rss-feeds',
        'http://www.questex.com/privacy-policy',
        'https://www.fiercepharma.com/about-us'
    );

//------------------FierceRetail------------------------//

    public static $Main_navLinks_fr = array
    (
        'https://www.fierceretail.com/' => array(

            'https://www.fierceretail.com/stores' => Array
            (

            ),
            'https://www.fierceretail.com/digital' => Array
            (

            ),
            'https://www.fierceretail.com/technology' => Array
            (

            ),

            'https://www.fierceretail.com/operations' => Array
            (

            ),
         )
    );

    public static $Top_navLinks_fr = array
    (
        'https://www.fierceretail.com/resources',
        'https://www.fierceretail.com/events',
        'https://pages.questexweb.com/FierceRetail-Newsletter-Signup.html'
    );

    public static $Top_SocLinks_fr = array
    (
        'https://www.linkedin.com/groups/6695224',
        'https://twitter.com/fierceretail'
    );

    public static $Bottom_SocLinks_fr = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_fr = array
    (
        'https://www.fierceretail.com/',
        'https://pages.questexweb.com/FierceRetail-Newsletter-Signup.html',
        'https://pages.questexweb.com/Manage-Subscriptions_Manage-Your-Subscriptions.html?&_ga=1.138251120.1944058679.1465391333',
        'https://www.fierceretail.com/advertise-us',
        'https://www.fierceretail.com/rss-feeds',
        'http://www.questex.com/privacy-policy',
        'https://www.fierceretail.com/about-us'
    );

    //------------------FierceTelecom------------------------//

    public static $Main_navLinks_ft = array
    (
        'https://www.fiercetelecom.com/' => array(

            'https://www.fiercetelecom.com/telecom' => Array
            (
                'https://www.fiercetelecom.com/telecom/operators',
                'https://www.fiercetelecom.com/telecom/regulatory',
                'https://www.fiercetelecom.com/telecom/financial',
            ),
            'https://www.fiercetelecom.com/installer' => Array
            (
            ),
            'https://www.fiercetelecom.com/enterprise' => Array
            (
            ),
        )
    );

    public static $Top_navLinks_ft = array
    (
        'http://www.fiercecable.com/',
        'http://www.fiercewireless.com/',
        'https://www.fiercetelecom.com/resources',
        'https://www.fiercetelecom.com/events',
        'http://jobs.fiercewireless.com/',
        'https://pages.questexweb.com/FierceTelecom-Newsletter-Signup.html'
    );

    public static $Top_SocLinks_ft = array
    (
        'https://www.linkedin.com/groups/3467187/profile',
        'https://www.facebook.com/FierceTelecom-142303065859229/?fref=nf',
        'https://twitter.com/FierceTelecom?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor'
    );

    public static $Bottom_SocLinks_ft = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_ft = array
    (
        'https://www.fiercetelecom.com/',
        'https://pages.questexweb.com/FierceTelecom-Newsletter-Signup.html',
        'https://pages.questexweb.com/Manage-Subscriptions_Manage-Your-Subscriptions.html?&_ga=1.138251120.1944058679.1465391333',
        'https://www.fiercetelecom.com/advertise-us',
        'https://www.fiercetelecom.com/rss-feeds',
        'http://www.questex.com/privacy-policy',
        'https://www.fiercetelecom.com/about-us'
    );

    //------------------FierceWireless------------------------//

    public static $Main_navLinks_fw = array
    (
        'https://www.fiercewireless.com/' => array(

            'https://www.fiercewireless.com/wireless' => Array
            (
                'https://www.fiercewireless.com/wireless/devices',
                'https://www.fiercewireless.com/wireless/operators',
                'https://www.fiercewireless.com/wireless/regulatory',
                'https://www.fiercewireless.com/wireless/financial'
            ),
            'https://www.fiercewireless.com/tech' => Array
            (

            ),
            'https://www.fiercewireless.com/europe' => Array
            (
                'https://www.fiercewireless.com/europe/devices',
                'https://www.fiercewireless.com/europe/operators',
                'https://www.fiercewireless.com/europe/regulatory',
                'https://www.fiercewireless.com/europe/financials'
            ),

            'https://www.fiercewireless.com/developer' => Array
            (

            ),
            'https://www.fiercewireless.com/5g' => Array
            (

            ),
            'https://www.fiercewireless.com/iot' => Array
            (

            ),
        )
    );

    public static $Top_navLinks_fw = array
    (
        'http://www.fiercecable.com/',
        'http://www.fiercetelecom.com/',
        'https://www.fiercewireless.com/resources',
        'https://www.fiercewireless.com/events',
        'http://jobs.fiercewireless.com/',
        'https://pages.questexweb.com/FierceWireless-Newsletter-Signup.html'
    );

    public static $Top_SocLinks_fw = array
    (
        'https://www.linkedin.com/groups/3467168/profile',
        'https://www.facebook.com/FierceWireless-140288599391026/?fref=nf',
        'https://twitter.com/FierceWireless?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor'
    );

    public static $Bottom_SocLinks_fw = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_fw = array
    (
        'https://www.fiercewireless.com/',
        'https://pages.questexweb.com/FierceWireless-Newsletter-Signup.html',
        'https://pages.questexweb.com/Manage-Subscriptions_Manage-Your-Subscriptions.html?&_ga=1.138251120.1944058679.1465391333',
        'https://www.fiercewireless.com/advertise-us',
        'https://www.fiercewireless.com/rss-feeds',
        'http://www.questex.com/privacy-policy',
        'https://www.fiercewireless.com/about-us'
    );

    //------------------Hotel Mangement------------------------//

    public static $Main_navLinks_hm = array
    (
        'https://www.hotelmanagement.net/' => array(

            'https://www.hotelmanagement.net/own' => Array
            (
                'https://www.hotelmanagement.net/own/asset-management',
                'https://www.hotelmanagement.net/own/development',
                'https://www.hotelmanagement.net/own/financing',
                'https://www.hotelmanagement.net/own/franchising',
                'https://www.hotelmanagement.net/own/openings',
                'https://www.hotelmanagement.net/own/transactions'
            ),
            'https://www.hotelmanagement.net/operate' => Array
            (
                'https://www.hotelmanagement.net/operate/energy-management',
                'https://www.hotelmanagement.net/operate/food-beverage',
                'https://www.hotelmanagement.net/operate/guest-relations',
                'https://www.hotelmanagement.net/operate/housekeeping',
                'https://www.hotelmanagement.net/operate/human-resources',
                'https://www.hotelmanagement.net/operate/legal',
                'https://www.hotelmanagement.net/operate/maintenance',
                'https://www.hotelmanagement.net/operate/revenue-management',
                'https://www.hotelmanagement.net/operate/sales-marketing'
            ),
            'https://www.hotelmanagement.net/tech' => Array
            (
                'https://www.hotelmanagement.net/tech/room-entertainment',
                'https://www.hotelmanagement.net/tech/property-management',
                'https://www.hotelmanagement.net/tech/security',
                'https://www.hotelmanagement.net/tech/telecom',
                'https://www.hotelmanagement.net/tech/wireless'
            ),

            'https://www.hotelmanagement.net/design' => Array
            (
                'https://www.hotelmanagement.net/design/construction',
                'https://www.hotelmanagement.net/design/conversions',
                'https://www.hotelmanagement.net/design/design-trends',
                'https://www.hotelmanagement.net/design/new-build',
                'https://www.hotelmanagement.net/design/renovations'
            ),
            'https://www.hotelmanagement.net/procure' => Array
            (
                'https://www.hotelmanagement.net/procure/ff-e',
                'https://www.hotelmanagement.net/procure/marketplace',
                'https://www.hotelmanagement.net/procure/os-e'
            ),
        )
    );

    public static $Top_navLinks_hm = array
    (
      //Desktop Menu
      'https://www.hotelmanagement.net/resources',
      'https://www.hotelmanagement.net/events',
      'https://pages.questexweb.com/HotelManagement-Newsletter-Signup.html',
      'https://www.hotelmanagement.net/subscribe',
        'http://www.hotelmanagementdigital.com/',
      //Mobile Menu
      'https://www.hotelmanagement.net/resources',
      'https://www.hotelmanagement.net/events',
      'https://pages.questexweb.com/HotelManagement-Newsletter-Signup.html',
      'https://www.hotelmanagement.net/subscribe',
        'http://www.hotelmanagementdigital.com/',
    );

    public static $Top_SocLinks_hm = array
    (
        //Desktop
        'https://www.linkedin.com/company/hotel-management-magazine',
        'https://www.facebook.com/HotelMgmt/',
        'https://twitter.com/HotelMgmtMag',
        //Mobile
        'https://www.linkedin.com/company/hotel-management-magazine',
        'https://www.facebook.com/HotelMgmt/',
        'https://twitter.com/HotelMgmtMag'
    );

    public static $Bottom_SocLinks_hm = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_hm = array
    (
        'https://www.hotelmanagement.net/',
        'https://pages.questexweb.com/HotelManagement-Newsletter-Signup.html?_ga=1.180077988.1629189661.1464192832',
        'https://www.hotelmanagement.net/subscribe',
        'https://www.hotelmanagement.net/about-us',
        'https://www.hotelmanagement.net/advertise-us',
        'https://www.hotelmanagement.net/rss-feeds',
        'http://www.questex.com/content/questex-terms-use',
        'http://www.questex.com/privacy-policy'
    );

    //------------------Luxury Travel Advisor------------------------//

    public static $Main_navLinks_lta = array
    (
        'https://www.luxurytraveladvisor.com/' => array(


            'https://www.luxurytraveladvisor.com/destinations' => Array
            (

            ),
            'https://www.luxurytraveladvisor.com/hotels' => Array
            (

            ),
            'https://www.luxurytraveladvisor.com/cruises' => Array
            (

            ),
            'https://www.luxurytraveladvisor.com/transportation' => Array
            (

            ),
            'https://www.luxurytraveladvisor.com/tours' => Array
            (

            ),
            'https://www.luxurytraveladvisor.com/aspirational' => Array
            (

            ),
            'https://www.luxurytraveladvisor.com/people' => Array
            (

            ),
            'https://www.luxurytraveladvisor.com/running-your-business' => Array
            (

            ),
            'https://www.luxurytraveladvisor.com/resources' => Array
            (
                'https://www.luxurytraveladvisor.com/products/51',
                'https://www.luxurytraveladvisor.com/products/2981',
                'https://www.luxurytraveladvisor.com/products/3351',
                'https://www.luxurytraveladvisor.com/products/43161'
            ),
        )
    );

    public static $Top_navLinks_lta = array
    (
        //Desktop
        'https://pages.questexweb.com/LTA-Newsletter-Signup.html',
        'http://www.luxurytraveldigital.com/publication/?m=17817&l=1',
        'https://www.luxurytraveladvisor.com/focus-series',
        'http://www.travelagentuniversity.com/',
        'http://www.ltaultrasummit.com/',
        'http://www.travelagentcentral.com/',
        'http://www.travelindustryexchange.com/',
        //Mobile
        'https://pages.questexweb.com/LTA-Newsletter-Signup.html',
        'http://www.luxurytraveldigital.com/publication/?m=17817&l=1',
        'https://www.luxurytraveladvisor.com/focus-series',
        'http://www.travelagentuniversity.com/',
        'http://www.ltaultrasummit.com/',
        'http://www.travelagentcentral.com/',
        'http://www.travelindustryexchange.com/',
    );

    public static $Top_SocLinks_lta = array
    (
        //Desktop
        'https://www.instagram.com/luxurytraveladvisor/?hl=en',
        'https://www.facebook.com/LuxuryTravelAdvisor/',
        'https://twitter.com/LuxuryTAmag',
        'https://www.pinterest.com/LuxTravelAdvsr/',
        'https://www.linkedin.com/groups/4621900',
        //Mobile
        'https://www.instagram.com/luxurytraveladvisor/?hl=en',
        'https://www.facebook.com/LuxuryTravelAdvisor/',
        'https://twitter.com/LuxuryTAmag',
        'https://www.pinterest.com/LuxTravelAdvsr/',
        'https://www.linkedin.com/groups/4621900',
    );

    public static $Bottom_SocLinks_lta = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_lta = array
    (
        'https://www.luxurytraveladvisor.com/',
        'https://pages.questexweb.com/LTA-Newsletter-Signup.html',
        'https://www.luxurytraveladvisor.com/subscribe-to-our-print-editions',
        'http://www.questex.com/privacy-policy',
        'https://www.luxurytraveladvisor.com/about-us',
        'https://www.luxurytraveladvisor.com/advertise',
        'https://www.luxurytraveladvisor.com/rss-feeds',
    );

    //------------------Travel Agent Central------------------------//

    public static $Main_navLinks_tac = array
    (
        'https://www.travelagentcentral.com/' => array(

            'https://www.travelagentcentral.com/destinations' => Array
            (

            ),
            'https://www.travelagentcentral.com/hotels' => Array
            (

            ),
            'https://www.travelagentcentral.com/cruises' => Array
            (

            ),

            'https://www.travelagentcentral.com/transportation' => Array
            (

            ),
            'https://www.travelagentcentral.com/tours' => Array
            (

            ),
            'https://www.travelagentcentral.com/people' => Array
            (

            ),
            'https://www.travelagentcentral.com/running-your-business' => Array
            (

            ),
            'https://www.travelagentcentral.com/resources' => Array
            (
                'https://www.travelagentcentral.com/products/2016',
                'https://www.travelagentcentral.com/products/2456',
                'https://www.travelagentcentral.com/products/2461',
                'https://www.travelagentcentral.com/products/4791',
                'https://www.travelagentcentral.com/products/52871'
            ),
        )
    );

    public static $Top_navLinks_tac = array
    (
        //Desktop
        'https://pages.questexweb.com/TAC-Newsletter-Signup.html',
        'http://www.travelagentmagazinedigital.com/publication?m=17816&amp;l=1',
        'https://www.travelagentcentral.com/focus-series',
        'http://www.travelagentuniversity.com/',
        'http://www.luxurytraveladvisor.com/',
        'http://www.travelindustryexchange.com/',
        //Mobile
        'https://pages.questexweb.com/TAC-Newsletter-Signup.html',
        'http://www.travelagentmagazinedigital.com/publication?m=17816&amp;l=1',
        'https://www.travelagentcentral.com/focus-series',
        'http://www.travelagentuniversity.com/',
        'http://www.luxurytraveladvisor.com/',
        'http://www.travelindustryexchange.com/'
    );

    public static $Top_SocLinks_tac = array
    (
        //Desktop
        'https://www.facebook.com/TravelAgentMagazine/',
        'https://twitter.com/TravelAgentMag',
        'https://www.linkedin.com/groups/4782086',
        'https://www.pinterest.com/travelagentmag/',
        'https://www.instagram.com/travelagentcentral/',
        //Mobile
        'https://www.facebook.com/TravelAgentMagazine/',
        'https://twitter.com/TravelAgentMag',
        'https://www.linkedin.com/groups/4782086',
        'https://www.pinterest.com/travelagentmag/',
        'https://www.instagram.com/travelagentcentral/'
    );

    public static $Bottom_SocLinks_tac = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_tac = array
    (
        'https://www.travelagentcentral.com/',
        'https://pages.questexweb.com/TAC-Newsletter-Signup.html',
        'https://www.travelagentcentral.com/subscribe-to-our-print-editions',
        'http://www.questex.com/privacy-policy',
        'https://www.travelagentcentral.com/about-us',
        'https://www.travelagentcentral.com/advertise',
        'https://www.travelagentcentral.com/rss-feeds',
    );

    //------------------American Spa------------------------//

    public static $Main_navLinks_Amspa = array
    (
        'https://www.americanspa.com/' => array(

            'https://www.americanspa.com/wellness' => Array
            (

            ),
            'https://www.americanspa.com/spas' => Array
            (

            ),
            'https://www.americanspa.com/treatments' => Array
            (

            ),

            'https://www.americanspa.com/skincare' => Array
            (

            ),
            'https://www.americanspa.com/medical-spa' => Array
            (

            ),
            'https://www.americanspa.com/business' => Array
            (

            ),
            'https://www.americanspa.com/news' => Array
            (

            ),
            'https://www.americanspa.com/people' => Array
            (
            ),
            'https://www.americanspa.com/estheticians' => Array
            (
    )));

    public static $Top_navLinks_Amspa = array
    (
        //Desktop
        'http://americanspainstitute.com/',
        'http://www.americanspadigital.com/view/questex/american-spa/december-2017.96',
        'http://www.americanspadigital.com/publication/?i=339542&ver=html5#{%22view%22:%22issuelistBrowser%22,%22issue_id%22:%22339542%22}',
        'https://www.americanspa.com/products/131',
        'https://www.americanspa.com/products/861',
        'https://www.americanspa.com/event-calendar',
        'https://pages.questexweb.com/AmericanSpa-Newsletter-Signup.html',
        //Mobile
        'http://americanspainstitute.com/',
        'http://www.americanspadigital.com/view/questex/american-spa/december-2017.96',
        'http://www.americanspadigital.com/publication/?i=339542&ver=html5#{%22view%22:%22issuelistBrowser%22,%22issue_id%22:%22339542%22}',
        'https://www.americanspa.com/products/131',
        'https://www.americanspa.com/products/861',
        'https://www.americanspa.com/event-calendar',
        'https://pages.questexweb.com/AmericanSpa-Newsletter-Signup.html'
    );

    public static $Top_SocLinks_Amspa = array
    (
        //Desktop
        'https://www.facebook.com/americanspa/',
        'https://instagram.com/americanspamag/',
        'https://twitter.com/americanspamag',
        'https://www.linkedin.com/groups/2992700',
        'https://www.pinterest.com/americanspa/',
        //Mobile
        'https://www.facebook.com/americanspa/',
        'https://instagram.com/americanspamag/',
        'https://twitter.com/americanspamag',
        'https://www.linkedin.com/groups/2992700',
        'https://www.pinterest.com/americanspa/'
    );

    public static $Bottom_SocLinks_Amspa = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_Amspa = array
    (
        'https://www.americanspa.com/',
        'https://pages.questexweb.com/AmericanSpa-Newsletter-Signup.html',
        'https://www.sub-forms.com/dragon/init.do?site=QTX5_AXland',
        'https://www.americanspa.com/advertise-us',
        'https://www.americanspa.com/rss-feeds',
        'https://www.americanspa.com/about-us',
        'http://www.questex.com/privacy-policy',
    );

    //------------------Sensors Mag------------------------//

    public static $Main_navLinks_sensorsMag = array
    (
        'https://www.sensorsmag.com/' => array(

            'https://www.sensorsmag.com/embedded' => Array
            (

            ),
            'https://www.sensorsmag.com/components' => Array
            (

            ),
            'https://www.sensorsmag.com/iot-wireless' => Array
            (

            ),
        )
    );

    public static $Top_navLinks_sensorsMag = array
    (
        //Desktop
        'https://www.sensorsmag.com/resources',
        'https://www.sensorsmag.com/sensors-events',
        'https://www.sensorsmag.com/products/26',
        'https://pages.questexweb.com/SensorsMag-Newsletter-Signup.html',
        'https://www.sensorsmag.com/topic/seventh-sense-blog',
        'https://www.sensorsmag.com/sensors-digital',
        //Mobile
        'https://www.sensorsmag.com/resources',
        'https://www.sensorsmag.com/sensors-events',
        'https://www.sensorsmag.com/products/26',
        'https://pages.questexweb.com/SensorsMag-Newsletter-Signup.html',
        'https://www.sensorsmag.com/topic/seventh-sense-blog',
        'https://www.sensorsmag.com/sensors-digital'
    );

    public static $Top_SocLinks_sensorsMag = array
    (
        //Desktop
        'https://www.facebook.com/SensorsMagazine',
        'https://twitter.com/sensorsonline',
        'https://www.linkedin.com/groups/Sensors-Magazine-4568519?trk=myg_ugrp_ovr',
        //Mobile
        'https://www.facebook.com/SensorsMagazine',
        'https://twitter.com/sensorsonline',
        'https://www.linkedin.com/groups/Sensors-Magazine-4568519?trk=myg_ugrp_ovr'
    );

    public static $Bottom_SocLinks_sensorsMag = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_sensorsMag = array
    (
        'https://www.sensorsmag.com/',
        'https://pages.questexweb.com/SensorsMag-Newsletter-Signup.html',
        'https://pages.questexweb.com/Manage-Your-Subscriptions_Manage-Your-Subscriptions.html',
        'https://www.sensorsmag.com/editorial',
        'https://www.sensorsmag.com/about-us',
        'https://www.sensorsmag.com/advertise',
        'https://www.sensorsmag.com/rss-feeds',
        'http://www.questex.com/privacy-policy'
    );

    //------------------American Salon------------------------//

    public static $Main_navLinks_salon = array
    (
        'https://www.americansalon.com/' => array(

            'https://www.americansalon.com/news' => Array
            (

            ),
            'https://www.americansalon.com/hair' => Array
            (

            ),
            'https://www.americansalon.com/skin-nails' => Array
            (

            ),
            'https://www.americansalon.com/business-career' => Array
            (

            ),

            'https://www.americansalon.com/digital-social' => Array
            (

            ),
            'https://www.americansalon.com/videos-podcasts' => Array
            (

            )
        )
    );

    public static $Top_navLinks_salon = array
    (   //Desktop
        'http://www.americansalondigital.com/publication/?i=451934&ver=html5',
        'https://www.sub-forms.com/dragon/init.do?site=QTX4_AHland',
        'https://pages.questexweb.com/AmericanSalon-Newsletter-Signup.html',
        //Mobile
        'http://www.americansalondigital.com/publication/?i=451934&ver=html5',
        'https://www.sub-forms.com/dragon/init.do?site=QTX4_AHland',
        'https://pages.questexweb.com/AmericanSalon-Newsletter-Signup.html'

    );

    public static $Top_SocLinks_salon = array
    (   //Desktop
        'https://www.facebook.com/AmericanSalon',
        'https://instagram.com/american_salon',
        'https://twitter.com/americansalon',
        'https://www.youtube.com/channel/UCNIoaazACPbCPYvC7EV2mqQ',
        //Mobile
        'https://www.facebook.com/AmericanSalon',
        'https://instagram.com/american_salon',
        'https://twitter.com/americansalon',
        'https://www.youtube.com/channel/UCNIoaazACPbCPYvC7EV2mqQ'
    );

    public static $Bottom_SocLinks_salon = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_salon = array
    (
        'https://www.americansalon.com/',
        'https://pages.questexweb.com/AmericanSalon-Newsletter-Signup.html',
        'https://www.sub-forms.com/dragon/init.do?site=QTX4_AHland',
        'https://pages.questexweb.com/Media-Kit-Entries_AmericanSalon-MediaKit-LP.html',
        'https://www.americansalon.com/rss-feeds',
        'http://www.questex.com/privacy-policy',
        'https://www.americansalon.com/about-us'
    );

    //------------------NightClub------------------------//

    public static $Main_navLinks_nc = array
    (
        'https://www.nightclub.com/' => array(

            'https://www.nightclub.com/operations' => Array
            (

            ),
            'https://www.nightclub.com/food-drink' => Array
            (

            ),
            'https://www.nightclub.com/products' => Array
            (

            ),

            'https://www.nightclub.com/promoting' => Array
            (

            ),
            'https://www.nightclub.com/industry-news' => Array
            (

            ),

        )
    );

    public static $Top_navLinks_nc = array
    (   //Desktop
        'http://www.ncbshow.com/',
        'http://www.vibeconference.com/',
        'https://www.nightclub.com/editorial-calendar',
        'https://www.nightclub.com/resources',
        'https://pages.questexweb.com/NCB-Newsletter-Signup.html',
        //Mobile
        'http://www.ncbshow.com/',
        'http://www.vibeconference.com/',
        'https://www.nightclub.com/editorial-calendar',
        'https://www.nightclub.com/resources',
        'https://pages.questexweb.com/NCB-Newsletter-Signup.html'

    );

    public static $Top_SocLinks_nc = array
    (   //Desktop
        'https://twitter.com/NightclubBar',
        'https://www.facebook.com/NightclubBar/',
        'https://www.instagram.com/nightclubbar/',
        //Mobile
        'https://twitter.com/NightclubBar',
        'https://www.facebook.com/NightclubBar/',
        'https://www.instagram.com/nightclubbar/'
    );

    public static $Bottom_SocLinks_nc = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_nc = array
    (
        'https://www.nightclub.com/',
        'https://pages.questexweb.com/NCB-Newsletter-Signup.html',
        'https://www.nightclub.com/advertise',
        'https://www.nightclub.com/rss-feeds',
        'http://www.questex.com/privacy-policy',
        'https://www.nightclub.com/about-us',
        'https://www.nightclub.com/contact-us',
    );

    //------------------FierceCEO------------------------//

    public static $Main_navLinks_Fceo = array
    (
        'https://www.fierceceo.com/' => array(

            'https://www.fierceceo.com/growth-innovation' => Array
            (

            ),
            'https://www.fierceceo.com/technology' => Array
            (

            ),
            'https://www.fierceceo.com/customer-experience' => Array
            (

            ),

            'https://www.fierceceo.com/human-capital' => Array
            (

            )));

    public static $Top_navLinks_Fceo = array
    (
        //Desktop
        'https://www.fierceceo.com/resources',
        'https://pages.questexweb.com/FierceCEO-Newsletter-Signup.html',
        'https://www.fierceceo.com/advertise-us',
        //Mobile
        'https://www.fierceceo.com/resources',
        'https://pages.questexweb.com/FierceCEO-Newsletter-Signup.html',
        'https://www.fierceceo.com/advertise-us',
    );

    public static $Top_SocLinks_Fceo = array
    (
        //Desktop
        'https://www.linkedin.com/groups/8615035/',
        'https://www.facebook.com/FierceCEO/',
        'https://twitter.com/fiercechiefexec',
        //Mobile
        'https://www.linkedin.com/groups/8615035/',
        'https://www.facebook.com/FierceCEO/',
        'https://twitter.com/fiercechiefexec'
    );

    public static $Bottom_SocLinks_Fceo = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_Fceo = array
    (
        'https://www.fierceceo.com/',
        'https://pages.questexweb.com/FierceCEO-Newsletter-Signup.html',
        'https://pages.questexweb.com/Manage-Subscriptions_Manage-Subscriptions-All.html',
        'https://www.fierceceo.com/advertise-us',
        'http://www.questex.com/privacy-policy',
        'https://www.fierceceo.com/about-us'
    );

    //------------------CWHK------------------------//

    public static $Main_navLinks_Cwhk = array
    (
        'https://www.cw.com.hk/' => array(

            'https://www.cw.com.hk/it-hk' => Array
            (
                'https://www.cw.com.hk/it-hk/smart-city',
            ),
            'https://www.cw.com.hk/it-infrastructure' => Array
            (
                'https://www.cw.com.hk/it-infrastructure/cloud',
                'https://www.cw.com.hk/it-infrastructure/data-center',
                'https://www.cw.com.hk/it-infrastructure/data-management',
                'https://www.cw.com.hk/it-infrastructure/networking',
                'https://www.cw.com.hk/it-infrastructure/security'
            ),
            'https://www.cw.com.hk/digital-transformation' => Array
            (
                'https://www.cw.com.hk/digital-transformation/it-leadership'
            ),
        )
    );

    public static $Top_navLinks_Cwhk = array
    (
        'https://www.cw.com.hk/topic/viewpoint',
        'https://www.cw.com.hk/resources',
        'https://pages.questexweb.com/CWHK-Newsletter-Signup.html',
        'https://www.cw.com.hk/topic/viewpoint',
        'https://www.cw.com.hk/resources',
        'https://pages.questexweb.com/CWHK-Newsletter-Signup.html'
    );

    public static $Top_SocLinks_Chwk = array
    (
        'https://www.linkedin.com/company-beta/6445303/',
        'https://www.facebook.com/Computerworld.HongKong/',
        'https://www.linkedin.com/company-beta/6445303/',
        'https://www.facebook.com/Computerworld.HongKong/'
    );

    public static $Bottom_SocLinks_Chwk = array
    (
        'http://questex.com/',
        'https://www.facebook.com/Questex-LLC-514978955241030/',
        'https://twitter.com/QuestexLLC',
        'https://www.linkedin.com/company/questex-media-group?trk=top_nav_home'
    );

    public static $footerMenuNav_Chwk = array
    (
        'https://www.cw.com.hk/',
        'https://pages.questexweb.com/CWHK-Newsletter-Signup.html',
        'https://pages.questexweb.com/CWHKManageYourSubscriptions.html',
        'https://www.cw.com.hk/advertise-us',
        'https://www.questex.asia/privacy-policy-statement',
        'http://www.questex.com/terms-of-use',
        'https://www.cw.com.hk/about-us',
        'https://www.telecomasia.net/',
        'https://www.networksasia.net/',
        'https://www.enterpriseinnovation.net/',
        'https://www.cfoinnovation.com/',
        'https://www.enterpriseinnovation.net/cmo',
    );



}

