<?php
namespace Page;

class Pageobj_unique
{
    // include url of current page
    public static $URL = '/';

    //--Base URLs
    public static $prod_url_amspa = 'https://www.americanspa.com/';
    public static $dev_url_amspa = 'https://tektitedev.americanspa.com/';
    public static $prod_url_pattern_amspa = '/\d at http:\/\/www.americanspa.com/';
    public static $prod_url_fbt = 'https://www.fiercebiotech.com/';
    public static $prod_url_pattern_fbt = '/\d at http:\/\/www.fiercebiotech.com/';
    public static $prod_url_fc = 'https://www.fiercecable.com/';
    public static $prod_url_pattern_fc = '/\d at http:\/\/www.fiercecable.com/';
    public static $prod_url_response = 'https://www.responsemagazine.com/';
    public static $prod_url_pattern_response = '/\d at http:\/\/www.responsemagazine.com/';
    public static $prod_url_fhc = 'https://www.fiercehealthcare.com/';
    public static $prod_url_pattern_fhc = '/\d at http:\/\/www.fiercehealthcare.com/';
    public static $prod_url_fp = 'https://www.fiercepharma.com/';
    public static $prod_url_pattern_fp = '/\d at http:\/\/www.fiercepharma.com/';
    public static $prod_url_fr = 'https://www.fierceretail.com/';
    public static $prod_url_pattern_fr = '/\d at http:\/\/www.fierceretail.com/';
    public static $prod_url_ft = 'https://www.fiercetelecom.com/';
    public static $prod_url_pattern_ft = '/\d at http:\/\/www.fiercetelecom.com/';
    public static $prod_url_fw = 'https://www.fiercewireless.com/';
    public static $prod_url_pattern_fw = '/\d at http:\/\/www.fiercewireless.com/';
    public static $prod_url_hm = 'https://www.hotelmanagement.net/';
    public static $prod_url_pattern_hm = '/\d at http:\/\/www.hotelmanagement.net/';
    public static $prod_url_lta = 'https://www.luxurytraveladvisor.com/';
    public static $prod_url_pattern_lta = '/\d at http:\/\/www.luxurytraveladvisor.com/';
    public static $prod_url_tac = 'https://www.travelagentcentral.com/';
    public static $prod_url_pattern_tac = '/\d at http:\/\/www.travelagentcentral.com/';
    public static $prod_url_sensorsMag = 'https://www.sensorsmag.com/';
    public static $prod_url_pattern_sensorsMag = '/\d at http:\/\/www.sensorsmag.com/';
    public static $prod_url_salon = 'https://www.americansalon.com/';
    public static $prod_url_pattern_salon = '/\d at http:\/\/www.americansalon.com/';
    public static $prod_url_nc = 'https://www.nightclub.com/';
    public static $prod_url_pattern_nc = '/\d at http:\/\/www.nightclub.com/';
    public static $prod_url_fceo = 'https://www.fierceceo.com/';
    public static $prod_url_pattern_fceo = '/\d at https:\/\/www.fierceceo.com/';
    public static $prod_url_Cwhk = 'https://www.cw.com.hk/';
    public static $preProd_url_Cwhk = 'http://tektiteprod.cw.com.hk/';
    public static $prod_url_pattern_Cwhk = '/\d at http:\/\/www.cw.com.hk/';

    //--Solr Core site names
    public static $prod_solr_amspa = '[@id="block-tektite-amspa-page-title"]';
    public static $prod_solr_fbt = 'biotechprod';
    public static $prod_solr_fc = 'fiercecable';
    public static $prod_solr_response = 'responsemagazine';
    public static $prod_solr_fhc = 'healthcareprod';
    public static $prod_solr_fp = 'pharmaprod';
    public static $prod_solr_fr = 'fierceretail';
    public static $prod_solr_ft = 'fiercetelecom';
    public static $prod_solr_fw = 'fiercewireless';
    public static $prod_solr_hm = 'tektiteprod';
    public static $prod_solr_lta = 'luxurytraveladvisor';
    public static $prod_solr_tac = 'travelagentcentral';
    public static $prod_solr_sensorsMag = 'sensorsmag';
    public static $prod_solr_salon = 'americansalon';
    public static $prod_solr_nc = 'nightclub';
    public static $prod_solr_Cwhk = 'cwhk';

    //--Solr Search
    public static $H1blk_solr_amspa = '[@id="block-tektite-amspa-page-title"]';
    public static $H1blk_solr_fbt = '[@id="block-tektite-biotech-page-title"]';
    public static $H1blk_solr_fc = '[@id="block-tektite-page-title"]';
    public static $H1blk_solr_response = '[@id="block-tektite-response-tektite-cmo-page-title"]';
    public static $H1blk_solr_fhc = '[@id="block-tektite-healthcare-page-title"]';
    public static $H1blk_solr_fp = '[@id="block-tektite-pharma-page-title"]';
    public static $H1blk_solr_fr = '[@id="block-tektite-retail-page-title"]';
    public static $H1blk_solr_ft = '[@id="block-tektite-page-title"]';
    public static $H1blk_solr_fw = '[@id="block-tektite-page-title"]';
    public static $H1blk_solr_hm = '[@id="block-tektite-hm-page-title"]';
    public static $H1blk_solr_lta = '[@id="block-tektite-lta-page-title"]';
    public static $H1blk_solr_tac = '[@id="block-tektite-tac-page-title"]';
    public static $H1blk_solr_sensorsMag = '[@id="block-tektite-sensorsmag-page-title"]';
    public static $H1blk_solr_salon = '[@id="block-tektite-amsalon-page-title"]';
    public static $H1blk_solr_nc = '[@id="block-tektite-nightclub-page-title"]';
    public static $H1blk_solr_Cwhk = '[@id="block-tektite-cwhk-page-title"]';


    //--Login Credentials----------------------------------
    #production env / engineer user
    public static $EngineerSSO_Amspa_prod = '99585290';
    public static $EngineerSSO_Amspa_Dev = '99585107'; #todo<--changed for ARC Update
    public static $EngineerSSO_Salon_prod = '104048939';
    public static $EngineerSSO_SensorsMag_prod = '113029319';
    public static $EngineerSSO_NC_prod = '112870012';
    public static $EngineerSSO_FBT_prod = '90356898';
    public static $EngineerSSO_FC_prod = '90356859';
    public static $EngineerSSO_FCMO_prod = '91932991';
    public static $EngineerSSO_FHC_prod = '90356878';
    public static $EngineerSSO_FP_prod = '90356901';
    public static $EngineerSSO_FR_prod = '91933499';
    public static $EngineerSSO_FT_prod = '90356871';
    public static $EngineerSSO_FW_prod = '90356857';
    public static $EngineerSSO_HM_prod = '90356903';
    public static $EngineerSSO_LTA_prod = '101871981';
    public static $EngineerSSO_TAC_prod = '101871693';
    public static $EngineerSSO_FCEO_prod = '127890650';
    #Newsletter tool
    public static $EngineerSSO_NWSLTool = '90356869';

    #production env / editor user
    public static $EdSSO_Amspa_prod = '117673846';
    public static $EdSSO_FBT_prod = '117673839';
    public static $EdSSO_FC_prod = '117673849';
    public static $EdSSO_Response_prod = '117673848';
    public static $EdSSO_FHC_prod = '117673841';
    public static $EdSSO_FP_prod = '117673840';
    public static $EdSSO_FR_prod = '117673847';
    public static $EdSSO_FT_prod = '117673851';
    public static $EdSSO_FW_prod = '117673850';
    public static $EdSSO_HM_prod = '117673844';
    public static $EdSSO_LTA_prod = '117673853';
    public static $EdSSO_TAC_prod = '117673852';
    public static $EdSSO_SensorsMag_prod = '119157143';
    public static $EdSSO_Salon_prod = '117673845';
    public static $EdSSO_nc_prod = '117673842';


    #production env / Admin Editor user
    public static $AdminEdSSO_Amspa_prod = '117673862';
    public static $AdminEdSSO_FBT_prod = '117673858';
    public static $AdminEdSSO_FC_prod = '117673865';
    public static $AdminEdSSO_Response_prod = '117673864';
    public static $AdminEdSSO_FHC_prod = '117673860';
    public static $AdminEdSSO_FP_prod = '117673859';
    public static $AdminEdSSO_FR_prod = '117673863';
    public static $AdminEdSSO_FT_prod = '117673867';
    public static $AdminEdSSO_FW_prod = '117673866';
    public static $AdminEdSSO_HM_prod = '117673857';
    public static $AdminEdSSO_LTA_prod = '117673869';
    public static $AdminEdSSO_TAC_prod = '117673868';
    public static $AdminEdSSO_SensorsMag_prod = '119157128';
    public static $AdminEdSSO_Salon_prod = '117673861';
    public static $AdminEdSSO_nc_prod = '114309935';
    public static $AdminEdSSO_Fceo_prod = '132082181';
    public static $AdminEdSSO_Cwhk_prod = '148900461';
    public static $AdminEdSSO_Cwhk_PREprod = '148900461';

    //AmSpa - ARC -- Article Thumbnail field
    public static $arc_thumbNailUpload_form = '//*[@id="edit-field-thumbnail-image-0-upload"]';
    public static $arc_thumbNailAltText_fld = '[data-drupal-selector*=\'edit-field-thumbnail-image-0-alt\']';
    public static $amSpa_thumbImgName = 'ARC_Thumb_Upload_Test_IMG.jpg';

    //Unique Locators
    #ResponseMag Unique locators menu
    public static $NavLocator_main_response = '//*[@id="block-tektite-response-tektite-cmo-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_response = '//*[@id="block-tektite-response-tektite-cmo-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_response = '//*[@id="block-tektite-response-tektite-cmo-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_response = '//*[@id="block-tektite-response-tektite-cmo-tektitefooterblock"]/ul/li/a';

    #FR Unique locators menu
    public static $NavLocator_main_fr = '//*[@id="block-tektite-retail-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_fr = '//*[@id="block-tektite-retail-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_fr = '//*[@id="block-tektite-retail-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_fr = '//*[@id="block-tektite-retail-tektitefooterblock"]/ul/li/a';

    #LTA Unique locators menu
    public static $NavLocator_main_lta = '//*[@id="block-tektite-lta-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_lta = '//*[@id="block-tektite-lta-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_lta = '//*[@id="block-tektite-lta-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_lta = '//*[@id="block-tektite-lta-tektitefooterblock"]/ul/li/a';

    #TAC Unique locators menu
    public static $NavLocator_main_tac = '//*[@id="block-tektite-tac-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_tac = '//*[@id="block-tektite-tac-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_tac = '//*[@id="block-tektite-tac-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_tac = '//*[@id="block-tektite-tac-tektitefooterblock"]/ul/li/a';

    #AmericanSpa Unique locators menu
    public static $NavLocator_main_Amspa = '//*[@id="block-tektite-amspa-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_Amspa = '//*[@id="block-tektite-amspa-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_Amspa = '//*[@id="block-tektite-amspa-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_Amspa = '//*[@id="block-tektite-amspa-tektitefooterblock"]/ul/li/a';

    #SensorsMag Unique locators menu
    public static $NavLocator_main_SensorsMag = '//*[@id="block-tektite-sensorsmag-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_SensorsMag = '//*[@id="block-tektite-sensorsmag-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_SensorsMag = '//*[@id="block-tektite-sensorsmag-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_SensorsMag = '//*[@id="block-tektite-sensorsmag-tektitefooterblock"]/ul/li/a';

    #American Salon Unique locators menu
    public static $NavLocator_main_salon = '//*[@id="block-tektite-amsalon-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_salon = '//*[@id="block-tektite-amsalon-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_salon = '//*[@id="block-tektite-amsalon-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_salon = '//*[@id="block-tektite-amsalon-tektitefooterblock"]/ul/li/a';

    #NightClub Unique locators menu
    public static $NavLocator_main_nc = '//*[@id="block-tektite-nightclub-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_nc = '//*[@id="block-tektite-nightclub-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_nc = '//*[@id="block-tektite-nightclub-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_nc = '//*[@id="block-tektite-nightclub-tektitefooterblock"]/ul/li/a';

    #FierceCEO
    public static $NavLocator_main_fceo = '//*[@id="block-tektite-fierce-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_fceo = '//*[@id="block-tektite-fierce-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_fceo = '//*[@id="block-tektite-fierce-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_fceo = '//*[@id="block-tektite-fierce-tektitefooterblock"]/ul/li/a';

    #CWHK Unique locators menu
    public static $NavLocator_main_Cwhk = '//*[@id="block-tektite-cwhk-mainnavigation"]/ul/li/a';
    public static $NavLocator_top_Cwhk = '//*[@id="block-tektite-cwhk-topmenu"]/ul/li/a';
    public static $NavLocator_topSoc_Cwhk = '//*[@id="block-tektite-cwhk-tektitesocialshareblock"]/a';
    public static $NavLocator_footerNav_Cwhk = '//*[@id="block-tektite-cwhk-tektitefooterblock"]/ul/li/a';

    //Search Block + SearchPageBodyDiv
    #American Spa
    public static $SearhBlockDiv_amSpa = '//*[@id="block-tektite-amspa-searchapipagesearchblockform"]';
    public static $pageBodyDiv_amSpa = '//*[@id="block-tektite-amspa-content"]/div[3]';

    #FierceBioTech
    public static $pageBodyDiv_fbt = '//*[@id="block-tektite-biotech-content"]/div[3]';

    #Response
    public static $SearhBlockDiv_response = '//*[@id="block-tektite-response-tektite-cmo-searchapipagesearchblockform"]';
    public static $pageBodyDiv_response = '//*[@id="block-tektite-response-tektite-cmo-content"]/div[3]';

    #FierceHealthCare
    public static $pageBodyDiv_fhc = '//*[@id="block-tektite-healthcare-content"]/div[3]';

    #FierceHealthCare
    public static $pageBodyDiv_fp = '//*[@id="block-tektite-pharma-content"]/div[3]';

    #FierceRetail
    public static $SearhBlockDiv_fr = '//*[@id="block-tektite-retail-searchapipagesearchblockform"]';
    public static $pageBodyDiv_fr = '//*[@id="block-tektite-retail-content"]/div[3]';

    #Hotel Management
    public static $pageBodyDiv_hm = '//*[@id="block-tektite-hm-content"]/div[3]';

    #Lux Travel Advisor
    public static $SearhBlockDiv_lta = '//*[@id="block-tektite-lta-searchapipagesearchblockform"]';
    public static $pageBodyDiv_lta = '//*[@id="block-tektite-lta-content"]/div[3]';

    #Travel Agent Central
    public static $SearhBlockDiv_tac = '//*[@id="block-tektite-tac-searchapipagesearchblockform"]';
    public static $pageBodyDiv_tac = '//*[@id="block-tektite-tac-content"]/div[3]';

    #Sensors Mag
    public static $SearhBlockDiv_sensorsMag = '//*[@id="block-tektite-sensorsmag-searchapipagesearchblockform"]';
    public static $pageBodyDiv_sensorsMag = '//*[@id="block-tektite-sensorsmag-content"]/div[3]';

    #American Salon
    public static $SearhBlockDiv_salon = '//*[@id="block-tektite-amsalon-searchapipagesearchblockform"]';
    public static $pageBodyDiv_salon = '//*[@id="block-tektite-amsalon-content"]/div[3]';

    #NightClub
    public static $SearhBlockDiv_nc = '//*[@id="block-tektite-nightclub-searchapipagesearchblockform"]';
    public static $pageBodyDiv_nc = '//*[@id="block-tektite-nightclub-content"]/div[3]';

    #FCEO
    public static $SearhBlockDiv_fceo = '//*[@id="block-tektite-fierce-searchapipagesearchblockform"]';
    public static $pageBodyDiv_fceo = '//*[@id="block-tektite-fierce-content"]/div[3]';

    #CWHK
    public static $SearhBlockDiv_Cwhk = '//*[@id="block-tektite-cwhk-searchapipagesearchblockform"]';
    public static $pageBodyDiv_Cwhk = '//*[@id="block-tektite-cwhk-content"]/div[3]';


    //Newsletter Tool site numbers
    public static $nwsl_siteNumber_AmSpa = '60378';
    public static $nwsl_siteNumber_FBT = '23022';
    public static $nwsl_siteNumber_FC = '44044';
    public static $nwsl_siteNumber_response = '132757';
    public static $nwsl_siteNumber_Fhc = '34837';
    public static $nwsl_siteNumber_Fp = '22600';
    public static $nwsl_siteNumber_Fr = '46796';
    public static $nwsl_siteNumber_Ft = '78433';
    public static $nwsl_siteNumber_Fw = '44047';
    public static $nwsl_siteNumber_hm = '43565';
    public static $nwsl_siteNumber_hmROI = '43607';
    public static $nwsl_siteNumber_lta = '62191';
    public static $nwsl_siteNumber_tac = '62198';
    public static $nwsl_siteNumber_sensorsMag = '86561';
    public static $nwsl_siteNumber_salon = '75654';
    public static $nwsl_siteNumber_nc = '80157';
    public static $nwsl_siteNumber_Fceo = '108255';
    public static $nwsl_siteNumber_Cwhk = '142904'; #Daily News template

    //Popular Content Block
    #American Spa
    public static $PopContentBlk_numberofElements_AmSpa = '//*[@id="block-tektite-amspa-popularcontentfromparsely"]/ul/li';
    public static $popContent_Title1_AmSpa = '//*[@id="block-tektite-amspa-popularcontentfromparsely"]/ul/li[1]/h3/a';
    public static $popContent_Title2_AmSpa = '//*[@id="block-tektite-amspa-popularcontentfromparsely"]/ul/li[2]/h3/a';
    public static $popContent_Title3_AmSpa = '//*[@id="block-tektite-amspa-popularcontentfromparsely"]/ul/li[3]/h3/a';
    public static $popContent_Title4_AmSpa = '//*[@id="block-tektite-amspa-popularcontentfromparsely"]/ul/li[4]/h3/a';
    public static $popContent_Title5_AmSpa = '//*[@id="block-tektite-amspa-popularcontentfromparsely"]/ul/li[5]/h3/a';

    //--Newsletter Sidebar elements
    #american spa
    static $nwsl_sidebarContainer_AmSpa = '//*[@id="mktoForm_14033"]';
    #fierce biotech
    static $nwsl_sidebarContainer_FBT = '//*[@id="mktoForm_12996"]';
    #fierce cable
    static $nwsl_sidebarContainer_FC = '//*[@id="mktoForm_12995"]';
    #fierce healthcare
    static $nwsl_sidebarContainer_FHC = '//*[@id="mktoForm_12998"]';
    #fierce pharma
    static $nwsl_sidebarContainer_FP = '//*[@id="mktoForm_12997"]';
    #fierce retail
    static $nwsl_sidebarContainer_FR = '//*[@id="mktoForm_12936"]';
    #fierce telecom
    static $nwsl_sidebarContainer_FT = '//*[@id="mktoForm_12908"]';
    #fierce wireless
    static $nwsl_sidebarContainer_FW = '//*[@id="mktoForm_12994"]';
    #LTA
    static $nwsl_sidebarContainer_LTA = '//*[@id="mktoForm_14632"]';
    #NightClub
    static $nwsl_sidebarContainer_NC = '//*[@id="mktoForm_16165"]';
    #AmericanSalon
    static $nwsl_sidebarContainer_Salon = '//*[@id="mktoForm_15904"]';
    #SensorsMag
    static $nwsl_sidebarContainer_SensorsMag = '//*[@id="mktoForm_16481"]';
    #TravelAgentCentral
    static $nwsl_sidebarContainer_TAC = '//*[@id="mktoForm_14622"]';
    #Hotel Management
    static $nwsl_sidebarContainer_HM = '//*[@id="mktoForm_9363"]';
    #FierceCEO
    static $nwsl_sidebarContainer_Fceo = '//*[@id="mktoForm_18072"]';
    #ResponseMag
    static $nwsl_sidebarContainer_response = '//*[@id="mktoForm_20209"]';
    #CWHK
    static $nwsl_sidebarContainer_Cwhk = '//*[@id="mktoForm_21048"]';

    //---Product pages
    #event pages
    public static $eventPg_products_fbt = '/products/147%2058401'; #event
    public static $eventPg_products_fc = '/products/3281%206001'; #event
    public static $eventPg_products_fhc = '/products/106%2033371'; #event
    public static $eventPg_products_fp = '/products/246%2036331'; #event
    public static $eventPg_products_fr = '/products/28676%2034806'; #event
    public static $eventPg_products_ft = '/products/9391%2010401'; #event
    public static $eventPg_products_fw = '/products/9766%2013031'; #event
    public static $eventPg_products_hm = '/products/12061'; #event
    public static $eventPg_products_response = '/products/14391'; #event
    public static $eventPg_products_sensors = '/products/26'; #event
    public static $eventPg_products_Cwhk = '/products/61'; #event

    #deal pages
    public static $dealPg_products_lta = '/products/2981'; #deals
    public static $dealPg_products_tac = '/products/2456'; #deals

    #award pages
    public static $awardPg_products_Cwhk = '/products/176'; #awards

    //--Custom 404 page elemets
    public static $errorblck = '//div[@class="page-not-found"]';
//    public static $errorpage = '//div[@class="error-page"]';
//    public static $errorblock = '//div[@class="error-block"]';

    //Newsletter Tool TOC testing
    #fbt
    public static $newslt_edit_art1_cord_fbt = '(221, 1500)';
    public static $newslt_edit_art2_cord_fbt = '(221, 1667)';
    public static $newslt_edit_art3_cord_fbt = '(221, 1767)';
    public static $newslt_edit_art4_cord_fbt = '(221, 1867)';
    public static $newslt_edit_art5_cord_fbt = '(221, 1967)';
    public static $newslt_edit_art6_cord_fbt = '(221, 2500)';
    #hm-roi
    public static $newslt_edit_art1_cord_hmROI = '(221, 1200)';
    public static $newslt_edit_art2_cord_hmROI = '(221, 1400)';
    public static $newslt_edit_art3_cord_hmROI = '(221, 1700)';
    public static $newslt_edit_art4_cord_hmROI = '(221, 1800)';
    public static $newslt_edit_art5_cord_hmROI = '(221, 1940)';
    public static $newslt_edit_art6_cord_hmROI = '(221, 2120)';


}