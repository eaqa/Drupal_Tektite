<?php
namespace Page;
use Codeception\Util\Locator;

class Pageobj_common
{
    // include url of current page
    public static $URL = '';

    //////Duplicate
    #Newsletter tool - Production
    public static $EngineerSSO_NWSLTool = '90356869';
    #Newsletter tool - Stage
    public static $EngineerSSO_NWSLTool_stage = '93574580';

    ////////

    //--aqLiftDrupalSettings----------------------------------

        public static $aqLift_credential_lnk = '//*[@id="edit-credential"]/summary';
        public static $accountName_fld = '//*[@id="edit-credential-account-name"]';
        public static $customerSite_fld = '//*[@id="edit-credential-customer-site"]';
        public static $apiUrl_fld = '//*[@id="edit-credential-api-url"]';
        public static $apiAccessKey_fld = '//*[@id="edit-credential-access-key"]';
        public static $jsPath_fld = '//*[@id="edit-credential-js-path"]';
        public static $identPar_fld = '//*[@id="edit-identity-identity-parameter"]';
        public static $identTypePar_fld = '//*[@id="edit-identity-identity-type-parameter"]';
        public static $defaultIdentType_fld = '//*[@id="edit-identity-default-identity-type"]';
        public static $fldMappings_lnk = '//*[@id="acquia-lift-settings-form"]/div[1]/div/ul/li[2]/a';
        public static $contentSec_fld = '//*[@id="edit-field-mappings-content-section"]';
        public static $contentKeywrds_fld = '//*[@id="edit-field-mappings-content-keywords"]';
        public static $persona_fld = '//*[@id="edit-field-mappings-persona"]';
        public static $aqLiftAdminPg = 'admin/config/content/acquia_lift';

    //--ARC_validation----------------------------------
        public static $articleCreatelnk = '/node/add/article';
        public static $browseBtn = '//*[@id="arc-browse-button"]';
        public static $arcTitleSearchFld = '//*[@id="edit-title"]';
        public static $arcSearchApplybtn = '//*[@id="edit-submit-asset-list"]';
        public static $arcSelectRadiobtn = '//*[@value ="asset_3951"]';
        public static $arcSelectRadiobtn2 = '//*[@value ="asset_17395"]';
        public static $arcInsertAssetbtn = '//*[@id="arc-browse-insert-button"]';
        public static $arc_imgSearchTerm = 'Huawei America';
        public static $arc_imgSearchTerm_2 = 'Hugh Riley and Scott Wiseman';
        public static $arc_primaryImgFld = '//*[@id="edit-field-arc-article-thumbnail-0-value"]';
        public static $arc_HeadlineFld = '#edit-title-0-value';
        public static $arc_altHeadlineFld = '//*[@id="edit-field-alternative-headline-0-value"]';
        public static $arc_introFld = '//*[@id="edit-field-introduction-0-value"]';
        public static $arc_introTeaserFld = '//*[@id="edit-field-introduction-teaser-0-value"]';
        public static $arc_authorFld = '//*[@id="edit-field-author-0-target-id"]';
        public static $arc_PrimaryTaxonomy_1stOption = '//*[@id="edit-field-primary-taxonomy"]/option[2]';
        public static $previewbutton = '//*[@id="edit-preview"]';
        public static $primaryImgFileName = 'huawei america_0.jpg';
        //public static $uploadBtn = '//*[@id="arc-upload-button"]';
        public static $uploadBtn = '//*[@id="edit-field-arc-article-thumbnail-0-upload"]'; #todo<--changed for ARC Update
        //upload iframe
        //public static $arcUpload_titlefld = '//*[@id="edit-title-0-value"]';
        public static $arcUpload_titlefld = '/descendant::input[@class="js-text-full text-full form-text required"][3]'; #todo<--changed for ARC Update
        //public static $arcUpload_altTxtfld  = '//*[@id="edit-field-alt-text-0-value"]';
        public static $arcUpload_altTxtfld  = '/descendant::input[@data-drupal-selector="edit-field-alt-text-0-value"]'; #todo<--changed for ARC Update
        //public static $arcUpload_tagfld = '//*[@id="edit-field-tags-0-value"]';
        public static $arcUpload_tagfld = '/descendant::input[@data-drupal-selector="edit-field-tags-0-value"][1]'; #todo<--changed for ARC Update
        //public static $arcUpload_RightsReqfld = '//*[@id="edit-field-rights"]';
        public static $arcUpload_RightsReqfld = '//select[@name=\'field_rights\']'; #todo<--changed for ARC Update
        //public static $arcUpload_imgUploadfld = '//*[@id="edit-field-image-0-upload"]';
        public static $arcUpload_imgUploadfld = '//input[@name=\'files[field_image_0]\']'; #todo<--changed for ARC Update
        //public static $arcUpload_imgSavebtn = '[class*=\'publish dropbutton-action\']';
        public static $arcUpload_imgSavebtn = '/descendant::span[@class="ui-button-text"][5]'; #todo<--changed for ARC Update
        //WYSIWYG interface
        public static $editor_arc_browserbtn = '//*[@id="cke_30"]';
        public static $editor_arc_firstRadioBtn = '/html/body/div[1]/div/section/div/div/div/div[2]/div/table/tbody/tr[1]/td[1]/input';
        public static $editor_arc_insertBtn = '//*[@id="cke_69_label"]';
        //Browse image
        public static $primaryImgFileName_2 = 'Hugh Riley and Scott Wiseman';
        //editor upload bttn
        public static $editor_arc_uploadbtn = '//*[@id="cke_29"]';
        public static $editor_arc_upload_img = 'ARC_Upload_Test_IMG_ckeditor.jpg';
        //Draft popup
        public static $draftPop_container = '//*[@id="ui-id-5"]';
        public static $draftPop_discardBttn = '//button[@class=\'autosave-form-reject-button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button\']';


    //Products Pages----------------------------------
        public static $filterButtonDiv_products = '//*[@id="views-exposed-form-leadgen-product-listing-eventslisting"]';
        public static $contestPg_products = 'products/861'; #contests
        public static $webinarPg_products = 'products/webinar';

        public static $categoryMenu_opts = '//select[@name=\'field_taxonomy_target_id\']/option';
        public static $sortOrderMenu_opts = './/*[@id=\'edit-sort-order\']/option';

        //Resource Pages
        public static $filterButtonDiv_resources = '//*[@id="views-exposed-form-leadgen-product-listing-leadgenproductlisting"]';
        public static $selectTypeFilterButton_resources = '//select[@name=\'field_product_type_target_id\']';
        public static $selectTypeFilterButton_resources_opts = '//*[@id="edit-field-product-type-target-id"]/option';
        public static $resourcesPg = '/resources';

        //Products + Resources shared
        public static $categoryFilterButton = '//select[@name=\'field_taxonomy_target_id\']';
        public static $categoryFilterButton_opt1 = '//select[@name=\'field_taxonomy_target_id\']/option[2]';
        public static $sortOrderFilterButton = '//select[@name=\'sort_order\']';
        public static $sortOrderFilterButton_opt1 = '//*[@id="edit-sort-order"]/option[2]';
        public static $contentWell_opts = '//div[@class=\'views-row\']';

    //SearchBlock----------------------------------
        static $searchIcon = '//div[@class="search-popup"]';
        static $searchShelf = '#search-api-page-block-form';
        static $searchBlockDiv = '//*[@id="block-searchapipagesearchblockform"]';
        static $SearchField = '#edit-keys';
        static $pageBodyDiv = '//*[@id="block-tektite-content"]/div[3]';

    //--OneLogin----------------------------------
        public static $OneLoginRoot_url = 'https://questex.onelogin.com/client/apps/select/';

    //--CSV files----------------------------------
        public static $amSpaAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_AmSpa.csv'; #unique
        public static $FBTAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_FBT.csv'; #unique
        public static $FCAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_FC.csv'; #unique
        public static $FcmoAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_Fcmo.csv'; #unique
        public static $FHCAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_FHC.csv'; #unique
        public static $FPAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_FP.csv'; #unique
        public static $FRAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_FR.csv'; #unique
        public static $FTAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_FT.csv'; #unique
        public static $FWAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_FW.csv'; #unique
        public static $HMAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_hm.csv'; #unique
        public static $LTAAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_LTA.csv'; #unique
        public static $TACAqLiftCreds = '/Users/erikanderson/Documents/CodeceptionCsvFiles/aqLift_credentialSettings_TAC.csv'; #unique
        public static $csvFile_admin = '/Users/erikanderson/Documents/CodeceptionCsvFiles/sso_admin.csv';
        public static $csvFile_editor = '/Users/erikanderson/Documents/CodeceptionCsvFiles/sso_editor.csv';
        public static $csvFile_AdminEd = '/Users/erikanderson/Documents/CodeceptionCsvFiles/sso_admin_editor.csv';
        public static $csvFile_EngineerSSO = '/Users/erikanderson/Documents/CodeceptionCsvFiles/sso_admin.csv';
        public static $parselyCSV_file = '/Users/erikanderson/Documents/CodeceptionCsvFiles/parsely_api.csv';
        public static $GA_CSV_file = '/Users/erikanderson/Documents/CodeceptionCsvFiles/GA_api.csv';

    //--RSS/XML data feeds----------------------------------
        public static $first_title = '/rss/channel/item[1]/title/text()';
        public static $second_title = '/rss/channel/item[2]/title/text()';
        public static $third_title = '/rss/channel/item[3]/title/text()';
        public static $forth_title = '/rss/channel/item[4]/title/text()';
        public static $fifth_title = '/rss/channel/item[5]/title/text()';
        public static $sixth_title = '/rss/channel/item[6]/title/text()';
        public static $seventh_title = '/rss/channel/item[7]/title/text()';

    //--Newsletter Tool UI Navigation elements
        public static $open2_bttn  = '//*[@id="boxes-box-draft_add_content_controls"]/div/div/button[2]';
        public static $Close2_bttn  = '//*[@id="boxes-box-draft_add_content_controls"]/div[1]/div/button[4]';
        public static $title_field = '//*[@id="edit-title"]';
        public static $save_bttn = '//*[@id="edit-submit"]';
        public static $newslt_panelizer_subMenu = '//*[@id="block-qxnews-blocks-panelizer-sub-menu"]';
        #Edition articles in the slideout menu
        public static $newslt_art1 = '//*[@id="block-views-solr-search-block-1"]/div[2]/div/div/div/ul/li[1]';
        public static $newslt_art2 = '//*[@id="block-views-solr-search-block-1"]/div[2]/div/div/div/ul/li[2]';
        public static $newslt_art3 = '//*[@id="block-views-solr-search-block-1"]/div[2]/div/div/div/ul/li[3]';
        public static $newslt_art4 = '//*[@id="block-views-solr-search-block-1"]/div[2]/div/div/div/ul/li[4]';
        public static $newslt_art5 = '//*[@id="block-views-solr-search-block-1"]/div[2]/div/div/div/ul/li[5]';
        public static $newslt_art6 = '//*[@id="block-views-solr-search-block-1"]/div[2]/div/div/div/ul/li[6]';
        #Edition sections
        public static $nweslt_sec_featured = '/descendant::div[@class="article-droppable position-0 ui-droppable"][1]';
        public static $newslt_sec_featured_bar = '/descendant::div[@class="grab-title grabber panel-draggable"][4]';
        public static $nweslt_sec_top = '/descendant::div[@class="article-droppable position-0 ui-droppable"][2]';
        public static $nweslt_sec_top_bar = '/descendant::div[@class="grab-title grabber panel-draggable"][6]';
        #Edition Error Alert
        public static $newslt_alert_err = '//input[@class="form-text required error"]';
        public static $newslt_alert_status = '//*[@id="main"]/div[1]/em';
        #Edition TOC menue
        public static $newslt_previewbtn = '//*[@id="block-qxnews-blocks-panelizer-edit-menu"]/div[2]/a[3]';
        public static $newslt_TOC_bloc = '//ul[@class=\'toc-list\']';
        #edition article editing
        public static $newslt_edit_saveBttn = '//input[@id=\'edit-submit\']';


        #Search Results Shelf view
        public static $keywords_txtbox = '//*[@id="edit-keywords"]';
        public static $apply_bttn = '//*[@id="edit-submit-solr-search"]';
        public static $clear_txt_bttn = '//*[@id="edit-keywords-wrapper"]/div/div/button';
        public static $clear_all_bttn = '#views-exposed-form-solr-search-block-1 > div > div > div > button';

    //--Popular Content Block elements
        public static $popContentBlockDiv = '[class*=\'block block-tektite-generic-blocks block-popular-content-from-parsely\']';
        public static $popContent_Title1 = '//*[@id="block-popularcontentfromparsely"]/ul/li[1]/h3/a';
        public static $popContent_Title2 = '//*[@id="block-popularcontentfromparsely"]/ul/li[2]/h3/a';
        public static $popContent_Title3 = '//*[@id="block-popularcontentfromparsely"]/ul/li[3]/h3/a';
        public static $popContent_Title4 = '//*[@id="block-popularcontentfromparsely"]/ul/li[4]/h3/a';
        public static $popContent_Title5 = '//*[@id="block-popularcontentfromparsely"]/ul/li[5]/h3/a';

    //--Newsletter Sidebar elements
        public static $nwsl_emailField = '//*[@id="Email"]';
        public static $mainForm_headerLogo = '//*[@id="header_logo"]';

    //--XML Sitemap elements
        public static $siteMapIndex = 'sitemap.xml';
        public static $siteMap_subpg1 = 'sitemaps/1/sitemap.xml';
        public static $siteMap_subpg2 = 'sitemaps/2/sitemap.xml';
        public static $siteMap_subpg3 = 'sitemaps/3/sitemap.xml';
        public static $siteMap_subpg4 = 'sitemaps/4/sitemap.xml';
        public static $treeNode_index = 'sitemap';
        public static $treeNode_subpg1 = 'url';
}
