<?php
use \page\Pageobj_unique;
use \page\Pageobj_common;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    public function aqLiftDrupal(\Step\Acceptance\AcquiaLift $TestSteps, \Step\Acceptance\BaseFunctions $Base, $URL, $EngineerSSO_tilenumber, $configCSV_file)
    {
        // $URL - specific to site and environment
        // $EngineerSSO - specific to site, environment and user
        // $loginCSV_file - specific to user
        // $configCSV_file - specific to site

        //$I = new BaseFunction($scenario);
        //log user into OneLogIn
        $Base->loginSSO(Pageobj_common::$csvFile_admin, $EngineerSSO_tilenumber); #logging into OneLogin main dashboard as Engineer User

        $Base->goToPage($URL, Pageobj_common::$aqLiftAdminPg); #aq lift drupal config page

        //$I = new AqliftConfig($scenario);
        $TestSteps->verifyAqLiftConfig($configCSV_file);

    }

    public function ARCValidation_browseForImg(\Step\Acceptance\ARC $TestSteps, \Step\Acceptance\BaseFunctions $Base, $URL, $arc_imgSearchTerm,
                                    $author, $primaryTaxonomy, $secondaryTaxonomy, $keyword, $topic)
    {
        $I = $this;
        //log user into OneLogIn - this function allows any user type to log in
        $Base->goToPage($URL, Pageobj_common::$articleCreatelnk);

        //if a previous draft dialog popup shows, dismiss by discarding draft and exit function
        //if a previous draft dialog popup dose not show (that is no draft exists), skip function and continue
        $TestSteps->DiscardDraft();

        //Getting Arc image by browsing for image
        $TestSteps->ARC_insertBrowsedImg($arc_imgSearchTerm, $Base);
        $I->wait(7);
        $I->pauseExecution();
        //seeing if Primary Image field is populated with image ID - will get a print out of
        $I->dontSeeFieldIsEmpty($PrimaryImgID_1 = $I->grabValueFrom(Pageobj_common::$arc_primaryImgFld));

        //Filling out required fields for create article form
        $TestSteps->fillOut_ReqCreateArticleFields($author, $primaryTaxonomy, $secondaryTaxonomy, $keyword, $topic);
        $I->comment('PRIMARY IMAGE ID =');
        codecept_debug($PrimaryImgID_1);
        return $PrimaryImgID_1;

    }

    public function ARCVaidation_Uploadimg_InsertImg(\Step\Acceptance\ARC $TestSteps, \Step\Acceptance\BaseFunctions $Base, $imgFileName, $author, $primaryTaxonomy, $secondaryTaxonomy, $keyword, $topic)
    {
        $I = $this;
        //Verifying if browsed ARC image is on the Preview page
        $TestSteps->verifyImgOnPreviewpg($imgFileName, $Base); #seeing if file name is in source code of the Preview page
        $I->moveBack();

        //Uploading image to ARC
        $TestSteps->ARC_insertUploadedImg($Base);

        //seeing if Primary Image field is populated with image ID - will get a print out of
        $I->dontSeeFieldIsEmpty($PrimaryImgID_2 = $I->grabValueFrom(Pageobj_common::$arc_primaryImgFld));
        $I->comment('PRIMARY IMAGE ID ='); codecept_debug($PrimaryImgID_2);

        //Inserting ARC image into CKeditor
        $TestSteps->ARC_insertRichTextEditorImg();

        //Filling out required create article forms
        $TestSteps->fillOut_ReqCreateArticleFields($author, $primaryTaxonomy, $secondaryTaxonomy, $keyword, $topic);
        return $PrimaryImgID_2;
    }

    public function ARCValidation_verfyImgsOnPreviewPg(\Step\Acceptance\ARC $TestSteps, $imgFileName, \Step\Acceptance\BaseFunctions $Base)
    {
        //$I = $this;

        //Verifying inserted CKeditor image is on the Preview Page
        $TestSteps->verifyImgOnPreviewpg($imgFileName, $Base); #seeing if file name is in source code of the Preview page

    }

    public function ArcValidation_Uploadimg_editor(\Step\Acceptance\ARC $TestSteps, $author, $primaryTaxonomy, $secondaryTaxonomy, $keyword, $topic, $imgFileName, $Base)
    {
        $I = $this;
        $I->moveBack();
        $TestSteps->ARC_upload_RichTextEditorImg();
        $I->wait(2);
        $TestSteps->fillOut_ReqCreateArticleFields($author, $primaryTaxonomy, $secondaryTaxonomy, $keyword, $topic);
        $I->wait(2);
        $TestSteps->ARC_insertBrowsedImg(Pageobj_common::$arc_imgSearchTerm, $Base);
        $I->wait(2);
        $TestSteps->ARCValidation_verfyImgsOnPreviewPg($TestSteps, $imgFileName, $Base);
        $I->canSeeInPageSource('title="Huawei America"');
        $I->pauseExecution();
    }

    public function productRespg_verifyContentPages(\Step\Acceptance\ResourceProductsPgs $TestSteps, \Step\Acceptance\BaseFunctions $Base,
                                                    $URL, $relativeURL, $Catoption, $optContentWEll)
    {
        $I = $this;
        //Verifying Filter menu div is present
        $Base->goToPage($URL, $relativeURL);
        $TestSteps->productsDiv();

        //verifying category menu is populated
        $TestSteps->filterMenuNotEmpty(Pageobj_common::$categoryMenu_opts);
        //verifying Sort Order menu is populated
        $TestSteps->filterMenuNotEmpty(Pageobj_common::$sortOrderMenu_opts);

        //verifying user can select an option from the Category menus
        $TestSteps->selectBttnOpts(Pageobj_common::$categoryFilterButton, $Catoption);
        //verifying user can select an option from the Category menus
        $TestSteps->selectBttnOpts(Pageobj_common::$sortOrderFilterButton, 'Newest');

        //verifying content well is not empty
        $TestSteps->contentWellNotEmpty(Pageobj_common::$contentWell_opts, Pageobj_common::$categoryFilterButton, $optContentWEll);


    }

    public function productRespg_verifyResourcePages(\Step\Acceptance\ResourceProductsPgs $TestSteps, \Step\Acceptance\BaseFunctions $Base,
                                                     $URL, $relativeURL, $Typeoption, $Catoption, $optContentWEll)
    {
        $I = $this;
        //Verifying Filter menu div is present
        $Base->goToPage($URL, $relativeURL);
        $TestSteps->resourcesDiv();

        //verifying Type menu is populated
        $TestSteps->filterMenuNotEmpty(Pageobj_common::$selectTypeFilterButton_resources_opts);
        //verifying category menu is populated
        $TestSteps->filterMenuNotEmpty(Pageobj_common::$categoryMenu_opts);
        //verifying Sort Order menu is populated
        $TestSteps->filterMenuNotEmpty(Pageobj_common::$sortOrderMenu_opts);

        //verifying user can select an option from the Type menus
        $TestSteps->selectBttnOpts(Pageobj_common::$selectTypeFilterButton_resources, $Typeoption);
        //verifying user can select an option from the Category menus
        $TestSteps->selectBttnOpts(Pageobj_common::$categoryFilterButton, $Catoption);
        //verifying user can select an option from the Category menus
        $TestSteps->selectBttnOpts(Pageobj_common::$sortOrderFilterButton, 'Newest');

        //verifying content well is not empty
        $TestSteps->contentWellNotEmpty(Pageobj_common::$contentWell_opts, Pageobj_common::$selectTypeFilterButton_resources, $optContentWEll);
    }

    public function MenuNavLinks_Main_SubLinks_verify(\Step\Acceptance\MenuNavLinksVeri $TestSteps, \Step\Acceptance\BaseFunctions $Base, $site, $navlocator, $preArray, $uniqueSubLink = '-')
    {
        $I = $this;
        $TestSteps->MainNav_Menu_verify($site, $navlocator, $preArray);
        # todo: maninNav link verificationwill catch multiple erring top nav links
        $TestSteps->SubNav_Menu_verify($site, $navlocator, $preArray, $uniqueSubLink);
        # todo: subnav link verification -will catch multiple erring subnav links in the same node, but not in different nodes
    }

    public function MenuNavLinks_Nav_verify(\Step\Acceptance\MenuNavLinksVeri $TestSteps, $site, $navlocator, $preArray)
    {
        $I = $this;
        $TestSteps->Nav_verify($site, $navlocator, $preArray);

    }

    public function Search_elements_present(\Step\Acceptance\SearchBlock $TestSteps, \Step\Acceptance\BaseFunctions $Base,
    $site, $SearchBlockDiv)
    {
        $I = $this;
        $I->amOnUrl($site);
        $TestSteps->SeeSearchElements($SearchBlockDiv);
        //also verifies the global element 'searchshelf' Pageobj_common::$searchShelf is present
    }

    public function ConductSearchAndVerify(\Step\Acceptance\SearchBlock $TestSteps,
    $pageBodyDiv, $SearchTerm, $SearchField)
    {
        $I = $this;
        $TestSteps->ConductSearch_verify($pageBodyDiv, $SearchTerm, $SearchField);
    }

    #part of the Newsletter tool is Acceptance and part is API
    public function NewsletterTool_Login_navigate(\Step\Acceptance\NewsletterTool $TestSteps,
    \Step\Acceptance\BaseFunctions $baseFunctions, $nwsl_Site_number)
    {
        $I = $this;
        $TestSteps->Login_navigate_to_nwsl($baseFunctions, $nwsl_Site_number);
    }

    public function Verify_nwslt_contains_grabbedArticle_data(\Step\Acceptance\NewsletterTool $TestSteps,
    $grabbed_article_title, $grabbed_article_title_NoSpace, $_)
    {
        $I = $this;

        //Article title 1
        $TestSteps->Nwslt_has_articleData(
        $grabbed_article_title[0],
        $grabbed_article_title_NoSpace[0]
        );

        //Reload Page
        $I->reloadPage();
        $I->click(Pageobj_common::$open2_bttn);

        //Article title 2
        $TestSteps->Nwslt_has_articleData(
            $grabbed_article_title[1],
            $grabbed_article_title_NoSpace[1]
        );

        //Reload Page
        $I->reloadPage();
        $I->click(Pageobj_common::$open2_bttn);

        //Article title 3
        $TestSteps->Nwslt_has_articleData(
            $grabbed_article_title[2],
            $grabbed_article_title_NoSpace[2]
        );

        //Reload Page
        $I->reloadPage();
        $I->click(Pageobj_common::$open2_bttn);

        //Article title 4
        $TestSteps->Nwslt_has_articleData(
            $grabbed_article_title[3],
            $grabbed_article_title_NoSpace[3]
        );

        //Reload Page
        $I->reloadPage();
        $I->click(Pageobj_common::$open2_bttn);

        //Article title 5
        $TestSteps->Nwslt_has_articleData(
            $grabbed_article_title[4],
            $grabbed_article_title_NoSpace[4]
        );

         //Reload Page
        $I->reloadPage();
        $I->click(Pageobj_common::$open2_bttn);

        //Article title 6
        $TestSteps->Nwslt_has_articleData(
            $grabbed_article_title[5],
            $grabbed_article_title_NoSpace[5]
        );

        //Reload Page
        $I->reloadPage();
        $I->click(Pageobj_common::$open2_bttn);

        //Article title 7
        $TestSteps->Nwslt_has_articleData(
            $grabbed_article_title[6],
            $grabbed_article_title_NoSpace[6]
        );


    }

    public function NwslSideBar_verifyElements(\Step\Acceptance\NEWSL_Sidebar $TestSteps,
    $site, $nwsl_sidebarContainer,$nwsl_sidebarSubmitbttn)
    {
        $TestSteps->Verify_nwslSideBar_elements($site, $nwsl_sidebarContainer);
    }

    public function NwslSideBar_submitForm(\Step\Acceptance\NEWSL_Sidebar $TestSteps, $pageTitle)
    {
        $TestSteps->Submit_nwslSideBar_form($pageTitle);
    }


    ///---Newsletter TOC generic---///
    public function NewsltTOC_generateEdition(\Step\Acceptance\NewsletterTool $TestSteps, $Base, $Newsletter_SiteNumber_Unique,$nwsl_Site_number)
    {
        $TestSteps->Login_newslt($Base);
        $TestSteps->Create_new_edition($Newsletter_SiteNumber_Unique);
        $TestSteps->dup_newslt_edition_err($nwsl_Site_number);

    }

    public function NewsltTOC_assert(\Step\Acceptance\NewsletterTool $TestSteps)
    {
        $TestSteps->assert_newslt_toc();

    }

    public function NewsltTOC_titleEdit_assert(\Step\Acceptance\NewsletterTool $TestSteps,
      $art1_cord, $art2_cord, $art3_cord, $art4_cord, $art5_cord, $art6_cord)
    {
        $edited_titles = $TestSteps->edit_articleTitle($art1_cord, $art2_cord, $art3_cord, $art4_cord, $art5_cord, $art6_cord);
        $TestSteps->assertTOC_editTitles($edited_titles);

    }



    ///---Newsletter TOC Specific to vertical---///
    Public function NewsltTOC_SelectingArticle_FBT(\Step\Acceptance\NewsletterTool $TestSteps)
    {
        $TestSteps->add_content_to_edition_FBT_featured();
        $TestSteps->selecting_article_newslt_FBT_featured();

        $TestSteps->add_content_to_edition_FBT_top();
        $TestSteps->selecting_article_newslt_FBT_top();
    }

    Public function NewsltTOC_SelectingArticle_HM_ROI(\Step\Acceptance\NewsletterTool $TestSteps)
    {
        $I = $this;

        $TestSteps->add_content_to_edition_HM_ROI_featured();
        $TestSteps->selecting_article_newslt_HM_ROI_featured();


        $TestSteps->add_content_to_edition_HM_ROI_top();
        $TestSteps->selecting_article_newslt_HM_ROI_top();
    }

    public function edit_articleTitle_fbt(\Step\Acceptance\NewsletterTool $TestSteps)
    {
        $I = $this;
        $TestSteps->edit_articleTitle();
    }

}
