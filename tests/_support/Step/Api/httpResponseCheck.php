<?php
namespace Step\Api;
use \Codeception\Util\HttpCode;
use Step\Api\httpResponseCheck as TestSteps;

class httpResponseCheck extends \ApiTester
{

    public function Validate_httpStatus_mainNav(TestSteps $TestStep, $URL, $mainNavLocator)
    {
        //Used to test main navigation -- 404 tests
        $I = $this;
        $mainNavLinks = $I->grabMultiple($mainNavLocator, 'href');
       # codecept_debug($mainNavLinks);
        foreach ($mainNavLinks as $link) {
            $I->amOnUrl($link);
            $I->pauseExecution();
            $I->sendGET($link);
            $I->pauseExecution();
            $I->grabHttpHeader($link);
            $I->canSeeResponseCodeIs(HttpCode::OK);
        }
    }

    public function Validate_httpStatus_subNav(TestSteps $TestStep, $site, $navlocator, $uniqueSubLink = '-')
    {

        //Used to test Sub navigation -- 404 tests
        $I = $this;
        $I->amOnUrl($site);
        $mainNavLinks = $I->grabMultiple($navlocator, 'href');
      #  codecept_debug($mainNavLinks);
        $cnt = count($mainNavLinks);
        $i = 0;
        while ($i < $cnt) {
            codecept_debug($mainNavLinks[$i]);
            $i++;
            $subLinks = $I->grabMultiple("//*[@id=\"block{$uniqueSubLink}mainnavigation\"]/ul/li[{$i}]/ul/li/a", 'href');

            #to test 404
            foreach ($subLinks as $link) {
               // $I->amOnUrl($link);
                $I->amOnUrl($link);
                $I->sendGET($link);
                $I->grabHttpHeader($link);
                $I->canSeeResponseCodeIs(HttpCode::OK);
            }

    }
        }

}