<?php
namespace Step\Api;

use Page\Pageobj_common;
use Yandex\Allure\Adapter\Allure;
use Yandex\Allure\Adapter\Annotation\Title;
use Yandex\Allure\Adapter\Annotation\Features;
use Yandex\Allure\Adapter\Annotation\Stories;

/**
 * @Title("Stack creation test")
 * @Features({"Initialization"})
 * @Stories({"Stack should be a LIFO data structure"})
 */


class PopContentBlock extends \ApiTester
{
    public function GrabTitleText($site, $blockFiller)
    {
        #@Title::{'Grabbing Title Text from RSS Feed'}();
        $I = $this;
        $I->amOnUrl($site);
        $I->seeElement(Pageobj_common::$popContentBlockDiv);
        #$popContentBlockDiv_numElements is the locator that defines the expected of pop content
        #items to be 5 articles. Asserting 5.
        $I->seeNumberOfElements('//*[@id="block-'.$blockFiller.'popularcontentfromparsely"]/ul/li', 5);
        #Grabing the 5 article titles off site markup
        $articleTitles = array   (
            $I->grabTextFrom('//*[@id="block-'.$blockFiller.'popularcontentfromparsely"]/ul/li[1]/h3/a'),
            $I->grabTextFrom('//*[@id="block-'.$blockFiller.'popularcontentfromparsely"]/ul/li[2]/h3/a'),
            $I->grabTextFrom('//*[@id="block-'.$blockFiller.'popularcontentfromparsely"]/ul/li[3]/h3/a'),
            $I->grabTextFrom('//*[@id="block-'.$blockFiller.'popularcontentfromparsely"]/ul/li[4]/h3/a'),
            $I->grabTextFrom('//*[@id="block-'.$blockFiller.'popularcontentfromparsely"]/ul/li[5]/h3/a')
            );


        echo gettype($articleTitles);
        var_dump($articleTitles);
        return $articleTitles;

    }

    public function Grab_rest_Data(BaseFunctions_api $Basefunctions,$index)
    {
        #@Title::{'Grabbing popular Pares.ly article titles'}();
        $I = $this;
        //$this->comment('Period Start date');
        $period_start = date("Y-m-d", strtotime("-1 month"));
        //echo $period_start;

        //$this->comment('Period End Date');
        $period_end = (new \DateTime())->format('Y-m-d');
        //echo $period_end;

        $apikey = $Basefunctions->ParselyAPI_key(Pageobj_common::$parselyCSV_file, $index);
        $secret = $Basefunctions->ParselyAPI_secret(Pageobj_common::$parselyCSV_file, $index);
        $parsely_endpoint = 'https://api.parsely.com/v2/analytics/posts?apikey=' . $apikey . '&secret=' . $secret .
            '&page=1&' . 'period_start=' . $period_start . '&' . 'period_end=' . $period_end .
            '&limit=200&sort=_hits&days=5';


        $responseData = $I->sendGET($parsely_endpoint);
        $I->seeResponseCodeIs(200);
        return $responseData;

    }

    public function VerifyResponseContainsTitleText($site, $blockFiller,
                                                    $Basefunctions, $index_csv)
    {
        #@Title::{'Verifying site markup has parse.ly popular article titles'}();
        $I = $this;
        #defining variable for grabbed pop content article titles:
        $articleTitles = $this->GrabTitleText($site, $blockFiller);
        //print_r($articleTitles);

        $this->Grab_rest_Data($Basefunctions, $index_csv);

        #@Features::{'Verifying that parse.ly popular articles are present in the SUT markup'}();
        //Validating Article Titles, 1-5, are in the $responseData
        $I->canSeeResponseContainsJson(array('title' => $articleTitles[0])); #Article Title 1
        $I->canSeeResponseContainsJson(array('title' => $articleTitles[1])); #Article Title 2
        $I->canSeeResponseContainsJson(array('title' => $articleTitles[2])); #Article Title 3
        $I->canSeeResponseContainsJson(array('title' => $articleTitles[3])); #Article Title 4
        $I->canSeeResponseContainsJson(array('title' => $articleTitles[4])); #Article Title 5
    }

}