<?php
namespace Step\Api;
use Flow\JSONPath\JSONPath;
use Seld\JsonLint\JsonParser;


class DataFeeds extends \ApiTester
{

    ///////----Json Syndication Feeds-----////////


    public function GrabJsonFeed_newscred($BASE_URL)
    {

        $I = $this;
        $I->sendGET($BASE_URL . '/syndication/newscred' );

        #response gets a 200 OK
        $I->seeResponseCodeIs(200);
        #response is valid json //This is done with json_last_error function -http://php.net/manual/en/function.json-last-error.php
        $I->seeResponseContainsJson();
        return;
    }

    public function GrabJsonFeed_lexis($BASE_URL)
    {
        $I = $this;
        $jsonObj_lexis = $I->sendGET($BASE_URL . '/syndication/lexisnexis' );

        #response gets a 200 OK
        $I->seeResponseCodeIs(200);
        #response is valid json //This is done with json_last_error function -http://php.net/manual/en/function.json-last-error.php
        $I->seeResponseContainsJson();
        return;
    }

    public function assert_JsonFeed_timestamp()
    {
        $I = $this;
        $results = $I->grabDataFromResponseByJsonPath('.items[*].date');
        //print_r($results);

        //Determining if there are $dates are older 7 days.
        //If found - will eval to TRUE and assertion will fail.

        foreach ($results as $dates) {
           if(strtotime($dates) < strtotime('8 days ago')) {
               \PHPUnit_Framework_Assert::fail(" timestamp $dates is older than a week \n");
            }
        }
    }

    public function assert_JsonFeed_domin_nextpage($domain)
    {
        $I = $this;
        $results = $I->grabDataFromResponseByJsonPath('.next_page');
        //print_r($results);

        $url = $I->remove_http($domain);

        //asserting 'next_page' node contains "http://{domain}/syndication/"
        \PHPUnit_Framework_Assert::assertContains($url, $results[0]);

    }

    public function assert_JsonFeed_domain_url($domain)
    {
        $I = $this;
        $results = $I->grabDataFromResponseByJsonPath('.items[*].url');

        $url = $I->remove_http($domain);
        foreach ($results as $urls) {
            \PHPUnit_Framework_Assert::assertContains($url, $urls);
            #echo "{$urls} -------> CONTAINS -------> {$domain} \n";
        }
    }

    ///////----XML RSS Feeds-----////////

    public function Validate_XML_Feed($BASE_URL)
    {

        $I = $this;
        $I->sendGET($BASE_URL . '/rss/xml' );

        #response gets a 200 OK
        $I->seeResponseCodeIs(200);
        #response is valid XML This is done with libxml_get_last_error function. http://php.net/manual/en/function.libxml-get-last-error.php
        $I->seeResponseIsXml();
        return;
    }

    public function assert_XML_Feed_pubdate($BASE_URL)
    {
        $I = $this;
        $xml = $I->Grab_XML_feed($BASE_URL);
        $response_array = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($xml), true);
        $y = $response_array['channel']['item'];
        $cnt = count($y);
        #print_r($response_array);

        $i = 0;
        while ($i < $cnt) {
            $i++;
            $dates = $response_array['channel']['item'][$i - 1]['pubDate'];

           if (strtotime($dates) < strtotime('13 days ago')) { #use -13 <<<<<<<<<<<<<<<<<<
               \PHPUnit_Framework_Assert::fail(" Pubdate $dates is older than 12 days \n");
           }
        }
    }

    public function assert_XMLFeed_links_domin($BASE_URL)
    {
        $I = $this;
        $xml = $I->Grab_XML_feed($BASE_URL);
        $response_array = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($xml), true);
        #print_r($response_array);
        $cnt = count($response_array['channel']['item']);

        $i = 0;
        while ($i < $cnt) {
            $i++;
            $url = $I->remove_http($BASE_URL);
            $links = $response_array['channel']['item'][$i - 1]['link'];
            \PHPUnit_Framework_Assert::assertContains($url, $links);
            #echo "{$links} -------> CONTAINS -------> {$url} \n";

        }
    }

    public function assert_XMLFeed_gUid_domin($BASE_URL, $urlPattern)
    {
        $I = $this;
        $xml = $I->Grab_XML_feed($BASE_URL);
        $response_array = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($xml), true);
        #print_r($response_array);
        $cnt = count($response_array['channel']['item']);

        $i = 0;
        while ($i < $cnt) {
            $i++;
            $links = $response_array['channel']['item'][$i - 1]['guid'];
            \PHPUnit_Framework_Assert::assertRegExp($urlPattern, $links);
           # echo "{$links} -------> CONTAINS -------> {$BASE_URL} \n";

        }
    }

}