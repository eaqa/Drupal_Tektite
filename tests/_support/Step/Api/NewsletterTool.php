<?php
namespace Step\Api;

use Page\Pageobj_common;

class NewsletterTool extends \ApiTester
{

    public function GrabRSSdata($BASE_URL)
    {
        $I = $this;
        $I->sendGET($BASE_URL . 'rss/xml');
        #response gets a 200 OK
        $I->seeResponseCodeIs(200);
        #response is valid xml
        $I->seeResponseIsXml();

        #Grabing the  top article titles off xml tree
        return array (
            $I->grabTextContentFromXmlElement(Pageobj_common::$first_title),
            $I->grabTextContentFromXmlElement(Pageobj_common::$second_title),
            $I->grabTextContentFromXmlElement(Pageobj_common::$third_title),
            $I->grabTextContentFromXmlElement(Pageobj_common::$forth_title),
            $I->grabTextContentFromXmlElement(Pageobj_common::$fifth_title),
            $I->grabTextContentFromXmlElement(Pageobj_common::$sixth_title),
            $I->grabTextContentFromXmlElement(Pageobj_common::$seventh_title));

    }

    public function CleanRSSData_Nochars($BASE_URL)
    {
        #function loops through all grabbed article titles, strips all non-alpha chars
        #output is $output_titles array with stripped titles
        $rssData = preg_replace("/[^a-zA-Z]/", " ", $this->GrabRSSdata($BASE_URL));
        $output_titles = array();

        foreach ($rssData as $CleanedTitles_noChar) {
            codecept_debug($CleanedTitles_noChar);
            echo gettype($CleanedTitles_noChar);
            array_push($output_titles,$CleanedTitles_noChar);
        }
        codecept_debug($output_titles);
        echo gettype($output_titles);
        return $output_titles;
    }

    public function CleanRSSData_NoTrailingSpace($BASE_URL)
    {
        $rssData = array_filter(array_map('trim', $this->GrabRSSdata($BASE_URL)));
        $output_titles_noSpace = array();

        foreach ($rssData as $CleanedTitles_noSpace) {
            codecept_debug($CleanedTitles_noSpace);
            echo gettype($CleanedTitles_noSpace);
            array_push($output_titles_noSpace, $CleanedTitles_noSpace);
        }
        codecept_debug($output_titles_noSpace);
        echo gettype($output_titles_noSpace);
        return $output_titles_noSpace;

    }

}