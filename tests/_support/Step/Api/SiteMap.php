<?php
namespace Step\Api;
use Page\Pageobj_common;

class SiteMap extends \ApiTester
{
    public function Grab_SiteMapxml($siteDomain, $node, $xmlURL, $treeNode, $modifier = '')
    {
        $xmlString = file_get_contents("http://{$siteDomain}.com{$modifier}/{$xmlURL}");
        $xml = simplexml_load_string($xmlString);

print $xmlString;
        foreach($xml->$treeNode as $item)
        {
            $device = array();

            foreach($item as $key =>$value)
            {
                $device[(string)$key] = (string)$value;
//print_r($device);
            }

            $devices[] = $device[$node]; //[$node]
        }

        return $devices;


    }

    public function AssertDomain_SiteMapxml($siteDomain, $node, $xmlURL, $treeNode, $modifier = '')
    {
        $output_data = $this->Grab_SiteMapxml($siteDomain, $node, $xmlURL, $treeNode, $modifier);
        $XMLstring_version = implode(',',$output_data);
        \PHPUnit_Framework_Assert::assertContains($siteDomain, $XMLstring_version);
    }

    public function Assert_URL_SiteMapIndex($siteDomain, $node, $xmlURL, $treeNode, $modifier = '')
    {
        $output_data = $this->Grab_SiteMapxml($siteDomain, $node, $xmlURL, $treeNode, $modifier);

        foreach ($output_data as $url) {
            if (filter_var($url, FILTER_VALIDATE_URL)) {
                echo("$url is a valid URL \n");

            } else {
                \PHPUnit_Framework_Assert::fail("$url is not a vaild URL");
            }
        }
    }


    public function AssertTimeStamp_SiteMapxml($siteDomain, $node, $xmlURL, $treeNode, $modifier = '')
    {
        $output_data = $this->Grab_SiteMapxml($siteDomain, $node, $xmlURL, $treeNode, $modifier);
        //print_r($output_data);

        $I = $this;
        $now = new \DateTime('now', new \DateTimeZone('America/New_York'));
        $format = 'Y-m-d';
        foreach($output_data as $x) {
            $lastModified = new \DateTime($x); #2017-06-14T01:00:57-04:00
            // print_r($lastModified);
            if ($now->format($format) === $lastModified->format($format)) ;

            //$I->assertEquals($now->format($format), $lastModified->format($format));
            \PHPUnit_Framework_Assert::assertEquals($now->format($format), $lastModified->format($format));
        }

    }

    public function AssertNodeURL_SiteMapxml($siteDomain, $node, $xmlURL, $treeNode, $modifier = '')
    {
        $output_data = $this->Grab_SiteMapxml($siteDomain, $node, $xmlURL, $treeNode, $modifier);
        $XMLstring_version = implode(',',$output_data);
        $count = substr_count($XMLstring_version, '/node/');

        $I = $this;
        #Asserting that $count (number of found nodes urls) is <= to 2
        if ($I->assertLessThanorEqual('2', $count, 'number of found nodes urls is >2')) {
        } else {
        }
        print "This is the amount of '/node/' found - " . $count;
    }
}
