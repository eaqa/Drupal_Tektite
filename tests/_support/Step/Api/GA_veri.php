<?php
namespace Step\Api;
use Page\Pageobj_common;

require_once '/Users/erikanderson/Tektite_refacor/vendor/autoload.php';

class GA_veri extends \ApiTester
{
    public function GA_rta(BaseFunctions_api $Basefunctions, $index)
    {
        //Google RealTime Connect
        $client = new \Google_Client();
        $client->setApplicationName("Google Realtime Analytics");
        $client->setAuthConfig('/Users/erikanderson/Documents/CodeceptionCsvFiles/GArtTEK-39151a545cd0.json');
        $client->addScope('https://www.googleapis.com/auth/analytics.readonly');
        $GA_VIEW_ID = $Basefunctions->GA_API_viewIDs(Pageobj_common::$GA_CSV_file, $index);
        $service = new \Google_Service_Analytics($client);
        try {
            $result = $service->data_realtime->get(
                $GA_VIEW_ID,
                'rt:activeVisitors'
            );
            $count = $result->totalsForAllResults['rt:activeVisitors'];
            echo $count;
            return $count;
        } catch(\Exception $e) {
            var_dump($e);
        }
    }

    public function VerifyGA_rtaHits(BaseFunctions_api $Basefunctions, $index)
    {
        $this->assertGreaterThan('1', $this->GA_rta($Basefunctions, $index), 'Real-time site view count is >1');

       //print_r($this->GA_rta($Basefunctions, $index));
    }

}