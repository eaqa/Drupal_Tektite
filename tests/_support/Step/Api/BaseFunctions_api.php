<?php
namespace Step\Api;

class BaseFunctions_api extends \ApiTester
{

    public function ParselyAPI_key($parselyCSV_file, $index)
    {
        //Getting apikey and secret
        #using lib Rap2hpoutre to get data from csv file
        $parse = \Rap2hpoutre\Csv\csv_to_associative_array($parselyCSV_file);
        codecept_debug($parse);

       #creating the apikey variable
        $arr1 = $parse[$index];
        $apikey = $arr1['apikey'];
        codecept_debug($apikey);
        return $apikey;

    }

    public function ParselyAPI_secret($parselyCSV_file, $index)
    {
        //Getting apikey and secret
        #using lib Rap2hpoutre to get data from csv file
        $parse = \Rap2hpoutre\Csv\csv_to_associative_array($parselyCSV_file);
        codecept_debug($parse);

        #creating the $secret variable
        $arr2 = $parse[$index];
        $secret = $arr2['secret'];
        codecept_debug($secret);
        return $secret;
    }

    public function GA_API_viewIDs($GA_CSV_file, $index)
    {
        //Getting apikey and secret
        #using lib Rap2hpoutre to get data from csv file
        $parse = \Rap2hpoutre\Csv\csv_to_associative_array($GA_CSV_file);
       # codecept_debug($parse);

        #creating the $secret variable
        $arr2 = $parse[$index];
        $viewID = $arr2['GA_VIEW_ID'];
        codecept_debug($viewID);
        return $viewID;
    }


}