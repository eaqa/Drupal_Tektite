<?php
namespace Step\Api;
use Page\Pageobj_unique;
require "/Users/erikanderson/Tektite_refacor/vendor/autoload.php";
use PHPHtmlParser\Dom;


class Solr extends \ApiTester
{


    public function fetchArticleData($articleURL)
    {
        $I = $this;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.aylien.com/api/v1/entities?url={$articleURL}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: 69a9a3a7-11ce-c369-ac38-6157535b9aab",
                "x-aylien-textapi-application-id: cd121299",
                "x-aylien-textapi-application-key: b47efd10722515f6d526acd3282c154f"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;

            $array = json_decode($response, true);
            //print_r($array);

            $keywords = $array['entities']['keyword'];
            //print_r($keywords);
            return $keywords; // returns an array of a dozen or more keywords, from given article url
        }

    }


    public function AssertKeywordQuery($BASE_URL, $keywordTerm_encoded, $articleKeyword_decode, $SearchH1Blk)
    {
        $I = $this;
        $dom = new Dom;
        $dom->loadFromUrl("{$BASE_URL}search-results/{$keywordTerm_encoded}");
        $Domtitle  = $dom->find("//*{$SearchH1Blk}/h1");
        $DomElement = $dom->getElementsByClass('results-showing-counter');
        $DomPageTitle = $dom->getElementsByTag('title');

        $title = (string)$Domtitle;

        $DomElement_pre = preg_replace('/<[^>]*>/', '', $DomElement);
        preg_match_all('/(\d+)(?!.*\d)/', $DomElement_pre, $numfound_arr);
        $numfound = $numfound_arr[0][0];

        //Assert 1
        #assert search <h1> has the correct copy
        \PHPUnit_Framework_Assert::assertSame("<h1 class=\"page-title\"><span>Search Results: </span>{$articleKeyword_decode} </h1>", $title, "Search Title does not have Search Term");
        \PHPUnit_Framework_Assert::assertNotContains('Default', $title, 'Search has \'Default\' copy');

        //Assert 2
        #assert Search Term has hits >= to 1
        \PHPUnit_Framework_Assert::assertGreaterThanOrEqual(1, $numfound, "There are Zero Search Hits");

        //Assert 3
        #assert page title has the correct copy
        $PageTitle = (string)$DomPageTitle;

        \PHPUnit_Framework_Assert::assertContains("Search Results: {$articleKeyword_decode} ", $PageTitle);
        \PHPUnit_Framework_Assert::assertNotContains('Default', $DomPageTitle);

        //Assert 4
        #assert 'results-showing-counter' class is on search results page
        if ($I->seeDomHasElement($DomElement)) {
            echo "\n >>>>>Keyword has search hits<<<<< \n";
        } else {
            #else no search results means 'no-results-message' class is on page -> assertion fails
            $failmessage = $dom->getElementsByClass('no-results-message');
            \PHPUnit_Framework_Assert::fail($failmessage);
        }
        return $numfound;
    }
}