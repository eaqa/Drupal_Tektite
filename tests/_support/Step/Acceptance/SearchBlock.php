<?php
namespace Step\Acceptance;

use Page\Pageobj_common;
use \Facebook\WebDriver\WebDriverKeys;

class SearchBlock extends \AcceptanceTester
{

    public function SeeSearchElements($SearchBlockDiv)
    {
        $I = $this;
        $I->seeElement(Pageobj_common::$searchIcon);
        $I->click(Pageobj_common::$searchIcon);
        $I->seeElement($SearchBlockDiv);
        $I->waitForElementVisible(Pageobj_common::$searchShelf);
        $I->seeElement(Pageobj_common::$searchShelf);
    }

    public function ConductSearch_verify($pageBodyDiv, $SearchTerm, $SearchField)
    { //#edit-keys"
        $I = $this;
        //Searching for 'Test'
        $I->fillField($SearchField, $SearchTerm);
        //Submitting search field, hitting the RETURN KEY
        $I->pressKey($SearchField,WebDriverKeys::RETURN_KEY);

        //Verifying user is on the search page
        $I->canSeeCurrentUrlMatches('~/search-results/' . $SearchTerm . '~');
        //with search term 'test'
        $I->canSeeElement($pageBodyDiv);
        $I->canSee($SearchTerm, $pageBodyDiv);
    }

}