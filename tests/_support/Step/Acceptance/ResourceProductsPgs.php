<?php
namespace Step\Acceptance;

use Page\Pageobj_common;

class ResourceProductsPgs extends \AcceptanceTester
{
    public function productsDiv()
    {
        $I = $this;
        $I->canSeeElement(Pageobj_common::$filterButtonDiv_products);
        $I->canSeeElement(Pageobj_common::$categoryFilterButton);
        $I->canSeeElement(Pageobj_common::$sortOrderFilterButton);
    }

    public function resourcesDiv()
    {
        $I = $this;
        $I->canSeeElement(Pageobj_common::$filterButtonDiv_resources);
        $I->canSeeElement(Pageobj_common::$selectTypeFilterButton_resources);
        $I->canSeeElement(Pageobj_common::$categoryFilterButton);
        $I->canSeeElement(Pageobj_common::$sortOrderFilterButton);
    }

    public function filterMenuNotEmpty($LocatorOpts)
    {
        $I = $this;
        $array = $I->grabMultiple($LocatorOpts);
        $sum = count($array);
        $I->comment("There are a total Menu suboptions: $sum");
        $I->wait(2);
        $I->seeNumberOfElements($LocatorOpts, $sum);
    }

    public function selectBttnOpts($filterbttn, $opt)
    {
        $I = $this;
        $I->selectOption($filterbttn, $opt);
        $I->wait(4);
        $I->canSeeOptionIsSelected($filterbttn, $opt);
    }

    public function contentWellNotEmpty($LocatorOpts, $filterbttnContentWEll, $optContentWEll)
    {
        $I = $this;
        $I->selectOption($filterbttnContentWEll, $optContentWEll);
        $I->wait(2);
        $array = $I->grabMultiple($LocatorOpts);
        //$I->wait(2);
        $sum = count($array);
        $I->comment("There are a total of Main Content well rows: $sum");
        $I->wait(3);
        $I->canSeeNumberOfElements($LocatorOpts, $sum);

    }

}