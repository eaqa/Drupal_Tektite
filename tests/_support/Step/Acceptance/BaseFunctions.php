<?php
namespace Step\Acceptance;
use Page\Pageobj_common;

class BaseFunctions extends \AcceptanceTester
{

    public function loginSSO($loginCSV_file, $userSite_tile)
    {
        $I = $this;
        //Getting user credentials
        #using lib Rap2hpoutre to get data from csv file
        $parse = \Rap2hpoutre\Csv\csv_to_associative_array($loginCSV_file);
        #codecept_debug($parse);

        #creating the $usrname variable
        $arr1 = $parse[0];
        $name = $arr1['username'];
        #codecept_debug($name);

        #creating the $pswrd variable
        $arr1 = $parse[0];
        $password = $arr1['password'];
        #codecept_debug($password);

        //login to SSO
        $I = $this;

        $I->amOnUrl('https://questex.onelogin.com/login');
        $I->submitForm('#login-form', [
            'email' => $name,
            'password' => $password
        ]);
        #now fully logged into the sso dashboard
        $I->wait(3);
        $I->seeInCurrentUrl('/client/apps'); //confirming we are on the sso dashboard
        // Onelogin root -> specific site tile
        $I->amOnUrl(Pageobj_common::$OneLoginRoot_url . $userSite_tile);
    }

    public function OneLogIn_AppUrl($userSite_tile)
    {
        $I = $this;
        $I->amOnUrl(Pageobj_common::$OneLoginRoot_url . $userSite_tile);
    }

    public function goToPage($BASE_URL, $relitive_url)
    {
        $I = $this;
        $I->amOnUrl( $BASE_URL. $relitive_url);
    }

    public function scrollClick($wait, $scroll, $click)
    {
        $I = $this;
        $I->waitForElement($wait);
        $I->scrollTo($scroll);
        $I->click($click);
    }

    public function uploadFile($uploadField, $imgFileName, $altTextField, $text)
    {
        $I = $this;
        $I->attachFile($uploadField, $imgFileName);
        $I->wait(5);
        $I->waitForElementVisible($altTextField);
        $I->fillField($altTextField, $text);
    }

}