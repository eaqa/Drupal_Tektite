<?php
namespace Step\Acceptance;

use Page\Pageobj_common;
use \Codeception\Util\Locator;

class NEWSL_Sidebar extends \AcceptanceTester
{
    public function Verify_nwslSideBar_elements($site, $nwsl_sidebarContainer)
    {
        $I = $this;
        $I->amOnUrl($site);
        //Verify Opt-in form div is present on the page
        $I->seeElement($nwsl_sidebarContainer);
        //Verify form field exists
        $I->seeElement(Pageobj_common::$nwsl_emailField);
        //Verify form submit button exists
        #sumbit button locator
        $I->seeElement(Locator::find('button', ['class' => 'mktoButton']));
    }

    public function Submit_nwslSideBar_form($pageTitle)
    {
        $I = $this;
        $I->fillField(Pageobj_common::$nwsl_emailField,"questexqa@gmail.com");
        #sumbit button locator
        $I->click(Locator::find('button', ['class' => 'mktoButton']));
        $I->waitForElement(Pageobj_common::$mainForm_headerLogo);
        $I->canSeeInTitle($pageTitle);
        //see submitted email in main form
        $I->seeInField(Pageobj_common::$nwsl_emailField,'questexqa@gmail.com');
    }
}