<?php
namespace Step\Acceptance;
use Page\Pageobj_NavLinks;

class MenuNavLinksVeri extends \AcceptanceTester
{

   public function MainNav_Menu_verify($site, $navlocator, $preArray)
    {
        $verifyArray = array_keys($preArray[$site]);
        $I = $this;
        $I->amOnUrl($site);
        $mainNavLinks = $I->grabMultiple($navlocator, 'href');
        codecept_debug($mainNavLinks);
        $cnt = count($mainNavLinks);
        $i = 0;
        while ($i < $cnt) {
            codecept_debug($mainNavLinks[$i]);
            $i++;
            $subLinks = $I->grabMultiple("//*[@id=\"block-mainnavigation\"]/ul/li[{$i}]/ul/li/a", 'href');

            codecept_debug($subLinks);
            //codecept_debug(key(next(Pageobj_NavLinks::$Main_navLinks_fbt[$site])));
            //$I->assertEquals($verifyArray, $mainNavLinks, 'Main nav links do not match the pre-array' );

        }
     #   $I->assertEquals($verifyArray, $mainNavLinks, 'Main nav links do not match the pre-array' );
        \PHPUnit_Framework_Assert::assertEquals($verifyArray, $mainNavLinks, 'Main nav links do not match the pre-array' );
    }

    public function SubNav_Menu_verify($site, $navlocator, $preArray, $uniqueSubLink = '-')
    {
        $verifyArray = array_keys($preArray[$site]);
        $I = $this;
        $I->amOnUrl($site);
        $mainNavLinks = $I->grabMultiple($navlocator, 'href');
        codecept_debug($mainNavLinks);
        $cnt = count($mainNavLinks);
        $i = 0;
        while ($i < $cnt) {
            codecept_debug($mainNavLinks[$i]);
            $i++;
            $subLinks = $I->grabMultiple("//*[@id=\"block{$uniqueSubLink}mainnavigation\"]/ul/li[{$i}]/ul/li/a", 'href');

            codecept_debug($subLinks);
            //codecept_debug(key(next(Pageobj_NavLinks::$Main_navLinks_fbt[$site])));

          #  $I->assertEquals($subLinks, $preArray[$site][$mainNavLinks[$i - 1]], 'Main Sub nav links do not match the pre-array');
            \PHPUnit_Framework_Assert::assertEquals($subLinks, $preArray[$site][$mainNavLinks[$i - 1]], 'Main Sub nav links do not match the pre-array');
        }

    }
    public function Nav_verify($site, $navlocator, $preArray)
    {
        $I = $this;
        $I->amOnUrl($site);
        $NavLinks = $I->grabMultiple($navlocator, 'href');
        codecept_debug($NavLinks);
        codecept_debug($preArray);

        //assert(($preArray === $TopNavLinks),1);
        #$I->assertEquals($preArray, $NavLinks, 'Top nav links do not match the pre-array');
        \PHPUnit_Framework_Assert::assertEquals($preArray, $NavLinks, 'Top nav links do not match the pre-array');

    }

}