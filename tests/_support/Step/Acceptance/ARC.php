<?php
namespace Step\Acceptance;
use Codeception\Lib\Driver\Facebook;
use Page\Pageobj_common;
use Codeception\Util\Locator;

class ARC extends \AcceptanceTester
{
    public function ARC_insertBrowsedImg($arc_imgSearchTerm, BaseFunctions $Base)
    {
        $I = $this;
        $I->waitForElement(Pageobj_common::$browseBtn);
        $I->executeJS('window.scrollTo(126, 681);');
        $I->pauseExecution();
        $I->click(Pageobj_common::$browseBtn);
        $I->makeScreenshot("(1)Browse Img: ARC iFrame Shows");
        $I->switchToIFrame('arc-browse-iframe');
        $I->waitForElementVisible(Pageobj_common::$arcTitleSearchFld);
        $I->fillField(Pageobj_common::$arcTitleSearchFld, $arc_imgSearchTerm); #searching for img in ARC browse
        $I->click(Pageobj_common::$arcSearchApplybtn);
        $I->makeScreenshot("(2)ARC Filter View");
        $I->waitForElementVisible(Pageobj_common::$arcSelectRadiobtn);
        $I->click(Pageobj_common::$arcSelectRadiobtn);
        $I->switchToWindow();

        #insert browsed ARC img
        $Base->scrollClick(Pageobj_common::$arcInsertAssetbtn, Pageobj_common::$arcInsertAssetbtn, Pageobj_common::$arcInsertAssetbtn);
        #wating for element, then scrolling to element, then clicking element
    }

    public function fillOut_ReqCreateArticleFields($author, $primaryTaxonomy, $secondaryTaxonomy, $keyword, $topic)
    {
        $I = $this;
        $I->waitForElementVisible("#edit-title-0-value");
        $I->scrollTo(Pageobj_common::$arc_HeadlineFld);
        $I->wait(7);
        $I->fillField(Pageobj_common::$arc_HeadlineFld, "Test Article");
        $I->fillField(Pageobj_common::$arc_altHeadlineFld, 'Test Article');
        $I->fillField(Pageobj_common::$arc_introFld, 'Test Article');
        $I->fillField(Pageobj_common::$arc_introTeaserFld, 'Test Article');
        $I->fillField(Pageobj_common::$arc_authorFld, $author);
        $I->selectOption('//*[@id="edit-field-primary-taxonomy"]', $primaryTaxonomy);
        $I->checkOption($secondaryTaxonomy);
        $I->fillField('//*[@id="edit-field-keywords-0-target-id"]', $keyword);
        $I->fillField('//*[@id="edit-field-topics-0-target-id"]', $topic);
    }

    public function verifyImgOnPreviewpg($imgFileName, BaseFunctions $Base)
    {
        $I = $this;
        //$I->pauseExecution();
        //$I->click(Pageobj_common::$previewbutton);
        $Base->scrollClick(Pageobj_common::$previewbutton, Pageobj_common::$previewbutton, Pageobj_common::$previewbutton);
        $I->wait(3);
        $I->makeScreenshot('User on preview article page, with image');
        $I->canSeeInPageSource($imgFileName); //<<<<<
    }

    public function ARC_insertUploadedImg(BaseFunctions $Base)
    {
        $I = $this;
        $this->DiscardDraft();
        $I->waitForElement(Pageobj_common::$uploadBtn);
        $I->executeJS('window.scrollTo(62, 1000);');
        $I->pauseExecution();
        $I->click(Pageobj_common::$uploadBtn);
        $I->makeScreenshot("(6)ARC_upload iFrame is visible");

        ///////$I->switchToIFrame('arc-upload-iframe'); #todo<--changed for ARC Update
        $I->pauseExecution();
        $I->waitForElementVisible(Pageobj_common::$arcUpload_titlefld);
        $I->fillField(Pageobj_common::$arcUpload_titlefld, 'test_image');
        $I->fillField(Pageobj_common::$arcUpload_altTxtfld, 'test_image');
        $I->fillField(Pageobj_common::$arcUpload_tagfld, 'test_image');
        $I->selectOption(Pageobj_common::$arcUpload_RightsReqfld, 'No');
        //uploading image
        $I->attachFile(Pageobj_common::$arcUpload_imgUploadfld, 'ARC_Upload_Test_IMG.jpg');
        $I->waitForText('ARC_Upload_Test_IMG.jpg');

        //saving image
        //$I->waitForElement(Pageobj_common::$arcUpload_imgSavebtn);
        $I->executeJS('window.scrollTo(85, 646);');
        //$I->scrollTo(Pageobj_common::$arcUpload_imgSavebtn);
        $I->waitForElement(Pageobj_common::$arcUpload_imgSavebtn);
        $I->click(Pageobj_common::$arcUpload_imgSavebtn);
        $I->wait(2);
        ////////$I->switchToWindow(); #todo<--changed for ARC Update
        //$Base->scrollClick(Pageobj_common::$arcInsertAssetbtn, Pageobj_common::$arcInsertAssetbtn, Pageobj_common::$arcInsertAssetbtn);

        //Validating that the Primary Image field is populated
        $I->waitForText('Select a label to make the article on the homepage and newsetter display an icon and/or message');
        $I->wait(7);
        $I->dontSeeFieldIsEmpty($y = $I->grabValueFrom(Pageobj_common::$arc_primaryImgFld));
        $I->comment('PRIMARY IMAGE ID ='); codecept_debug($y);
    }

    public function ARC_insertRichTextEditorImg()
    {
        $I = $this;
        //$Base->scrollClick(Pageobj_common::$editor_arc_browserbtn, Pageobj_common::$editor_arc_browserbtn, Pageobj_common::$editor_arc_browserbtn);
        $I->waitForElement(Pageobj_common::$editor_arc_browserbtn);
        $I->executeJS('window.scrollTo(126, 681);');
        $I->pauseExecution();
        $I->click(Pageobj_common::$editor_arc_browserbtn);

        $I->wait(5);

        $I->executeInSelenium(function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
            $my_frame = $webDriver->findElement(\WebDriverBy::xpath('.//iframe[@class=\'cke_dialog_ui_iframe\']'));
            $webDriver->switchTo()->frame($my_frame);
        });



        $I->waitForElementVisible(Pageobj_common::$arcTitleSearchFld);
        $I->fillField(Pageobj_common::$arcTitleSearchFld, Pageobj_common::$arc_imgSearchTerm_2); #searching for img in ARC browse
        $I->click(Pageobj_common::$arcSearchApplybtn);
        $I->makeScreenshot("(2)ARC Filter View");
        $I->waitForElementVisible(Pageobj_common::$arcSelectRadiobtn2); //<<<<<
        $I->click(Pageobj_common::$arcSelectRadiobtn2);

        $I->switchToWindow();
       //  $Base->scrollClick(Pageobj_common::$editor_arc_insertBtn, Pageobj_common::$editor_arc_insertBtn, Pageobj_common::$editor_arc_insertBtn);
        $I->executeInSelenium(function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
            $webDriver->findElement(\WebDriverBy::xpath("//*[@title=\"Arc Insert Selected\"]"))->click();
        });


        $I->wait(3);
        $I->makeScreenshot('(11)verify image is inserted into ckeditor'); #img is inserted into ckeditor
    }

    public function ARC_upload_RichTextEditorImg()
    {
        $I = $this;
        $I->pauseExecution();
        //if a previous draft dialog popup shows, dismiss by discarding draft and exit function
        //if a previous draft dialog popup dose not show (that is no draft exists), skip function and continue
        $this->DiscardDraft();
        $I->waitForElement(Pageobj_common::$editor_arc_uploadbtn);
        $I->executeJS('window.scrollTo(126, 681);');
        $I->click(Pageobj_common::$editor_arc_uploadbtn);
        $I->waitForElementVisible(Pageobj_common::$arcUpload_titlefld);
        $I->fillField(Pageobj_common::$arcUpload_titlefld, 'test_image');
        $I->fillField(Pageobj_common::$arcUpload_altTxtfld, 'test_image - ARC CKEditor Upload Test image');
        $I->fillField(Pageobj_common::$arcUpload_tagfld, 'test_image');
        $I->selectOption(Pageobj_common::$arcUpload_RightsReqfld, 'No');
        //uploading image
        $I->attachFile(Pageobj_common::$arcUpload_imgUploadfld, 'ARC_Upload_Test_IMG_ckeditor.jpg');
        $I->waitForText('ARC_Upload_Test_IMG_ckeditor.jpg');

        //saving image
        //$I->waitForElement(Pageobj_common::$arcUpload_imgSavebtn);
        $I->executeJS('window.scrollTo(85, 646);');
        //$I->scrollTo(Pageobj_common::$arcUpload_imgSavebtn);
        $I->waitForElement(Pageobj_common::$arcUpload_imgSavebtn);
        $I->click(Pageobj_common::$arcUpload_imgSavebtn);
        $I->wait(5);
        $I->pauseExecution();
    }

    public function DiscardDraft()
    {
        $I = $this;
        if ($I->seePageHasElement(Pageobj_common::$draftPop_container)) {
            $I->click(Pageobj_common::$draftPop_discardBttn);
        }

    }
}