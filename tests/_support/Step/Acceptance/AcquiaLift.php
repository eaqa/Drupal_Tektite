<?php
namespace Step\Acceptance;
use Page\Pageobj_common;

class AcquiaLift extends \AcceptanceTester
{

    public function verifyAqLiftConfig($configCSV_file)
    {
        $I = $this;
        #using lib Rap2hpoutre to get data from csv file
        $parse = \Rap2hpoutre\Csv\csv_to_associative_array($configCSV_file);
        codecept_debug($parse);

        #creating variables
        $arr1 = $parse[0];
        $accountName = $arr1['Account Name'];
        $customerSite = $arr1['Customer Site'];
        $apiUrl = $arr1['API URL'];
        $apiAccessKey = $arr1['API Access Key'];
        $jsPath = $arr1['JavaScript Path'];
        $identPar = $arr1['Identity Parameter'];
        $identTypePar = $arr1['Identity Type Parameter'];
        $defaultIdentType = $arr1['Default Identity Type'];
        $contentSec = $arr1['Content Section'];
        $contentKeywrds = $arr1['Content Keywords'];
        $persona = $arr1['Persona'];

        #credential settings tab
        $I->seeInField(Pageobj_common::$accountName_fld, $accountName);
        $I->seeInField(Pageobj_common::$customerSite_fld, $customerSite);
        $I->seeInField(Pageobj_common::$apiUrl_fld, $apiUrl);
        $I->seeInField(Pageobj_common::$apiAccessKey_fld, $apiAccessKey);
        $I->seeInField(Pageobj_common::$jsPath_fld, $jsPath);

        #identity settings tab
        $I->seeInField(Pageobj_common::$identPar_fld, $identPar);
        $I->seeInField(Pageobj_common::$identTypePar_fld, $identTypePar);
        $I->seeInField(Pageobj_common::$defaultIdentType_fld, $defaultIdentType);

        #field mappings tab
        $I->seeElement(Pageobj_common::$fldMappings_lnk);
        $I->click(Pageobj_common::$fldMappings_lnk);
        $I->waitForElement(Pageobj_common::$contentSec_fld);
        $I->seeInField(Pageobj_common::$contentSec_fld, $contentSec);
        $I->seeInField(Pageobj_common::$contentKeywrds_fld, $contentKeywrds);
        $I->seeInField(Pageobj_common::$persona_fld, $persona);

    }

}