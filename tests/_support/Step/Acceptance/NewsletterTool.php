<?php
namespace Step\Acceptance;

use Facebook\WebDriver\WebDriverAction;
use Facebook\WebDriver\WebDriverElement;
use function foo\func;
use Page\Pageobj_common;
use \Codeception\Util\Locator;
use Facebook\WebDriver\Remote\RemoteWebDriver as webdriver;

class NewsletterTool extends \AcceptanceTester
{


    public function Login_navigate_to_nwsl(BaseFunctions $Base, $nwsl_Site_number)
    {
        $I = $this;
        $Base->loginSSO(
            Pageobj_common::$csvFile_admin,
            Pageobj_common::$EngineerSSO_NWSLTool);
        $I->amOnUrl('https://qtxnewsletter.com/node/' . $nwsl_Site_number . '/panelizer/default/content');
        $I->click(Pageobj_common::$open2_bttn);

    }

    public function NavigateToEdition($nwsl_Site_number)
    {
        $I = $this;
        $I->amOnUrl('https://questex.onelogin.com/client/apps/select/90356869');
        $I->amOnUrl('https://qtxnewsletter.com/node/' . $nwsl_Site_number . '/panelizer/default/content');
        $I->click(Pageobj_common::$open2_bttn);
    }

    public function Nwslt_has_articleData($grabbed_article_title, $grabbed_article_title_NoSpace)
    {
        $I = $this;
        $I->wait(3);
        $I->fillField(Pageobj_common::$keywords_txtbox, $grabbed_article_title);
        $I->click(Pageobj_common::$apply_bttn);
        $I->wait(5);
        $I->canSee($grabbed_article_title_NoSpace);
    }

    /*Newsletter tool TOC tests*/
    //Logs into Stage//

    public function Login_newslt(BaseFunctions $Base)
    {
        $Base->loginSSO(
            Pageobj_common::$csvFile_admin,
            Pageobj_common::$EngineerSSO_NWSLTool_stage);
    }

    public function Create_new_edition($nwsl_Site_number)
    {
        $I = $this;

        $I->amOnUrl("https://stage.qtxnewsletter.com/create-edition/{$nwsl_Site_number}/clone");
        $autoTxt = $I->grabAttributeFrom(Pageobj_common::$title_field, 'value');
        echo $autoTxt;
        $I->pauseExecution();
        //$I->see("Edit Edition {$vertical_title} - (Tektite) " . date('m/d/Y'));
        $I->fillField(Pageobj_common::$title_field, "QA Test {$autoTxt}--" . mt_rand(1, 100));
        $I->pauseExecution();
        $I->scrollTo(Pageobj_common::$save_bttn);
        $I->wait(2);
        $I->click(Pageobj_common::$save_bttn);
        $I->comment("\n do you see err message \n");
    }

    public function add_content_to_edition_FBT_featured()
    {

        $featured_bar = Pageobj_common::$newslt_sec_featured_bar;
        $I = $this;
        $I->comment("add_content_to_edition<-------------");
        $I->pauseExecution();
        $I->waitForElement(Pageobj_common::$newslt_panelizer_subMenu);

        //preparing for drag and drop
        $I->scrollTo($featured_bar);
        $I->doubleClick($featured_bar);

        $I->wait(3);

        $I->click(Pageobj_common::$open2_bttn);
    }

    public function add_content_to_edition_FBT_top()
    {

        $top_bar = Pageobj_common::$nweslt_sec_top_bar;

        $I = $this;
        $I->comment("add_content_to_edition<-------------");
        $I->pauseExecution();
        $I->waitForElement(Pageobj_common::$newslt_panelizer_subMenu);

        $I->click(Pageobj_common::$Close2_bttn);

        //preparing for drag and drop
        $I->scrollTo($top_bar);
        $I->doubleClick($top_bar);

        $I->wait(3);


        $I->click(Pageobj_common::$open2_bttn);
    }



    public function selecting_article_newslt_FBT_featured()
    {
        //Drag and dropping articles into Edition Sections
        $I = $this;
        $I->wait(5);


        //----Featured Stores-----//
        $I->executeInSelenium(function (webdriver $webDriver) {
            $art1 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art2));
            $loc1 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_featured));
            $webDriver->action()
                ->moveToElement($art1)
                ->clickAndHold($art1)
                ->moveToElement($loc1)
                ->release($loc1)
                ->perform();
        });
        $I->pauseExecution();
        $I->wait(3);

        $I->executeInSelenium(function (webdriver $webDriver) {
            $art2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art1));
            $loc1 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_featured));
            $webDriver->action()
                ->moveToElement($art2)
                ->clickAndHold($art2)
                ->moveToElement($loc1)
                ->release($loc1)
                ->perform();
        });
        $I->pauseExecution();
        $I->wait(3);

    }

        //----Top Stores-----//

    public function selecting_article_newslt_FBT_top()
    {
        $I = $this;
        $I->executeInSelenium(function (webdriver $webDriver) {
            $art3 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art6));
            $loc2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_top));
            $webDriver->action()
                ->moveToElement($art3)
                ->clickAndHold($art3)
                ->moveToElement($loc2)
                ->release($loc2)
                ->perform();
        });
        $I->pauseExecution();
        $I->wait(3);

        $I->executeInSelenium(function (webdriver $webDriver) {
            $art3 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art5));
            $loc2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_top));
            $webDriver->action()
                ->moveToElement($art3)
                ->clickAndHold($art3)
                ->moveToElement($loc2)
                ->release($loc2)
                ->perform();
        });
        $I->pauseExecution();
        $I->wait(3);

        $I->executeInSelenium(function (webdriver $webDriver) {
            $art3 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art4));
            $loc2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_top));
            $webDriver->action()
                ->moveToElement($art3)
                ->clickAndHold($art3)
                ->moveToElement($loc2)
                ->release($loc2)
                ->perform();
        });

        $I->wait(3);
        $I->pauseExecution();
        $I->executeInSelenium(function (webdriver $webDriver) {
            $art4 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art3));
            $loc2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_top));
            $webDriver->action()
                ->moveToElement($art4)
                ->clickAndHold($art4)
                ->moveToElement($loc2)
                ->release($loc2)
                ->perform();
        });



        $I->pauseExecution();
    }


    ////---HM ROI TOC Tests-----////
    public function add_content_to_edition_HM_ROI_featured()
    {

        $featured_bar = Pageobj_common::$newslt_sec_featured_bar;

        $I = $this;
        $I->comment("add_content_to_edition<-------------");
        $I->pauseExecution();
        $I->waitForElement(Pageobj_common::$newslt_panelizer_subMenu);

        //preparing for drag and drop
        $I->scrollTo($featured_bar);
        $I->doubleClick($featured_bar);

        $I->wait(3);

        $I->click(Pageobj_common::$open2_bttn);
    }

    public function add_content_to_edition_HM_ROI_top()
    {

        $top_bar = Pageobj_common::$nweslt_sec_top_bar;


        $I = $this;
        $I->comment("TOP SECTION<-------------");
        $I->pauseExecution();
        $I->waitForElement(Pageobj_common::$newslt_panelizer_subMenu);

        $I->click(Pageobj_common::$Close2_bttn);

        //preparing for drag and drop
        $I->scrollTo($top_bar);
        $I->doubleClick($top_bar);

        $I->wait(3);


        $I->click(Pageobj_common::$open2_bttn);
    }





    public function selecting_article_newslt_HM_ROI_featured()
    {
        //Drag and dropping articles into Edition Sections
        $I = $this;
        $I->wait(5);


        //----Featured Stores-----//

        $I->wait(3);

        $I->executeInSelenium(function (webdriver $webDriver) {
            $art2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art2));
            $loc1 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_featured));
            $webDriver->action()
                ->moveToElement($art2)
                ->clickAndHold($art2)
                ->moveToElement($loc1)
                ->release($loc1)
                ->perform();
        });

        $I->wait(3);

        $I->executeInSelenium(function (webdriver $webDriver) {
            $art1 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art1));
            $loc1 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_featured));
            $webDriver->action()
                ->moveToElement($art1)
                ->clickAndHold($art1)
                ->moveToElement($loc1)
                ->release($loc1)
                ->perform();
        });

    }

    public function selecting_article_newslt_HM_ROI_top()
    {

        $I = $this;

        //----Top Stores-----//
        $I->wait(3);
        $I->pauseExecution();
        $I->executeInSelenium(function (webdriver $webDriver) {
            $art3 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art6));
            $loc2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_top));
            $webDriver->action()
                ->moveToElement($art3)
                ->clickAndHold($art3)
                ->moveToElement($loc2)
                ->release($loc2)
                ->perform();
        });
        $I->pauseExecution();
        $I->wait(3);

        $I->executeInSelenium(function (webdriver $webDriver) {
            $art3 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art5));
            $loc2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_top));
            $webDriver->action()
                ->moveToElement($art3)
                ->clickAndHold($art3)
                ->moveToElement($loc2)
                ->release($loc2)
                ->perform();
        });
        $I->pauseExecution();
        $I->wait(3);

        $I->executeInSelenium(function (webdriver $webDriver) {
            $art4 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art4));
            $loc2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_top));
            $webDriver->action()
                ->moveToElement($art4)
                ->clickAndHold($art4)
                ->moveToElement($loc2)
                ->release($loc2)
                ->perform();
        });


        $I->executeInSelenium(function (webdriver $webDriver) {
            $art3 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$newslt_art3));
            $loc2 = $webDriver->findElement(\WebDriverBy::xpath(Pageobj_common::$nweslt_sec_top));
            $webDriver->action()
                ->moveToElement($art3)
                ->clickAndHold($art3)
                ->moveToElement($loc2)
                ->release($loc2)
                ->perform();
        });
        $I->comment("\n LAST IMAGE LAST IMAGE LAST IMAGE LAST IMAGE <<<<<<<<<<<<<< \n");
        $I->pauseExecution();
    }




////----TOC Generic Functions----/////

    public function assert_newslt_toc()
    {
        $I = $this;
        //grab titles of articles - in filter menu
        $filter_menue = $I->grabMultiple('//*[@id="block-views-solr-search-block-1"]/div[2]/div/div/div/ul/li');
        print_r($filter_menue);

        //$I->scrollTo(Pageobj_common::$newslt_previewbtn);
        $I->executeJS('window.scrollTo(941, 22);');
        $I->wait(3);
        $I->click(Pageobj_common::$newslt_previewbtn);

        $I->wait(3);

        //switch to iFrame
        $I->executeInSelenium(function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
            $my_frame = $webDriver->findElement(\WebDriverBy::xpath('.//iframe[@id=\'email-preview\']'));
            $webDriver->switchTo()->frame($my_frame);
        });

        $I->waitForElementVisible(Pageobj_common::$newslt_TOC_bloc);

        $TOC_list = $I->grabMultiple('//*[@id="mkto_autogen_id_0"]/div/table/tbody/tr/td/center/table/tbody/tr/td/ul/li/a');
        print_r($TOC_list);

        \PHPUnit_Framework_Assert::assertSame($filter_menue, $TOC_list, "TOC's differ!");

    }

    public function dup_newslt_edition_err($nwsl_Site_number)
    {
        $I = $this;
        $edition_node = $I->grabFromCurrentUrl('~\/node\/(\d+)~');

        if ($I->seePageHasElement(Pageobj_common::$newslt_alert_err)) {
           $I->comment("\n YOU HAVE ENTERED THE DUP ERRER FUNCTION \n");
           $I->amOnUrl("https://stage.qtxnewsletter.com/node/{$edition_node}/delete");
           $I->pauseExecution();
           $I->click(Pageobj_common::$save_bttn);
           $I->pauseExecution();
           $this->Create_new_edition($nwsl_Site_number);
        }
    }


    function delete_edition()
    {
        $I = $this;
        $edition_node = $I->grabFromCurrentUrl('~\/node\/(\d+)~');
        $I->amOnUrl("https://stage.qtxnewsletter.com/node/{$edition_node}/delete");
        $I->pauseExecution();
        $I->click(Pageobj_common::$save_bttn);
    }

    function scrollDoubleClick($y, $element)
    {
        $I = $this;

        $I->executeJS("window.scrollTo(0, {$y});");
        $I->moveMouseOver($element);
        $I->doubleClick($element);

    }

    ////---TOC Copy Change tests---////

    function edit_articleTitle($art1_cord, $art2_cord, $art3_cord, $art4_cord, $art5_cord, $art6_cord)
    {
        $modal_header = '//div[@class=\'modal-header\']';
        $modal_titleFld = '//input[@id=\'edit-title\']';
        $article_one = Locator::elementAt('//span[@class=\'edit\']', 1);
        $article_two = Locator::elementAt('//span[@class=\'edit\']', 2);
        $article_three = Locator::elementAt('//span[@class=\'edit\']', 3);
        $article_four = Locator::elementAt('//span[@class=\'edit\']', 4);
        $article_five = Locator::elementAt('//span[@class=\'edit\']', 5);
        $article_six = Locator::elementAt('//span[@class=\'edit\']', 6);

        $I = $this;
        $I->reloadPage();

        //article one
            $I->executeJS("window.scrollTo{$art1_cord};");
            $I->comment('xxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            $I->pauseExecution();

            $I->click($article_one);
            $I->waitForElementVisible($modal_header);
            $I->wait(3);
            $original_title_copy_1 = $I->grabAttributeFrom($modal_titleFld, 'value');
            $I->fillField($modal_titleFld, "(edit) " . $original_title_copy_1);
            $I->click(Pageobj_common::$newslt_edit_saveBttn);
            $I->wait(3);
            $editCopy_1 = "(edit) $original_title_copy_1";

        //article two
            $I->executeJS("window.scrollTo{$art2_cord};");

            $I->click($article_two);
              $I->waitForElementVisible($modal_header);
              $I->wait(3);
            $original_title_copy_2 = $I->grabAttributeFrom($modal_titleFld, 'value');
            $I->fillField($modal_titleFld, "(edit) " . $original_title_copy_2);
            $I->click(Pageobj_common::$newslt_edit_saveBttn);
            $I->wait(3);
            $editCopy_2 = "(edit) $original_title_copy_2";

        //article three
            $I->executeJS("window.scrollTo{$art3_cord};");

            $I->click($article_three);
            $I->waitForElementVisible($modal_header);
            $I->wait(3);
            $original_title_copy_3 = $I->grabAttributeFrom($modal_titleFld, 'value');
            $I->fillField($modal_titleFld, "(edit) " . $original_title_copy_3);
            $I->click(Pageobj_common::$newslt_edit_saveBttn);
            $I->wait(3);
            $editCopy_3 = "(edit) $original_title_copy_3";

        //article four
            $I->executeJS("window.scrollTo{$art4_cord};");

            $I->click($article_four);
            $I->waitForElementVisible($modal_header);
            $I->wait(3);
            $original_title_copy_4 = $I->grabAttributeFrom($modal_titleFld, 'value');
            $I->fillField($modal_titleFld, "(edit) " . $original_title_copy_4);
            $I->click(Pageobj_common::$newslt_edit_saveBttn);
            $I->wait(3);
            $editCopy_4 = "(edit) $original_title_copy_4";

        //article five
            $I->executeJS("window.scrollTo{$art5_cord};");
            $I->click($article_five);
            $I->waitForElementVisible($modal_header);
            $I->wait(3);
            $original_title_copy_5 = $I->grabAttributeFrom($modal_titleFld, 'value');
            $I->fillField($modal_titleFld, "(edit) " . $original_title_copy_5);
            $I->click(Pageobj_common::$newslt_edit_saveBttn);
            $I->wait(3);
            $editCopy_5 = "(edit) $original_title_copy_5";

        //article six
            $I->executeJS("window.scrollTo{$art6_cord};");

            $I->click($article_six);
            $I->waitForElementVisible($modal_header);
            $I->wait(3);
            $original_title_copy_6 = $I->grabAttributeFrom($modal_titleFld, 'value');
            $I->fillField($modal_titleFld, "(edit) " . $original_title_copy_6);
            $I->click(Pageobj_common::$newslt_edit_saveBttn);
            $I->pauseExecution();
            $I->wait(3);
            $editCopy_6 = "(edit) $original_title_copy_6";

            $edited_titles = array();
            array_push($edited_titles, $editCopy_1, $editCopy_2, $editCopy_3, $editCopy_4, $editCopy_5, $editCopy_6);
            print_r($edited_titles);

            $I->reloadPage();

            return $edited_titles;
    }

    public function assertTOC_editTitles($edited_titles)
    {
        $I = $this;

        $I->executeJS('window.scrollTo(941, 22);');
        $I->wait(3);
        $I->click(Pageobj_common::$newslt_previewbtn);

        $I->wait(3);

        //switch to iFrame
        $I->executeInSelenium(function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
            $my_frame = $webDriver->findElement(\WebDriverBy::xpath('.//iframe[@id=\'email-preview\']'));
            $webDriver->switchTo()->frame($my_frame);
        });

        $I->waitForElementVisible(Pageobj_common::$newslt_TOC_bloc);

        $TOC_list_grabbed = $I->grabMultiple('//*[@id="mkto_autogen_id_0"]/div/table/tbody/tr/td/center/table/tbody/tr/td/ul/li/a');
        print_r($TOC_list_grabbed);

        \PHPUnit_Framework_Assert::assertSame($edited_titles, $TOC_list_grabbed, "TOC's differ!");
    }


}