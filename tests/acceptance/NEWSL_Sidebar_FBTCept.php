<?php
use \page\Pageobj_unique;

$I = new AcceptanceTester($scenario);
$TestSteps = new Step\Acceptance\NEWSL_Sidebar($scenario);

$site_unique = Pageobj_unique::$prod_url_fbt;
$sidebarContainer_unique = Pageobj_unique::$nwsl_sidebarContainer_FBT;
$Pagtitle_unique = 'FierceBiotech Publication Signup';


$I->wantTo('know that the Optin Form is functioning and displayed as expected -- FierceBiotech');

//Verifying Optin form components are on the page under test
$TestSteps->Verify_nwslSideBar_elements(
    $site_unique,
    $sidebarContainer_unique
);

//Verifying that Optin form submits correctly + main form's email field has submitted email
//This script fails on this step, no email in form
$TestSteps->NwslSideBar_submitForm($TestSteps,
    $Pagtitle_unique
);
