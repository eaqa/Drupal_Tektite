<?php
/*
 * todo refactor to RESPONSE
 * */
use \page\Pageobj_unique;
use \page\Pageobj_common;

$I = new AcceptanceTester($scenario);
$Base = new Step\Acceptance\BaseFunctions($scenario);
$TestSteps = new Step\Acceptance\SearchBlock($scenario);

$I->wantTo('Verify the presence and functionality of the Global Search Block - ResponseMag');

//Unique
$Site = Pageobj_unique::$prod_url_response;
$SearchBlock_unique = Pageobj_unique::$SearhBlockDiv_response;
$PageBodyDiv_unique = Pageobj_unique::$pageBodyDiv_response;

$SearchTerm = 'media';
$SearchField = Pageobj_common::$SearchField;

$TestSteps->Search_elements_present($TestSteps, $Base,
    $Site,
    $SearchBlock_unique
);

$TestSteps->ConductSearchAndVerify($TestSteps,
    $PageBodyDiv_unique,
    $SearchTerm,
    $SearchField
);
