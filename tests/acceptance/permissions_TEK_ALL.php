<?php
use \page\Pageobj_unique;
use \page\Pageobj_common;
use Step\Acceptance\BaseFunctions as BaseFunctions;
use Step\Acceptance\permissions as Permissions;
/**
 * @skip
 */


/*TO DO
Need to uodate baselines to what is currently on Production as your baseline compare.
What is on confluence is out of date, many diffs found.
Need to code for the two remaining sites:
NightClub
SensorsMag


*/





$I = new AcceptanceTester($scenario);
$Base = new BaseFunctions($scenario);
$TestSteps = new Permissions($scenario);

//skipping test
$scenario->skip();

$Usr_Role_Creds = Pageobj_common::$csvFile_admin; #define user role credentials

$I->wantTo('verify permissions integrity -- Tektite Platform');

/////Grab Permissions from Prod - All Sites/////

############AmericanSpa############
$Base->loginSSO(
    $Usr_Role_Creds,
    Pageobj_unique::$EngineerSSO_Amspa_prod #Unique per site
);
$TestSteps->GrabPermissionConfig('americanspa');

############FierceBiotech############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_FBT_prod); #Unique per site
$TestSteps->GrabPermissionConfig('fiercebiotech');

############FierceCable############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_FC_prod); #Unique per site
$TestSteps->GrabPermissionConfig('fiercecable');

############FierceCmo############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_FCMO_prod); #Unique per site
$TestSteps->GrabPermissionConfig('fiercecmo');

############FierceHealthCare############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_FHC_prod); #Unique per site
$TestSteps->GrabPermissionConfig('fiercehealthcare');

############FiercePharma############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_FP_prod); #Unique per site
$TestSteps->GrabPermissionConfig('fiercepharma');

############FierceRetail############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_FR_prod); #Unique per site
$TestSteps->GrabPermissionConfig('fierceretail');

############FierceTelecom############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_FT_prod); #Unique per site
$TestSteps->GrabPermissionConfig('fiercetelecom');

############FierceWireless############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_FW_prod); #Unique per site
$TestSteps->GrabPermissionConfig('fiercewireless');

############HotelManagement############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_HM_prod); #Unique per site
$TestSteps->GrabPermissionConfig('hotelmanagement');

############LuxuryTravelAdvisor############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_LTA_prod); #Unique per site
$TestSteps->GrabPermissionConfig('luxurytraveladvisor');

############TravelAgentCentral############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_TAC_prod); #Unique per site
$TestSteps->GrabPermissionConfig('travelagentcentral');

############AmericanSalon############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_Salon_prod); #Unique per site
$TestSteps->GrabPermissionConfig('americansalon');

############SensorsMag############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_SensorsMag_prod); #Unique per site
$TestSteps->GrabPermissionConfig('sensorsmag');

############NightClub############
$Base->OneLogIn_AppUrl(Pageobj_unique::$EngineerSSO_NC_prod); #Unique per site
$TestSteps->GrabPermissionConfig('nightclub');

$script = file_get_contents('/Users/erikanderson/Documents/CodeceptionCsvFiles/user_perms.sh');
echo shell_exec($script);