<?php
use \page\Pageobj_unique;
use \page\Pageobj_common;

$I = new AcceptanceTester($scenario);
$Base = new Step\Acceptance\BaseFunctions($scenario);
$TestSteps = new Step\Acceptance\SearchBlock($scenario);

$I->wantTo('Verify the presence and functionality of the Global Search Block - SensorsMag');

//Unique
$Site = Pageobj_unique::$prod_url_sensorsMag;
$PageBodyDiv_unique = Pageobj_unique::$pageBodyDiv_sensorsMag;
$searchBlockDiv_unique = Pageobj_unique::$SearhBlockDiv_sensorsMag;
$SearchTerm = 'Test';


$TestSteps->Search_elements_present($TestSteps, $Base,
    $Site,
    $searchBlockDiv_unique
);

$TestSteps->ConductSearchAndVerify($TestSteps,
    $PageBodyDiv_unique,
    $SearchTerm,
    $SearchField = Pageobj_common::$SearchField
);
