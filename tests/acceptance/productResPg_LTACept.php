<?php
use \page\Pageobj_unique;
use \page\Pageobj_common;

use Step\Acceptance\BaseFunctions as BaseFunction;
use Step\Acceptance\ResourceProductsPgs as productRes;

$I = new AcceptanceTester($scenario);
$Base = new BaseFunction($scenario);
$TestSteps = new productRes($scenario);


$Base_URL = Pageobj_unique::$prod_url_lta;


$I->wantTo('verify the integrity of Product & Resources pages -- Lux Travel Adviser');

//Testing Events Product Page
$I->productRespg_verifyContentPages($TestSteps, $Base,
//Navigating to Page
    $Base_URL,
    Pageobj_unique::$dealPg_products_lta, //Content Page
    'Cruises', //Cat option to select //Category Selection
    'Cruises' //option to be selected
);

//Testing Resource Page
$I->productRespg_verifyResourcePages($TestSteps, $Base,
//Navigating to Page
    $Base_URL,
    Pageobj_common::$resourcesPg, //Resources page
    'Deal',
    'Cruises', //Cat option to select //Category Selection
    'Deal' //option to be selected
);