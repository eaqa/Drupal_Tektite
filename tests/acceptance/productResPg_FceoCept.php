<?php
use \page\Pageobj_unique;
use \page\Pageobj_common;

use Step\Acceptance\BaseFunctions as BaseFunction;
use Step\Acceptance\ResourceProductsPgs as productRes;

$I = new AcceptanceTester($scenario);
$Base = new BaseFunction($scenario);
$TestSteps = new productRes($scenario);

$Base_URL = Pageobj_unique::$prod_url_fceo;

$I->wantTo('verify the integrity of Product & Resources pages -- FierceCEO');

//Testing Resource Page
$I->productRespg_verifyResourcePages($TestSteps, $Base,
//Navigating to Page
    $Base_URL,
    Pageobj_common::$resourcesPg, //Resources page
    'Webinar', //selected type option to select
    'Customer Experience', //Cat option to select //Category Selection
    'Webinar' //option to be selected
);