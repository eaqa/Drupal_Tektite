<?php
use \page\Pageobj_unique;
use \page\Pageobj_NavLinks;

use Step\Acceptance\BaseFunctions as BaseFunction;
use Step\Acceptance\MenuNavLinksVeri as VerifyNav;

$I = new AcceptanceTester($scenario);
$Base = new BaseFunction($scenario);
$TestSteps = new VerifyNav($scenario);


#Unique to site:
$Site = Pageobj_unique::$prod_url_tac;
$Unique_Navlocator_main = Pageobj_unique::$NavLocator_main_tac;
$Unique_Navlocator_topSoc = Pageobj_unique::$NavLocator_topSoc_tac;
$Unique_NavLocator_top = Pageobj_unique::$NavLocator_top_tac;
$Unique_NavLocator_footerNav = Pageobj_unique::$NavLocator_footerNav_tac;
$Unique_SubMenuXpath = '-tektite-tac-';


$PreArray_main = Pageobj_NavLinks::$Main_navLinks_tac;
$PreArray_top = Pageobj_NavLinks::$Top_navLinks_tac;
$PreArray_topSoc = Pageobj_NavLinks::$Top_SocLinks_tac;
$PreArray_bottomSoc = Pageobj_NavLinks::$Bottom_SocLinks_tac;
$PreArray_footerNav = Pageobj_NavLinks::$footerMenuNav_tac;

$I->wantTo('verify the integrity of Navigation Links -- Travel Agent Central');

//Testing Main and Sub Nav links
$TestSteps->MenuNavLinks_Main_SubLinks_verify($TestSteps, $Base,
    $Site, //url to navigate to
    $Unique_Navlocator_main, //xpath to nav element to test   ---- #unique Locator
    $PreArray_main, //defined array to verify SUT
    $Unique_SubMenuXpath //custom sublink Xpath ---- #unique Locator
);

//Testing Top Nav links
$TestSteps->MenuNavLinks_Nav_verify($TestSteps,
    $Site, //url to navigate to
    $Unique_NavLocator_top, //xpath to nav element to test  ---- #unique Locator
    $PreArray_top //defined array to verify SUT
);

//Testing Top Social Media links
$TestSteps->MenuNavLinks_Nav_verify($TestSteps,
    $Site, //url to navigate to
    $Unique_Navlocator_topSoc, //xpath to nav element to test  ---- #unique Locator
    $PreArray_topSoc //defined array to verify SUT
);

//Testing Footer Social Media/QtxLogo links
$TestSteps->MenuNavLinks_Nav_verify($TestSteps,
    $Site, //url to navigate to
    Pageobj_NavLinks::$NavLocator_bottomSoc, //xpath to nav element to test
    $PreArray_bottomSoc //defined array to verify SUT
);

//Testing Footer Nav menu links
$TestSteps->MenuNavLinks_Nav_verify($TestSteps,
    $Site, //url to navigate to
    $Unique_NavLocator_footerNav, //xpath to nav element to test ---- #unique Locator
    $PreArray_footerNav //defined array to verify SUT
);