<?php
use \page\Pageobj_unique;
use \page\Pageobj_common;

use Step\Acceptance\BaseFunctions as BaseFunction;
use Step\Acceptance\ResourceProductsPgs as productRes;

$I = new AcceptanceTester($scenario);
$Base = new BaseFunction($scenario);
$TestSteps = new productRes($scenario);

$Base_URL = Pageobj_unique::$prod_url_amspa;

$I->wantTo('verify the integrity of Product & Resources pages -- American Spa');

//Testing Contest Product Page
$I->productRespg_verifyContentPages($TestSteps, $Base,
//Navigating to Page
    $Base_URL, //American Spa
    Pageobj_common::$contestPg_products, //Content Page
    'Spas', //Cat option to select //Category Selection
    'Wellness' //option to be selected
);

//Testing Webinar Product Page
$I->productRespg_verifyContentPages($TestSteps, $Base,
//Navigating to Page
    $Base_URL,
    Pageobj_common::$webinarPg_products, //Webinar Page
    'Spas', //Cat option to select //Category Selection
    'Skincare'); //option to be selected

//Testing Resource Page
$I->productRespg_verifyResourcePages($TestSteps, $Base,
//Navigating to Page
    $Base_URL,
    Pageobj_common::$resourcesPg, //Resources page
    'eBook', //selected type option to select
    'Wellness', //Cat option to select //Category Selection
    'Contest' //option to be selected
);