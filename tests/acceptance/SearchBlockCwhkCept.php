<?php

use \page\Pageobj_unique;
use \page\Pageobj_common;

$I = new AcceptanceTester($scenario);
$Base = new Step\Acceptance\BaseFunctions($scenario);
$TestSteps = new Step\Acceptance\SearchBlock($scenario);

$I->wantTo('Verify the presence and functionality of the Global Search Block - CWHK');

//Unique
$Site = Pageobj_unique::$prod_url_Cwhk;

$SearchBlock_unique = Pageobj_unique::$SearhBlockDiv_Cwhk;
$PageBodyDiv_unique = Pageobj_unique::$pageBodyDiv_Cwhk;

$SearchTerm = 'Shanghai';
$SearchField = Pageobj_common::$SearchField;

$TestSteps->Search_elements_present($TestSteps, $Base,
    $Site,
    $SearchBlock_unique
);

$TestSteps->ConductSearchAndVerify($TestSteps,
    $PageBodyDiv_unique,
    $SearchTerm,
    $SearchField
);
