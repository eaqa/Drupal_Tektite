<?php

use \page\Pageobj_unique;

$I = new AcceptanceTester($scenario);
$TestSteps = new Step\Acceptance\NEWSL_Sidebar($scenario);

$site_unique = Pageobj_unique::$prod_url_Cwhk;
$sidebarContainer_unique = Pageobj_unique::$nwsl_sidebarContainer_Cwhk;
$Pagtitle_unique = 'ComputerWorld Hong Kong | Newsletter Signup'; ##Update to actual title when available


$I->wantTo('know that the Optin Form is functioning and displayed as expected -- CWHK');

//Verifying Optin form components are on the page under test
$TestSteps->Verify_nwslSideBar_elements(
    $site_unique,
    $sidebarContainer_unique
);

//Verifying that Optin form submits correctly
$TestSteps->NwslSideBar_submitForm($TestSteps,
    $Pagtitle_unique
);
