<?php
use \page\Pageobj_unique;
use \page\Pageobj_common;

use Step\Acceptance\BaseFunctions as BaseFunction;
use Step\Acceptance\ResourceProductsPgs as productRes;

$I = new AcceptanceTester($scenario);
$Base = new BaseFunction($scenario);
$TestSteps = new productRes($scenario);


$Base_URL = Pageobj_unique::$prod_url_tac;


$I->wantTo('verify the integrity of Product & Resources pages -- Travel Agent Central');

//Testing Events Product Page
$I->productRespg_verifyContentPages($TestSteps, $Base,
//Navigating to Page
    $Base_URL,
    Pageobj_unique::$dealPg_products_tac, //Content Page
    'Destinations', //Cat option to select //Category Selection
    'Destinations' //option to be selected
);

//Testing Resource Page
$I->productRespg_verifyResourcePages($TestSteps, $Base,
//Navigating to Page
    $Base_URL,
    Pageobj_common::$resourcesPg, //Resources page
    'Deal',
    'Destinations', //Cat option to select //Category Selection
    'Deal' //option to be selected
);