<?php
use \page\Pageobj_unique;
use \page\Pageobj_common;

$I = new AcceptanceTester($scenario);
$Base = new Step\Acceptance\BaseFunctions($scenario);
$TestSteps = new Step\Acceptance\SearchBlock($scenario);

$I->wantTo('Verify the presence and functionality of the Global Search Block - Hotel Management');

//Unique
$Site = Pageobj_unique::$prod_url_hm;
$PageBodyDiv_unique = Pageobj_unique::$pageBodyDiv_hm;
$SearchTerm = 'Hoteliers';


$TestSteps->Search_elements_present($TestSteps, $Base,
    $Site,
    Pageobj_common::$searchBlockDiv
);

$TestSteps->ConductSearchAndVerify($TestSteps,
    $PageBodyDiv_unique,
    $SearchTerm,
    $SearchField = Pageobj_common::$SearchField
);
