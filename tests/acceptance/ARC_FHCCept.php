<?php
// @group ARC
use \page\Pageobj_unique;
use \page\Pageobj_common;

use Step\Acceptance\BaseFunctions as BaseFunction;
use Step\Acceptance\ARC as ARCfunc;

$I = new AcceptanceTester($scenario);
$Base = new BaseFunction($scenario);
$TestSteps = new ARCfunc($scenario);

$Site = Pageobj_unique::$prod_url_fhc;
$Usr_Role_Creds = Pageobj_common::$csvFile_AdminEd; #define user role credentials
$OneLogIn_AppTile_unique = Pageobj_unique::$AdminEdSSO_FHC_prod;

//search words unique to FHC
$Author = 'lotus_kid';
$PrimaryTaxonomy = 'Healthcare';
$SecondaryTaxonomy = 'Healthcare';
$Keyword = 'Adventist Health System';
$Topic = 'Bad Debt';

$I->wantTo('verify the integrity of ARC - upload/browse/insert -- FierceHealthcare');


//Used to log into OneLogin
$Base->loginSSO(
    $Usr_Role_Creds,
    $OneLogIn_AppTile_unique #Unique per site
);

$PrimaryImgID_1 =
    $I->ARCValidation_browseForImg($TestSteps, $Base,
        $Site,
        Pageobj_common::$arc_imgSearchTerm, #used to change img search term

        //Filling out Article Required Fields
        $Author,
        $PrimaryTaxonomy,
        $SecondaryTaxonomy,
        $Keyword,
        $Topic);


$PrimaryImgID_2 =
    //Verifying Primary image is on the Preview article page
    $I->ARCVaidation_Uploadimg_InsertImg($TestSteps, $Base,
        Pageobj_common::$primaryImgFileName, #used to change img file name
        //Filling out Article Required Fields
        $Author,
        $PrimaryTaxonomy,
        $SecondaryTaxonomy,
        $Keyword,
        $Topic);

$I->ARCValidation_verfyImgsOnPreviewPg($TestSteps,
    Pageobj_common::$primaryImgFileName_2, //inserted image file name
    $Base
);

$I->ArcValidation_Uploadimg_editor($TestSteps,
    $Author,
    $PrimaryTaxonomy,
    $SecondaryTaxonomy,
    $Keyword,
    $Topic,
    Pageobj_common::$editor_arc_upload_img,
    $Base
);

//Asserting that the upload primary image has a different ID then the browse for primary image ID
PHPUnit_Framework_Assert::assertNotEquals($PrimaryImgID_1, $PrimaryImgID_2, 'Primary Image ID_1 and ID_2 are equal, they should be different');