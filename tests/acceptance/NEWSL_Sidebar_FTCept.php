<?php
use \page\Pageobj_unique;

$I = new AcceptanceTester($scenario);
$TestSteps = new Step\Acceptance\NEWSL_Sidebar($scenario);

$site_unique = Pageobj_unique::$prod_url_ft;
$sidebarContainer_unique = Pageobj_unique::$nwsl_sidebarContainer_FT;
$Pagtitle_unique = 'FierceTelecom Publication Signup';


$I->wantTo('know that the Optin Form is functioning and displayed as expected -- FierceTelecom');

//Verifying Optin form components are on the page under test
$TestSteps->Verify_nwslSideBar_elements(
    $site_unique,
    $sidebarContainer_unique
);

//Verifying that Optin form submits correctly
$TestSteps->NwslSideBar_submitForm($TestSteps,
    $Pagtitle_unique
);
