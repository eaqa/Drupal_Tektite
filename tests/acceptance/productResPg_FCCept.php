<?php
use \page\Pageobj_unique;
use \page\Pageobj_common;

use Step\Acceptance\BaseFunctions as BaseFunction;
use Step\Acceptance\ResourceProductsPgs as productRes;

$I = new AcceptanceTester($scenario);
$Base = new BaseFunction($scenario);
$TestSteps = new productRes($scenario);


$Base_URL = Pageobj_unique::$prod_url_fc;


$I->wantTo('verify the integrity of Product & Resources pages -- FierceCable');

//Testing Events Product Page
$I->productRespg_verifyContentPages($TestSteps, $Base,
//Navigating to  Page
    $Base_URL,
    Pageobj_unique::$eventPg_products_fc, //Content Page
    'Broadcasting', //Cat option to select //Category Selection
    'Cable' //option to be selected
);

//Testing Resource Page
$I->productRespg_verifyResourcePages($TestSteps, $Base,
//Navigating to Resource Page
    $Base_URL,
    Pageobj_common::$resourcesPg, //Resources page
    'eBook',
    'Cable', //Cat option to select //Category Selection
    'eBook' //option to be selected
);