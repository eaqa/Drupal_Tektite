<?php
use \page\Pageobj_unique;

$I = new AcceptanceTester($scenario);
$TestSteps = new Step\Acceptance\NEWSL_Sidebar($scenario);

$site_unique = Pageobj_unique::$prod_url_response;
$sidebarContainer_unique = Pageobj_unique::$nwsl_sidebarContainer_response;
$Pagtitle_unique = 'Response Magazine | Newsletter Signup';


$I->wantTo('know that the Optin Form is functioning and displayed as expected -- Response');

//Verifying Optin form components are on the page under test
$TestSteps->Verify_nwslSideBar_elements(
    $site_unique,
    $sidebarContainer_unique
);

//Verifying that Optin form submits correctly
$TestSteps->NwslSideBar_submitForm($TestSteps,
    $Pagtitle_unique
);
