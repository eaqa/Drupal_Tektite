<?php
use \page\Pageobj_unique;
use \page\Pageobj_NavLinks;

use Step\Acceptance\BaseFunctions as BaseFunction;
use Step\Acceptance\MenuNavLinksVeri as VerifyNav;

$I = new AcceptanceTester($scenario);
$Base = new BaseFunction($scenario);
$TestSteps = new VerifyNav($scenario);

//swap out for each site
$Site = Pageobj_unique::$prod_url_fw;
$PreArray_main = Pageobj_NavLinks::$Main_navLinks_fw;
$PreArray_top = Pageobj_NavLinks::$Top_navLinks_fw;
$PreArray_topSoc = Pageobj_NavLinks::$Top_SocLinks_fw;
$PreArray_bottomSoc = Pageobj_NavLinks::$Bottom_SocLinks_fw;
$PreArray_footerNav = Pageobj_NavLinks::$footerMenuNav_fw;

$I->wantTo('verify the integrity of Navigation Links -- FierceWireless');

//Testing Main and Sub Nav links
$TestSteps->MenuNavLinks_Main_SubLinks_verify($TestSteps, $Base,
    $Site, //url to navigate to
    Pageobj_NavLinks::$NavLocator_main, //xpath to nav element to test
    $PreArray_main //defined array to verify SUT
);

//Testing Top Nav links
$TestSteps->MenuNavLinks_Nav_verify($TestSteps,
    $Site, //url to navigate to
    Pageobj_NavLinks::$NavLocator_top, //xpath to nav element to test
    $PreArray_top //defined array to verify SUT

);

//Testing Top Social Media links
$TestSteps->MenuNavLinks_Nav_verify($TestSteps,
    $Site, //url to navigate to
    Pageobj_NavLinks::$NavLocator_topSoc, //xpath to nav element to test
    $PreArray_topSoc //defined array to verify SUT

);

//Testing Footer Social Media/QtxLogo links
$TestSteps->MenuNavLinks_Nav_verify($TestSteps,
    $Site, //url to navigate to
    Pageobj_NavLinks::$NavLocator_bottomSoc, //xpath to nav element to test
    $PreArray_bottomSoc //defined array to verify SUT

);

//Testing Footer Nav menu links
$TestSteps->MenuNavLinks_Nav_verify($TestSteps,
    $Site, //url to navigate to
    Pageobj_NavLinks::$NavLocator_footerNav, //xpath to nav element to test
    $PreArray_footerNav //defined array to verify SUT

);