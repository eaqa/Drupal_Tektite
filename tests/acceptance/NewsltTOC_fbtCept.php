<?php

use Page\Pageobj_unique as Pageobj_unique;
//Run on Stage//

use Step\Acceptance\BaseFunctions as BaseFunction;
$Newsletter_SiteNumber_Unique = Pageobj_unique::$nwsl_siteNumber_FBT;
$art1_cord = Pageobj_unique::$newslt_edit_art1_cord_fbt;
$art2_cord = Pageobj_unique::$newslt_edit_art2_cord_fbt;
$art3_cord = Pageobj_unique::$newslt_edit_art3_cord_fbt;
$art4_cord = Pageobj_unique::$newslt_edit_art4_cord_fbt;
$art5_cord = Pageobj_unique::$newslt_edit_art5_cord_fbt;
$art6_cord = Pageobj_unique::$newslt_edit_art6_cord_fbt;

$I = new AcceptanceTester($scenario);
$Base = new BaseFunction($scenario);
$TestSteps = new \Step\Acceptance\NewsletterTool($scenario);

$I->wantTo('Test the functionality of the Newsletter Editions TOC - FierceBiotech');


$TestSteps->NewsltTOC_generateEdition($TestSteps, $Base, $Newsletter_SiteNumber_Unique, $Newsletter_SiteNumber_Unique);
$TestSteps->NewsltTOC_SelectingArticle_FBT($TestSteps);
$TestSteps->NewsltTOC_assert($TestSteps);
$TestSteps->NewsltTOC_titleEdit_assert($TestSteps, $art1_cord, $art2_cord, $art3_cord, $art4_cord, $art5_cord, $art6_cord);
$TestSteps->delete_edition();


