<?php
require_once './vendor/codeception/codeception/autoload.php';
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    use \Codeception\Task\MergeReports;
    use \Codeception\Task\SplitTestsByGroups;

    #evenly split all acceptance test into 4 batches
    public function parallelSplitTests_acceptance()
    {
        //Split tests by files
        $this->taskSplitTestFilesByGroups(4)
            ->projectRoot('.')
            ->testsFrom('tests/acceptance')
            ->groupsTo("tests/_data/acceptance_Batch_")
            ->run();
    }

    #evenly split all acceptance test into 4 batches
    public function parallelSplitTests_api()
    {
        //Split tests by files
        $this->taskSplitTestFilesByGroups(4)
            ->projectRoot('.')
            ->testsFrom('tests/api')
            ->groupsTo("tests/_data/api_Batch_")
            ->run();
    }



    #run test acceptance batches in parallel
    public function parallelRun_acceptance()
    {
        $parallel = $this->taskParallelExec();

        for ($i = 1; $i <= 4; $i++) {
            $parallel->process(
                $this->taskExec('codecept run acceptance')
                    ->optionList('group', 'acceptance_Batch_' . "$i")
                    ->optionList('xml', "tests/_log/result_acceptance_Batch_$i.xml")
            );
            }
            return $parallel->run();
        }

    #run test api batches in parallel
    public function parallelRun_api()
    {
        $parallel = $this->taskParallelExec();

        for ($i = 1; $i <= 4; $i++) {
            $parallel->process(
                $this->taskExec('codecept run api')
                    ->optionList('group', 'api_Batch_' . "$i")
                    ->optionList('xml', "tests/_log/result_api_Batch_$i.xml")
            );
        }
        return $parallel->run();
    }

    #merge acceptance test result reports into one
    public function parallelMergeResults_acceptance()
    {
        $merge = $this->taskMergeXmlReports();
        for ($i=1; $i<=4; $i++) {
            $merge->from("tests/_output/tests/_log/result_acceptance_Batch_$i.xml");
        }
        $merge->into("tests/_output/tests/_log/result_Merged_acceptance_Batch__$i.xml")->run();
    }

    #merge api test result reports into one
    public function parallelMergeResults_api()
    {
        $merge = $this->taskMergeXmlReports();
        for ($i=1; $i<=4; $i++) {
            $merge->from("tests/_output/tests/_log/result_api_Batch_$i.xml");
        }
        $merge->into("tests/_output/tests/_log/result_Merged_api_Batch__$i.xml")->run();
    }

    #evenly split all ARC test into 4 batches
        public function parallelSplitTests_arc()
    {
        //Split tests by files
        $this->taskSplitTestsByGroups(4)
            ->projectRoot('.')
            ->testsFrom('tests/acceptance/ARC')
            ->groupsTo("tests/_data/arc_Batch_")
            ->run();
    }

    #evenly split all NewsletterTool test into 4 batches
    public function parallelSplitTests_NEWSLT()
    {
        //Split tests by files
        $this->taskSplitTestsByGroups(4)
            ->projectRoot('.')
            ->testsFrom('tests/api/NEWSLT')
            ->groupsTo("tests/_data/NEWSLT_Batch_")
            ->run();
    }

    #run test ARC batches in parallel
    public function parallelRun_arc()
    {
        $parallel = $this->taskParallelExec();

        for ($i = 1; $i <= 4; $i++) {
            $parallel->process(
                $this->taskExec('codecept run acceptance')
                    ->optionList('group', 'arc_Batch_' . "$i")
                    ->optionList('xml', "tests/_log/result_arc_Batch_$i.xml")
            );
        }
        return $parallel->run();
    }

    #run test Newsletter Tool batches in parallel
    public function parallelRun_NEWSLT()
    {
        $parallel = $this->taskParallelExec();

        for ($i = 1; $i <= 4; $i++) {
            $parallel->process(
                $this->taskExec('codecept run api')
                    ->optionList('group', 'NEWSLT_Batch_' . "$i")
                    ->optionList('xml', "tests/_log/result_NEWSLT_Batch_$i.xml")
            );
        }
        return $parallel->run();
    }

    #merge ARC test result reports into one
    public function parallelMergeResults_arc()
    {
        $merge = $this->taskMergeXmlReports();
        for ($i=1; $i<=4; $i++) {
            $merge->from("tests/_output/tests/_log/result_arc_Batch_$i.xml");
        }
        $merge->into("tests/_output/tests/_log/result_Merged_arc_Batch_$i.xml")->run();
    }

    #merge Newsletter Tool test result reports into one
    public function parallelMergeResults_NEWSLT()
    {
        $merge = $this->taskMergeXmlReports();
        for ($i=1; $i<=4; $i++) {
            $merge->from("tests/_output/tests/_log/result_NEWSLT_Batch_$i.xml");
        }
        $merge->into("tests/_output/tests/_log/result_Merged_NEWSLT_Batch_$i.xml")->run();
    }


    #Run All acceptance tasks together
    function parallelAll_acceptance()
    {
        $this->parallelSplitTests_acceptance();
        $result = $this->parallelRun_acceptance();
        $this->parallelMergeResults_acceptance();
        return $result;
    }

    #Run All api tasks together
    function parallelAll_api()
    {
        $this->parallelSplitTests_api();
        $result = $this->parallelRun_api();
        return $result;
    }

    #Run All ARC tasks together
    function parallelAll_arc()
    {
        $this->parallelSplitTests_arc();
        $result = $this->parallelRun_arc();
        $this->parallelMergeResults_arc();
        return $result;
    }

    #Run All ARC tasks together
    function parallelAll_NEWSLT()
    {
        $this->parallelSplitTests_NEWSLT();
        $result = $this->parallelRun_NEWSLT();
        $this->parallelMergeResults_arc();
        return $result;
    }

}